module.exports = function(grunt) {
 
    grunt.initConfig({
 
        //our JSHint options
        jshint: {
                options: {
                        laxcomma: true,
                        curly: true,
                        eqeqeq: true,
                        eqnull: true,
                        browser: true,
                        globals: {
                            jQuery: true
                        },
                },
                common_models: ['./common/models/*js', './common/*js', './server/boot/root.js'] //files to lint
        },

        mochaTest: {
        	test: {
        		options: {
        		
        			reporter: 'spec',
        			quiet: false,
        			clearRequireCache: false
        		},
        	src: ['./api-test/sprint1/*.js']
        	}
        }
    });
 
    //load our tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.registerTask('default', ['jshint','mochaTest']);

};