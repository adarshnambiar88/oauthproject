var async = require('async');
var util = require('./utilities.js');

var getLeaveRequestList = function(id, clb){
	var leaveData = [];
	var pendingLeaves = {};
	var pendingLeavesData = [];
	var takenLeaves = {};
	var takenLeavesData = [];
	var toBeTakenLeaves = {};
	var toBeTakenLeavesData = [];
	var currentDate = new Date();

	var pendingStatus;
	var approvedStatus;
	var deniedStatus;
	var cancelStatus;
	var cancellationInProgressStatus;

	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();
	
	var data = {};

	async.waterfall([
		function getLeaveStatusValue(callback){											//function to get the cancel leave status
		util.ErpKeysCode("leave_status", null, function(err, response){
			if(err){
				callback(err);
			}
			else{
				response.forEach(function(res){
					if(res.keyValue === "Approved"){
						approvedStatus = res.keyCode;
					}
					else if(res.keyValue === "Pending"){
						pendingStatus = res.keyCode;
					}
					else if(res.keyValue === "Denied"){
						deniedStatus = res.keyCode;
					}
					else if(res.keyValue === "Cancelled"){
						cancelStatus = res. keyCode;
					}
					else if(res.keyValue === "Cancellation In Progress"){
						cancellationInProgressStatus = res.keyCode;
					}
				});
				callback(null);
			}
		});
		},

		function leaveRequests(callback){															//function to find all leave request along with type of leaves by making a join between leave_request and type_of_leave table
			app.models.LeaveRequest.find({
			where: {and: [{employeeId:id}, {leaveRequestYear:currentYear}, {status: {nin: [cancelStatus,deniedStatus]}}]},
			order: 'fromDate ASC',
			include: {relation: 'typeOfLeaves'}
			}, function(err, allLeaveRequests){
				if(err){
					return callback(err);
				}
				else{
					callback(null, allLeaveRequests);
				}
			});
		},

		function categorizeLeaveRequests(leaves, cb){
			async.forEachOf(leaves,function(leave, index, callback){				//to loop through all leaves to get the leaves under pending,future and raken category if any
				data = {};															//creating a JSON formatted data
				data.fromDate = leave.fromDate;
				data.toDate = leave.toDate;
				data.numberOfdays = leave.numberOfDays;
				data.typeOfLeave = leave.typeOfLeaves().leaveType;
				data.description = leave.description;
				data.leaveRequestId = leave.id;
				data.halfDaySlot = leave.halfDaySlot;
				if(leave.status === pendingStatus || leave.status === cancellationInProgressStatus){
					if(leave.status === pendingStatus){
						data.status = "Pending";
					}
					else if(leave.status === cancellationInProgressStatus){
						data.status = "Cancellation In Progress";	
					}
					pendingLeavesData.push(data);
					callback(null);
				}	
				else if(leave.status === approvedStatus){
					data.status = "Approved";
					if(moment(currentDate).isBefore(leave.fromDate)){
						toBeTakenLeavesData.push(data);
					}
					else{
						takenLeavesData.push(data);
					}
					callback(null);
				}
				else{
					callback(null);
				} 
				console.log("DATA", data);
			}
			,function(err){
				if(err){
					return cb(err);
				}
				else{
					cb(null);
				}
			});
		},

		function leaveFormatting(callback){													//data to be send
			if(pendingLeavesData.length > 0){
				pendingLeaves.title = "Pending";
				pendingLeaves.data = pendingLeavesData;
				leaveData.push(pendingLeaves);
			}
			if(toBeTakenLeavesData.length > 0){
				toBeTakenLeaves.title = "Future";
				toBeTakenLeaves.data = toBeTakenLeavesData;
				leaveData.push(toBeTakenLeaves);
			}
			if(takenLeavesData.length > 0){
				takenLeaves.title = "Taken";
				takenLeaves.data = takenLeavesData;
				leaveData.push(takenLeaves);
			}
			callback(null);
		}

		]
		,function(err, response){															//function to send the result to calling function
			if(err){
				return clb(err);
			}
			else{
				console.log("leaveData", leaveData);
				clb(null, leaveData);
			}
	});									
};

exports.getLeaveRequestList = getLeaveRequestList;