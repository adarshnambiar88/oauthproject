var async = require('async');
var util = require('./utilities.js');

var leaveOptions =function(id, cb){																		//function to fetch all the leave options an employee has
	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();

	var leaveTypes = {};
	var privilege_eligible_count = 0;
	var maternity_eligible_count = 0;
	var paternity_eligible_count = 0;
	var compOff_eligible_count = 0;

	var cancellationInProgressStatus;
	var pendingStatus; 
	var approvedStatus;
	var cancelStatus;
	var deniedStatus;


	var count_privilege = 0;
	var count_paternity = 0;
	var count_maternity = 0;
	var count_compOff = 0;


	var types = [];

	async.waterfall([
	function getLeaveStatusValue(callback){											//function to get the cancel leave status
		util.ErpKeysCode("leave_status", null, function(err, response){
			if(err){
				callback(err);
			}
			else{
				response.forEach(function(res){
					if(res.keyValue === "Approved"){
						approvedStatus = res.keyCode;
					}
					else if(res.keyValue === "Pending"){
						pendingStatus = res.keyCode;
					}
					else if(res.keyValue === "Denied"){
						deniedStatus = res.keyCode;
					}
					else if(res.keyValue === "Cancelled"){
						cancelStatus = res. keyCode;
					}
					else if(res.keyValue === "Cancellation In Progress"){
						cancellationInProgressStatus = res.keyCode;
					}
				});
				callback(null);
			}
		});
	},

	function getLeaveTypeCodes(callback){
		util.typeOfLeaves(null , function(err, response){
			if(err){
				return callback(err);
			}
			else{
				leaveTypes = response;
				console.log(leaveTypes);
				callback(null);
			}
		});
	},

	function getEligibleLeaveCount(callback){
		app.models.EmployeeLeaveEligibility.find({where: {and: [{employeeId: id}, {eligibilityYear: currentYear}]}},function(err, eligibleLeaveTypes){
			if(err){
				return callback(err);
			}
			else if(eligibleLeaveTypes){
				eligibleLeaveTypes.forEach(function(eligibleLeaveType){
					if(eligibleLeaveType.typeOfLeavesId === leaveTypes.privilegeId){
						privilege_eligible_count = eligibleLeaveType.eligibilityCount;
					}
					else if(eligibleLeaveType.typeOfLeavesId === leaveTypes.paternityId){
						paternity_eligible_count = eligibleLeaveType.eligibilityCount;
					}
					else if(eligibleLeaveType.typeOfLeavesId === leaveTypes.maternityId){
						maternity_eligible_count = eligibleLeaveType.eligibilityCount;
					}
					else if(leaveEligibility.typeOfLeavesId === leaveTypes.compOffId){
						compOff_eligible_count = leaveEligibility.eligibilityCount;
					}
				});
				callback(null);
			}
			else{
				return callback("No Leaves Has Been Assigned To You");
			}
		});
	},

	function getAllLeaveRequests(callback){																//function to get all leave requests by an employee  
		app.models.LeaveRequest.find({
		where: {and: [{employeeId:id}, {leaveRequestYear:currentYear}, {status: {nin: [cancelStatus, deniedStatus]}}]}
		,include: {relation: 'typeOfLeaves'}
		},function(err, leaves){
			if(err){
				return callback(err);
			}
			else{
				callback(null, leaves);
			}	
		});
	},

	function privilegeLeavesCount(leaves, callback){													//function to get the privilege leaves count applied by an employee
		var leaveType = {};
		console.log("********", privilege_eligible_count);
		if(leaves.length < 1){
			leaveType = {};
			if(privilege_eligible_count > 0){
				leaveType = {};
				leaveType.name = "privilege";
	 			leaveType.count = privilege_eligible_count;
	 			types.push(leaveType);
			}
			if(paternity_eligible_count > 0){
				leaveType = {};
				leaveType.name = "paternity";
	 			leaveType.count = paternity_eligible_count;
	 			types.push(leaveType);
			}
			if(maternity_eligible_count > 0){
				leaveType = {};
				leaveType.name = "maternity";
	 			leaveType.count = maternity_eligible_count;
	 			types.push(leaveType);
			}
			if(compOff_eligible_count > 0){
				leaveType = {};
				leaveType.name = "Comp-off";
	 			leaveType.count = compOff_eligible_count;
	 			types.push(leaveType);
			}
			callback(null);
		}
		else{
			leaves.forEach(function(leave){																	//for-loop to loop through all the leaves
				if((leave.typeOfLeavesId === leave.typeOfLeaves().id)&&(leave.typeOfLeaves().leaveType === "Privilege")){
					count_privilege += leave.numberOfDays;
				}
				if((leave.typeOfLeavesId === leave.typeOfLeaves().id)&&(leave.typeOfLeaves().leaveType === "Paternity")){
					count_paternity += leave.numberOfDays;
				}
				if((leave.typeOfLeavesId === leave.typeOfLeaves().id)&&(leave.typeOfLeaves().leaveType === "Maternity")){
					count_maternity += leave.numberOfDays;
				}
				if((leave.typeOfLeavesId === leave.typeOfLeaves().id)&&(leave.typeOfLeaves().leaveType === "Compoff")){
					count_compOff += leave.numberOfDays;
				}
			});
			if((privilege_eligible_count - count_privilege) > 0){
				leaveType = {};
				leaveType.name = "privilege";
	 			leaveType.count = privilege_eligible_count - count_privilege;
	 			types.push(leaveType);
			}
			if((paternity_eligible_count - count_paternity) > 0){
				leaveType = {};
				leaveType.name = "paternity";
	 			leaveType.count = paternity_eligible_count - count_paternity;
	 			types.push(leaveType);
			}
			if((maternity_eligible_count - count_maternity) > 0){
				leaveType = {};
				leaveType.name = "maternity";
	 			leaveType.count = maternity_eligible_count - count_maternity;
	 			types.push(leaveType);
			}
			if((compOff_eligible_count - count_compOff) > 0){
				leaveType = {};
				leaveType.name = "Comp-off";
	 			leaveType.count = compOff_eligible_count - count_compOff;
	 			types.push(leaveType);
			}
			callback(null);
		}
	}
	]
	,function(err, response){																			//returning final result to the calling function
		if(err){
			return cb(err);
		}
		else{
			console.log("counts", types);
			cb(null, types);
		}
	});
};

exports.leaveOptions = leaveOptions ;
