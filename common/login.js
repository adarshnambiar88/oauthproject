var async = require('async');
var util = require('../common/utilities.js');

var getUserDetails = function(callback){
	var data = {};
	var loggedInData = {};

	async.waterfall([
		function getLoggedInData(callback){
			util.userIdentification(function(err, response){
  				if(err){
    				return callback("Unauthorized User");
  				}
  				loggedInData = response;
  				console.log("user******************", loggedInData);
          data.id = loggedInData.id;
  				//console.log("*********** data", data);
				callback(null, loggedInData);
			});
		},

		function employeeLogin(loggedInData, callback){                                                              //function for employee login(it gives an access token)
     		app.models.Employee.findOne({where: {userId: loggedInData.userId}}, function(err, employeeData){
       			if (err) {
          			console.log("asda??????sd",err);
         			return callback("Incorect Username Or Password");
       			}
        		else{
         			data.employeeData = employeeData ;
         			//console.log("*********** data", data);
         			callback(null);
        		}
      		});
    	},

    	function loginStatus(callback){                                                                 //function to maintain the login status                             
      		util.loginStatus(data.employeeData.id, function(err, status){
        		if(err){
       				return callback(err);
       			}
       			if(status === 0){
        			data.freshLogin = 1;
        		}
       			//console.log("*********** data", data, status);
       			callback(null);
      		});
    	},

    	function isEmployeeManager(callback){                                                           //function to determine whether an user is manager or not
      		util.managerList(function(err, managerList){
        		if(err){
         			return callback(err);
       			}	
        		var employeeId = data.employeeData.id;
        		var isManager = managerList.find(function(element){
          			return (element.reportingManagerId === employeeId.toString());
       			});
        		if(isManager!== undefined){
          			data.manager = 1;
        		}
        		//console.log("managerList", managerList,isManager);
        		//console.log("*********** data", data);
        		callback(null);
      		});
    	},

    	function isEmployeeAdmin(callback){
      		var employeeId = data.employeeData.id;
      		//console.log("ADmin login");
      		app.models.EmployeeRoleMapping.find({where: {employeeId: employeeId}, include : 'roles'}, function(err, employeeRoles){
        		if(err){
          			return callback(err);
        		}
        		else{
          		//console.log("employeeRoles", employeeRoles);
          			if(employeeRoles.length > 0){
            			employeeRoles.forEach(function(employeeRole){
              				if(employeeRole.roles().name === "Admin"){
                				data.admin = 1;
                				console.log("Admin login");
              				}
            			});
            			//console.log("*********** data", data);
            			callback(null);
          			}
          			else{
          				//console.log("*********** data", data);
          				callback(null);
          			}
          		}
      		});
    	},

    	function isEmployeeALastLevelManager(callback){                                                 //function to check whether an employee is a highest level manager(management) or not
      		var employeeId = data.employeeData.id;
      		app.models.Employee.findOne({where: {id: employeeId}}, function(err, response){
        		if(err){
          			return callback(err);
        		}
        		if((response.emp_id === "P10E0001") || (response.emp_id === "P10E0002")){
          			data.lastLevelManager = 1;
        		}
        		if(response.active === '0'){
        			return callback("UserId Is Not Active");
        		}
        		data.name = response.name;
        		data.firstName = response.firstName;
        		data.lastName = response.lastName;
        		//console.log("*********** data", data);
        		callback(null);
      		});
    	}

	], function(err, response){
		if(err){
			return callback(err);
		}
		else{
			callback(null, data);
		}
	});
};

exports.getUserDetails = getUserDetails;