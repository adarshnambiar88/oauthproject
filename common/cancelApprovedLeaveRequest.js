var util = require('./utilities.js');
var async = require('async');

var cancelApprovedLeaveRequest = function(req, callback){
	console.log("Request received:-", req);
	var data = {};
	var currentDate = new Date().toLocaleString();
	var cancellationInProgressCode;
	var actionTakenByEmployee;
	var emailData = {};
	async.waterfall([
	function cancellationInProgressCodeValue(callback){
		util.ErpKeysCode("leave_status", "Cancellation In Progress", function(err, key){
			if(err){
				return callback(err);
			}
			else{
				cancellationInProgressCode = key.keyCode;
				console.log("cancellationInProgressCode", cancellationInProgressCode);
				callback(null, cancellationInProgressCode);
			}
		});
	},

	function updateLeaveStatus(cancellationInProgressCode, callback){
		app.models.LeaveRequest.updateAll({id: req.id}, {status: cancellationInProgressCode}, function(err, response){
			if(err){
				callback(err);
			}
			else{
				console.log("updateLeaveStatus", response);
				callback(null);
			}
		});
	},

	function cancelActionTakenCode(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("action_taken", "Cancel", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					actionTakenByEmployee = key.keyCode;
					console.log("cancelActionTakenCode", actionTakenByEmployee);
					callback(null);
				}
			});
	},

	function getEmployeeDetails(callback){
		util.employeeDetail(req.employeeId, function(err, employeeDetail){
			if(err){
				return callback(err);
			}
			else{
				console.log("getEmployeeDetails", employeeDetail);
				callback(null, employeeDetail);
			}
		});
	},

	function getManagerDetails(employeeDetail, callback){
		util.employeeDetail(employeeDetail.reportingManagerId, function(err, managerDetail){
			if(err){
				return callback(err);
			}
			else{
				console.log("getManagerDetails", managerDetail);
				callback(null, employeeDetail,managerDetail);
			}
		});
	},

	function makeLeaveWorkflowEntry(employeeDetail, managerDetail, callback){
		data.actionTaken = actionTakenByEmployee;
		data.actionTimestamp = currentDate;
		data.comments = req.comments;
		data.leaveRequestId = req.id;
		data.toEmployee = employeeDetail.reportingManagerId;
		data.fromEmployee = employeeDetail.id;
		app.models.LeaveWorkflow.create(data, function(err, response){
			if(err){
				return callback(err);
			}
			else{
				console.log("makeLeaveWorkflowEntry", response);
				callback(null, employeeDetail, managerDetail);
			}
		});
	},

	function sendEmailNotification(employeeDetail, managerDetail, callback){
		emailData.actionByEmployee = "Applied For A Cancellion Of Leave Request";
		emailData.employeeEmpId = employeeDetail.empId;
		emailData.employeeName = employeeDetail.name;
		emailData.managerEmail = managerDetail.email;
		emailData.employeeEmail = employeeDetail.email;
		emailData.employeeDesignation = employeeDetail.designation;
		emailData.comments = req.comments;
		emailData.leaveFromDate = moment(req.fromDate).format("MMM DD, YYYY");
		emailData.leaveToDate = moment(req.toDate).format("MMM DD, YYYY");
		emailData.numberOfDays = req.numberOfDays;
		util.sendEmailNotificationToManagerForCancellationRequest(emailData, function(err, response){
			if(err){
				return callback(err);
			}
		});
		callback(null);
	}
	], function(err, response){
		if(err){
			callback(err);
		}
		else{
			callback(null);
		}

	});
};


exports.cancelApprovedLeaveRequest = cancelApprovedLeaveRequest;