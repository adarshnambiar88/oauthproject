var util = require('./utilities.js');
var async = require('async');

var actionOnCancellationInProgressLeaveRequest = function(req, callback){
	var data = {};
	var actionTakenByEmployee;
	var actionTakenByManager;
	var cancelActionTakenCode;
	var employeeInformation = {};
	var emailData = {};
	var currentDate = new Date().toLocaleString();
	if(req.action === "cancel"){
		async.waterfall([
		function cancelCancellationRequestActionTakenCode(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("action_taken", "CancelCancellationRequest", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					actionTakenByEmployee = key.keyCode;
					console.log("Cancel Cancellation Request", actionTakenByEmployee);
					callback(null);
				}
			});
		},

		function approvedLeaveStatusCodeValue(callback){
			util.ErpKeysCode("leave_status", "Approved", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					approvedLeaveStatusCode = key.keyCode;
					console.log("Code", approvedLeaveStatusCode);
					callback(null, approvedLeaveStatusCode);
				}
			});
		},

		function updateLeaveStatus(approvedLeaveStatusCode, callback){
			app.models.LeaveRequest.updateAll({id: req.id}, {status: approvedLeaveStatusCode}, function(err, response){
				if(err){
					callback(err);
				}
				else{
					console.log("updateLeaveStatus", response);
					callback(null);
				}
			});
		},

		function cancelActionTakenCodeValue(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("action_taken", "Cancel", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					cancelActionTakenCode = key.keyCode;
					console.log("cancelActionTakenCode", cancelActionTakenCode);
					callback(null);
				}
			});
		},

		function approveActionTakenCode(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("action_taken", "Approve", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					actionTakenByManager = key.keyCode;
					console.log("approveActionTakenCode", actionTakenByManager);
					callback(null);
				}
			});
		},

		function getEmployeeDetails(callback){
			util.employeeDetail(req.employeeId, function(err, employeeInformation){
				if(err){
					return callback(err);
				}
				else{
					employeeDetail = employeeInformation;
					console.log("getEmployeeDetails", employeeDetail);
					callback(null, employeeDetail);
				}
			});
		},

		function makeLeaveWorkflowEntry(employeeDetail, callback){
			data.actionTaken = actionTakenByEmployee;
			data.actionTimestamp = currentDate;
			data.comments = req.comments;
			data.leaveRequestId = req.id;
			data.toEmployee = employeeDetail.reportingManagerId;
			data.fromEmployee = employeeDetail.id;
			app.models.LeaveWorkflow.create(data, function(err, response){
				if(err){
					return callback(err);
				}
				else{
					console.log("makeLeaveWorkflowEntry", response);
					callback(null, employeeDetail);
				}
				});
		},

		function leaveWorkflowIdOfCancelLeaveRequest(employeeDetail, callback){
			app.models.LeaveWorkflow.findOne({where: {and: [{leaveRequestId: req.id}, {actionTaken: cancelActionTakenCode}]}, order: 'actionTimestamp'}, function(err, leaveWorkflow){
				if(err){
					return callback(err);
				}
				else{
					console.log("leaveWorkflowIdOfCancelLeaveRequest", leaveWorkflow);
					callback(null, employeeDetail, leaveWorkflow);
				}
			});
		},

		function findLeaveWorkflowAttachedWithLeaveId(employeeDetail, leaveWorkflow, callback){
			app.models.LeaveWorkflow.find({where: {and: [{leaveRequestId: req.id},{id: {gt: leaveWorkflow.id}}]}, order: 'actionTimestamp'}, function(err, leaveWorkflows){
				if(err){
					return callback(err);
				}
				else{
					console.log("findLeaveWorkflowAttachedWithLeaveId", leaveWorkflows);
					callback(null, leaveWorkflows);
				}
			});
		},

		function sendEmailNotification(leaveWorkflows, callback){
			console.log("sendEmailNotification", employeeDetail);
			async.each(leaveWorkflows, function(leaveWorkflow, clb){
				if((leaveWorkflow.actionTaken === actionTakenByManager)&&(leaveWorkflow.toEmployee !== employeeDetail.id)){
					async.waterfall([
					// function getEmployeeDetails(callback){
					// 	util.employeeDetail(leaveWorkflow.fromEmployee, function(err, employeeDetail){
					// 		if(err){
					// 			return callback(err);
					// 		}
					// 		else{
					// 			callback(null, employeeDetail);
					// 		}
					// 	});
					// },

					function getManagerDetails(callback){
						util.employeeDetail(leaveWorkflow.fromEmployee, function(err, managerDetail){
							if(err){
								return callback(err);
							}
							else{
								callback(null, managerDetail);
							}
						});
					},

					function sendEmailNotificationToManager(managerDetail, callback){
						emailData.actionByEmployee = "Cancelling A Leave Cancel Request";
						emailData.employeeEmpId = employeeDetail.empId;
						emailData.employeeName = employeeDetail.name;
						emailData.managerEmail = managerDetail.email;
						emailData.employeeEmail = employeeDetail.email;
						emailData.employeeDesignation = employeeDetail.designation;
						emailData.comments = req.comments;
						emailData.leaveFromDate = moment(req.fromDate).format("MMM DD, YYYY");
						emailData.leaveToDate = moment(req.toDate).format("MMM DD, YYYY");
						emailData.numberOfDays = req.numberOfDays;
						util.sendEmailNotificationToManager(emailData, function(err, response){
							if(err){
								return callback(err);
							}
						});
						callback(null);
					}
					], function(err, response){
						if(err){
							return clb(err);
						}
						else{
							clb(null);
						}
					});
				}
				else{
					clb(null);
				}
			}, function(err){
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		
		}
		], function(err, response){
			if(err){
				callback(err);
			}
			else{
				callback(null);
			}
		});
	}
	else{
		callback(null);
	}
};


exports.actionOnCancellationInProgressLeaveRequest = actionOnCancellationInProgressLeaveRequest;