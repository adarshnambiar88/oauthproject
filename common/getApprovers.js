var util = require('./utilities.js');
var async = require('async');


var getApproverNames = function(id, cb){											 //Function to get the approvers for a particular employee

    app.models.Employee.findOne({where: {id: id}
    },function(err, emp){
        if(err){
          return cb(err);
        }
        var data = {};
        var approvers = [];
        var level = emp.levelOfApprovalRequired;
        var managerId = emp.reportingManagerId;
        console.log("approval",level,managerId);
        if(level){
        	async.whilst(                                                                 //while-loop to find the list of approvers 
        	function(){
          	if(level > 0){
            	return true;
          	}
          	else{
          		return false;
          	}
        	},

        	function getManagerName(callback){
          	app.models.Employee.findOne({where: {id: managerId}
          	},function(err, res){
            		if(err){
              		return callback(err);
            		}
            		data = {};
            		data.name = res.name ;
            		data.id = res.id;
            		approvers.push(data);
            		managerId = res.reportingManagerId;
            		level--;
            		callback(null,approvers);
          		});
        	},

        	function(err,data){
          		if(err){
            		return cb(err);
          		}
          		else{ 
            		console.log("list of managers",data);
            		cb(null, data);
          		}
        	}
      	); 
      }
      else if(level === 0){
        async.whilst(                                                                 //while-loop to find the list of approvers 
          function(){
            if(managerId){
              return true;
            }
            else{
              return false;
            }
          },

          function getManagerName(callback){
            app.models.Employee.findOne({where: {id: managerId}
            },function(err, res){
                if(err){
                  return callback(err);
                }
                data = {};
                data.name = res.name ;
                data.id = res.id;
                approvers.push(data);
                managerId = res.reportingManagerId;
                level--;
                callback(null,approvers);
              });
          },

          function(err,data){
              if(err){
                return cb(err);
              }
              else{ 
                console.log("list of managers",data);
                cb(null, data);
              }
          }
        );
      }
      else{
        callback(null, data);
      }     
    });
};

exports.getApproverNames = getApproverNames;