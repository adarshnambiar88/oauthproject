var util = require('./utilities.js');
var async = require('async');
var leaveSummary = require('./leave-statistics.js');
var moment = require('moment');
require('moment-weekday-calc');

		
var leaveRequestsWaitingForApproval = function(id, cb){
	var data =[];
	var cancelStatus;
	var approvedLeaveStatusCode;
	var cancellationInProgressCode;
	var currentDate = new Date();

	async.waterfall([
	function cancelStatusValue(callback){												//function to get the cancel leave status code
		util.ErpKeysCode("leave_status", "Cancelled", function(err, res){
			if(err){
				return callback(err);
			}
			else{
				cancelStatus = res.keyCode;
				callback(null);
			}
		});
	},

	function cancellationInProgressValue(callback){												//function to get the cancel leave status code
		util.ErpKeysCode("leave_status", "Cancellation In Progress", function(err, res){
			if(err){
				return callback(err);
			}
			else{
				cancellationInProgressCode = res.keyCode;
				callback(null);
			}
		});
	},


	function approvedLeaveStatusCodeValue(callback){
			util.ErpKeysCode("leave_status", "Approved", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					approvedLeaveStatusCode = key.keyCode;
					console.log("Code", approvedLeaveStatusCode);
					callback(null);
				}
			});
	},

	function getLatestTimestamps(callback){												//function to get the latest entry from the leave workflow corresponding to the particular leave request
		util.latestTimestamp(id, function(err, leaveTimestamps){
			if(err){
				return cb(err);
			}
			else{
				//console.log(leaveTimestamps);
				callback(null, leaveTimestamps);	
			}
		});
	},

	function findLeaveWorkflows(leaveTimestamps, clb){									//function to get the leave workflows 
		var leaveRequest = {};
			//console.log("leaveRequestsWaitingForApproval is called", leaveTimestamps);
		async.eachSeries(leaveTimestamps, function(leaveTimestamp, callback){
			var leaveId = leaveTimestamp.leave_request_id;
			var actionTimestamp = 	new Date(leaveTimestamp.action_timestamp).toLocaleString();
			//console.log("&&&&&&&&&&&&&&&&",leaveId, actionTimestamp);
			app.models.LeaveWorkflow.findOne({											//query to find the leave workflow correspondind to the leave id 
			where: {and: [{toEmployee: id}, {leaveRequestId: leaveId}, {actionTimestamp: actionTimestamp}]}
			,include: {
				relation: 'leaveRequest',
				scope:{
					include: {
						relation: 'employee',
						scope: {
							fields: ['name', 'firstName', 'lastName', 'empId', 'designation']
						}
					}
				}
			}
			},function(err, leaveWorkflow){
				if(err){
					return callback(err);
				}
				//console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", leaveWorkflow, id);
				if(leaveWorkflow !== null && leaveWorkflow.leaveRequest().employeeId!==id && leaveWorkflow.leaveRequest().status!== cancelStatus && leaveWorkflow.leaveRequest().status!== approvedLeaveStatusCode){ 
					var leaveRequestData = leaveWorkflow.leaveRequest();
					leaveRequest = {};
					//console.log("leaveRequestsWaitingForApproval is called", leaveWorkflow.leaveRequest().employeeId, id);
					//console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%", leaveWorkflow, id);
					leaveRequest.fromEmployee = leaveRequestData.employeeId;
					leaveRequest.employeeOfficialId = leaveRequestData.employee().empId;
					leaveRequest.fromDate = leaveRequestData.fromDate;
					leaveRequest.toDate = leaveRequestData.toDate;
					leaveRequest.numberOfDays = leaveRequestData.numberOfDays;
					leaveRequest.typeOfLeave = leaveRequestData.typeOfLeavesId;
					leaveRequest.description = leaveRequestData.description;
					leaveRequest.employeeName = leaveRequestData.employee().name;
					leaveRequest.employeeFirstName = leaveRequestData.employee().firstName;
					leaveRequest.employeeLastName = leaveRequestData.employee().lastName;
					leaveRequest.leaveId = leaveRequestData.id;


					if(leaveRequestData.status === cancellationInProgressCode){
						leaveRequest.status = "Cancellation In Progress";
					}
					if(leaveRequestData.halfDaySlot !== 0){
						leaveRequest.halfDay = leaveRequestData.halfDaySlot;
					}
					app.models.ErpKeys.findOne({where: {and: [{type: "designation"}, {keyCode: leaveRequestData.employee().designation}]}}, function(err, key){
						if(err){
							return callback(err);
						}
						else{
							leaveRequest.designation = key.keyValue;
							data.push(leaveRequest);
							//console.log("asdasdas",leaveRequest);	
							callback(null);	
						}
					});
				}
				else{
					callback(null);
				}
			});
		}, function(err){																//function returning from async.eachSeries
			if(err){
				return clb(err);
			}
			else{
				clb(null);
			}
		});
	},

	function leaveTypeFormatting(cb){													//function to format the leave type
		async.each(data, function(leaveData, callback){									//function to find the type of leave request corresponding to the leave type recieved from UI
			app.models.TypeOfLeaves.findOne({
			where: {id: leaveData.typeOfLeave}}
			,function(err, type){
				if(err){
					return callback(err);
				}
				leaveData.leaveType = type.leaveType;
				leaveSummary.leaveStatistics(leaveData.fromEmployee, function(err, statistics){
					if(err){
						return callback(err);
					}
					leaveData.leaveRemaining = statistics.remaining;
					leaveData.totalLeavesTaken = statistics.availed;
					callback(null);	
				});	
			});
		}
		,function(err){																	//function returning from asyn.each
			if(err){
				return cb(err);
			}
			else{
				cb(null);
			}
		});
	},


	function findAppliedOn(cb){													
		async.each(data, function(leaveData, callback){			
			//console.log("######", leaveData);						
			app.models.LeaveWorkflow.findOne({										
			where: {and: [{leaveRequestId: leaveData.leaveId}, {fromEmployee: leaveData.fromEmployee}]},
			order: 'actionTimestamp ASC'
			}, function(err, timestamp){
				if(err){
					return callback(err);
				}
				else{
					//console.log("######", timestamp);
					leaveData.appliedOn = timestamp.actionTimestamp;
					callback(null);
					}
				});
		}
		,function(err){																	//function returning from asyn.each
			if(err){
				return cb(err);
			}
			else{
				cb(null);
			}
		});
	},

	function appovedStatusCode(callback){												//function to find the "approved" leave status code 
		util.ErpKeysCode("leave_status", "Approved", function(err, key){
			if(err){
				return callback(err);
			}
			else{
				var approvedStatus = key.keyCode;
				//console.log("1st phase",approvedStatus);
				callback(null, approvedStatus);
			}
		});
	},

	function lastLeaveTakenData(approvedStatus, cb){									//function to get the last leave taken details
		async.each(data, function(leaveData, callback){									//to loop through all the approved leave requests to find the last taken leave
			app.models.LeaveRequest.find({
			where: {and: [{employeeId:leaveData.fromEmployee},{status: approvedStatus},{fromDate: {lte: currentDate}}]},
			order: 'id DESC',
			limit: 1
			}, function(err, leaves){
				if(err){
					return callback(err);
				}
				if(leaves.length > 0){
					console.log("approvedStatus",approvedStatus);
					leaveData.lastLeaveTakenFromDate = leaves[0].fromDate;
					leaveData.lastLeaveTakenToDate = leaves[0].toDate;
					callback(null);
				}
				else{
					callback(null);
				}
			});
		}
		,function(err){																	//function returning from async.each
			if(err){
					cb(err);
				}
				else{
					cb(null);
				}
			});
	},
	],function(err, response){															//function returning the result of the API
		if(err){
			return cb(err);
		}
		else{
			cb(null, data);
		}
	});

};


exports.leaveRequestsWaitingForApproval = leaveRequestsWaitingForApproval;
