var async = require('async');
var ejs = require('ejs');
var fs = require('fs');
var loopback = require('loopback');
var moment = require('moment');
require('moment-weekday-calc');

var date = function(fromDate, toDate, numberOfDays, cb){						//To calculate the no. of days /to-date from from-date
	var days = 0;
	console.log("%%%%%%%%%%",fromDate,toDate,numberOfDays);
	if((fromDate == "Invalid Date") || (numberOfDays === undefined))
	{
		var arr = [];
		var toDateString = toDate.toISOString();
		var count = 0;
		var day = toDate.getDay();
		console.log("@@@@@@@@@@@@@@",day);
		app.models.HolidayList.find({where: {holidayDate: {lte: toDate}}
		},function(err, leaves){
			if(err)
				return cb(err);

			leaves.forEach(function(leave){
				if(leave.holidayDate.toISOString()==toDateString)
					count++;
				arr.push(leave.holidayDate);
			});

			console.log(count);
			if(fromDate == "Invalid Date")
			{
				if(count!=0 || day==0 || day==6){
					fromDate = moment(toDate).addWorkdays(-numberOfDays,arr)._d;	
				}
				else{
					fromDate = moment(toDate).addWorkdays(-(numberOfDays-1),arr)._d;
				}
			var data ={
						fromDate: fromDate
			};
			}
			else
			{
				days = moment().weekdayCalc(fromDate, toDate, [1,2,3,4,5] ,arr);
				var data = {
						numberOfDays: days
				};
			}
			console.log(data);
		cb(null, data);
		});
	}
	else{
		var arr = [];
		var fromDateString = fromDate.toISOString();
		var count = 0;
		var day = fromDate.getDay();
		console.log("**************",day);
		app.models.HolidayList.find({
		},function(err, leaves){
			if(err)
				return cb(err);

			leaves.forEach(function(leave){
				if(leave.holidayDate.toISOString()==fromDateString)
					count++;
				arr.push(leave.holidayDate);
			});

			console.log(count);
			//console.log(arr);
			var data = {};
			if(count!=0 || day==0 || day==6){
				if(numberOfDays == 0.5)
				{
					toDate = fromDate ;
					data.halfDay = 1;
				}
				else
					toDate = moment(fromDate).addWorkdays(numberOfDays,arr)._d;	
			}
			else{
				if(numberOfDays == 0.5)
				{
					toDate = fromDate ;
					data.halfDay = 1;
				}
				else
				toDate = moment(fromDate).addWorkdays(numberOfDays-1,arr)._d;
			}

			data.toDate = toDate ;
			console.log(data);
			cb(null, data);
		});
	}
};


var specialLeaveDateCalculation = function(fromDate, toDate, numberOfDays, callback){                    //function to calculate the to-date/number of days from from-date field
	var data = {};
	console.log(fromDate, toDate, numberOfDays);
	var toDateString = toDate.toString(); 																						 //in this case holidays and weekends will be included
	console.log(fromDate, toDateString, numberOfDays);	
	if(numberOfDays === undefined){
		var days = moment().weekdayCalc(fromDate, toDate, [0,1,2,3,4,5,6]);
		data.numberOfDays = days;
	}
	else if(toDateString === "Invalid Date"){
		toDate = moment(fromDate).addWeekdaysFromSet(numberOfDays-1, [0,1,2,3,4,5,6]);
		data.toDate = toDate ;
	}
	callback(null, data);
};


var ErpKeysCode = function(type, keyValue, cb){															//function to fetch the key values from erp_keys table
	if(keyValue === null){
		app.models.ErpKeys.find({where: {type: type}
		},function(err, keys){
			if(err){
				return cb(err);
			}
			else{
				cb(null, keys);
			}
		});
	}
	else{
		app.models.ErpKeys.findOne({where: {and: [{type: type}, {keyValue: keyValue}]}
		},function(err, key){
			if(err){
				return cb(err);
			}
			else{
				cb(null, key);
			}
		});
	}
};

	
var sortWorkflow = function(arr, cb){																	//function to sort the leave workflow
	arr.sort(function(a, b){
		return b.actionTimestamp - a.actionTimestamp;
	});
	cb(null, arr);
};



var latestTimestamp = function(id, callback){																//function to get the latest leave_workflow entry 
	var pgp = require("pg-promise")(/*options*/);
	var db = pgp("postgres://" + username + ":" + password + "@" + host + ":" + port + "/" + database);
	console.log("Database Connection in utilities.js", "postgres://" + username + ":" + password + "@" + host + ":" + port + "/" + database);								//here we have used native query as group by is not there in loopback
	//var db = pgp("postgres://postgres:postgres@10.0.1.73:5432/test");

	db.manyOrNone("SELECT leave_request_id, MAX(action_timestamp) AS action_timestamp FROM leave_workflow GROUP BY leave_request_id ORDER BY action_timestamp DESC")
  	.then(function (data) {
  		console.log("*********successfully done;",data);
        callback(null, data);
  	})
  	.catch(function (error) {
      console.log("ERROR:", error);
      callback(error);
  	});

};


var dateConflicts = function(req, callback){															//function to validate the leave request when a user applies a leave
	var fromDate = new Date(req.fromDate);
	var fromDate_new = new Date(fromDate.toDateString());
	var toDate = new Date(req.toDate);
	var toDate_new = new Date(toDate.toDateString());
	var count_total = 21;
	var count_date_match = 0;
	var leaveId;
	var typeOfLeaveRequest = req.type;
	console.log("###############",req.type);

	var approvedStatus;
	var pendingStatus;
	var cancelStatus;
	var deniedStatus;

	var leaveTypes;
	var privilege_eligible_count = 0;
	var maternity_eligible_count = 0;
	var paternity_eligible_count = 0;
	var compOff_eligible_count = 0;

	var count_privilege = 0;
	var count_paternity = 0;
	var count_maternity = 0;
	var count_compOff = 0;

	if(req.id === undefined){
		leaveId = null;
	}
	else{
		leaveId = req.id;
	}

	async.waterfall([
	function getLeaveStatusValue(callback){											//function to get the cancel leave status
		ErpKeysCode("leave_status", null, function(err, response){
			if(err){
				callback(err);
			}
			else{
				response.forEach(function(res){
					if(res.keyValue === "Approved"){
						approvedStatus = res.keyCode;
					}
					else if(res.keyValue === "Pending"){
						pendingStatus = res.keyCode;
					}
					else if(res.keyValue === "Denied"){
						deniedStatus = res.keyCode;
					}
					else if(res.keyValue === "Cancelled"){
						cancelStatus = res. keyCode;
					}
				});
				callback(null);
			}
		});
	},

	function getLeaveTypeCodes(callback){
		typeOfLeaves(null , function(err, response){
			if(err){
				return callback(err);
			}
			else{
				leaveTypes = response;
				console.log(leaveTypes);
				callback(null);
			}
		});
	},
	function findEligibleLeaveCount(callback){
		app.models.EmployeeLeaveEligibility.find({where: {employeeId: req.employeeId}},function(err, leaveEligibilities){
			if(err){
				callback(err);
			}
			else if(leaveEligibilities){
				leaveEligibilities.forEach(function(leaveEligibility){
					if(leaveEligibility.typeOfLeavesId === leaveTypes.privilegeId){
						privilege_eligible_count = leaveEligibility.eligibilityCount;
					}
					else if(leaveEligibility.typeOfLeavesId === leaveTypes.compOffId){
						compOff_eligible_count = leaveEligibility.eligibilityCount;
					}
					else if(leaveEligibility.typeOfLeavesId === leaveTypes.maternityId){
						maternity_eligible_count = leaveEligibility.eligibilityCount;
					}
					else if(leaveEligibility.typeOfLeavesId === leaveTypes.paternityId){
						paternity_eligible_count = leaveEligibility.eligibilityCount;
					} 
				});
				callback(null);
			}
		});
	},

	function checkingForDateConflicts(callback){														//function to check for date conflicts
		app.models.LeaveRequest.find({
		where: {and: [{employeeId: req.employeeId}, {status:{nin: [cancelStatus,deniedStatus]}}]},
		include: {relation: 'typeOfLeaves'}}
		,function(err, response){
			if(err){
				return callback(err);
			}
			var res_fromDate;
			var fromDate_db;
			var res_toDate;
			var toDate_db;
			var check_1;
			var check_2;
			var check_3;
			var check_4;
			response.forEach(function(res){																//to loop through all the leave requests to check for date matching 
				if(res.id === leaveId){
					console.log("Do nothing");
				}
				else{
					res_fromDate = new Date(res.fromDate);
					fromDate_db = new Date(res_fromDate.toDateString());
					res_toDate = new Date(res.toDate);
					toDate_db = new Date(res_toDate.toDateString());
					check_1 = moment(fromDate_new).isBefore(fromDate_db);
					check_2 = moment(toDate_new).isBefore(fromDate_db);
					check_3 = moment(fromDate_new).isAfter(toDate_db);
					check_4 = moment(toDate_new).isAfter(toDate_db);
					if((check_1&&check_2) || (check_3&&check_4)){}
					else{
						count_date_match++;	
					}
					if(res.typeOfLeaves().leaveType === "Privilege"){
						count_privilege+=res.numberOfDays;
					}
					else if(res.typeOfLeaves().leaveType === "Paternity"){
						count_paternity+=res.numberOfDays;
					}					
					else if(res.typeOfLeaves().leaveType === "Maternity"){
						count_maternity+=res.numberOfDays;
					}
					if(res.typeOfLeaves().leaveType === "Compoff"){
						count_compOff+=res.numberOfDays;
					}					
				}
			});
			if( ((count_privilege + req.numberOfDays) > privilege_eligible_count) && (req.type === "Privilege") ){			//validation for privilege leave count
				return callback("You have only " + (privilege_eligible_count-count_privilege) + " privilege leave available");
			}
			else if( ((count_paternity + req.numberOfDays) > paternity_eligible_count) && (req.type === "Paternity") ){			//validation for privilege leave count
				return callback("You have only " + (paternity_eligible_count-count_privilege) + " paternity leave available");
			}
			else if( ((count_maternity + req.numberOfDays) > maternity_eligible_count) && (req.type === "Maternity") ){			//validation for privilege leave count
				return callback("You have only " + (maternity_eligible_count-count_privilege) + " maternity leave available");
			}
			else if( ((count_privilege + req.numberOfDays) > compOff_eligible_count) && (req.type === "Compoff") ){			//validation for privilege leave count
				return callback("You have only " + (compOff_eligible_count-count_privilege) + " comp Off leave available");
			}

			if(count_date_match !== 0){
				return callback("Leave has been taken already for the same date");
			}
		//	console.log("inside util 2nd phase", req);
			callback(null);
		});
	}
	]
	,function(err, response){
		if(err){
			return callback(err);
		}
		else{
			callback(null, req);
		}
	});

};



// var updateSpecialLeaves = function(req, clb){													//function to update employee's leave data in case of special leaves
// 	var typeOfLeaveRequest = req.type;

// 	async.waterfall([
// 	function getAvailableSpecialLeaves(callback){
// 		if((typeOfLeaveRequest === "Paternity")||(typeOfLeaveRequest === "Maternity")||(typeOfLeaveRequest === "Comp-off")){
// 			app.models.EmployeeSpecialLeaveEligibility.find({									//function to get all the special leaves not taken by an employee
// 			where: {and: [{employeeId: req.employeeId},{typeOfLeavesId: req.typeOfLeavesId}, {status:{neq: 1}}]}
// 			},function(err, specialLeaves){
// 				if(err){
// 					return callback(err);
// 				}
// 				else{
// 					callback(null, specialLeaves);	
// 				}
// 			});
// 		}
// 		else{
// 			callback(null, null);
// 		}
// 	},

// 	function updateSpecialLeaveStatus(specialLeaves, callback){									//function to employee's special leave status field
// 		var requestedDays = req.numberOfDays;
// 		var count = 0;
// 		//console.log("specialLeaves", specialLeaves, requestedDays);
// 		if(specialLeaves !== null){
// 			if(requestedDays < 1){
// 				callback("SPECIAL LEAVES SHOULD BE IN INTEGER FORM");
// 			}
// 		//	console.log("start");
// 			if(typeOfLeaveRequest === "Comp-off"){												//to loop through so that applied comp off leaves status can be changed
// 				async.whilst(
// 				function(){ return requestedDays > 0;},
// 				function(cb){
// 					requestedDays -= 1;
// 					app.models.EmployeeSpecialLeaveEligibility.updateAll(
// 					{id:specialLeaves[count].id},{leaveRequestId: req.id, status: 1},function(err, response){
// 						if(err){
// 							return cb(err);
// 						}
// 						else{
// 							count++;
// 							cb(null);
// 						}
// 					});			
// 				},
// 				function(err, n){
// 					if(err){
// 						callback(err);
// 					}
// 					else{
// 						callback(null);
// 					}
// 				}
// 				);
// 			}
// 			else{
// 				app.models.EmployeeSpecialLeaveEligibility.updateAll(							//if it is not comp off it must be either paternity or comp off
// 				{id:specialLeaves[0].id},{leaveRequestId: req.id, status: 1},function(err, response){
// 					if(err){
// 						return callback(err);
// 					}
// 					else{
// 						callback(null);
// 					}
// 				});
// 			}
// 		}
// 		else{
// 			callback(null);
// 		}
// 		}
// 	]
// 	,function(err, response){																	//function returning result to calling function
// 		if(err){
// 			clb(err);
// 		}
// 		else{
// 			clb(null);
// 		}
// 	});
// };



var sendEmailNotificationToEmployee = function(emailData, cb){									//function to send email notification to the employee by manager in case of approve,sendback or deny
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/emailNotificationToEmployee.ejs', 'utf8');
	var subject = "Action on your leave request: " + emailData.leaveFromDate  + " To " + emailData.leaveToDate + ", Days: " + emailData.numberOfDays;
	var html = ejs.render(str, emailData);

    app.models.Email.send({																		//query to send the email
    	to: emailData.employeeEmail,
      	from: 'Manager',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail){
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
  };


  var sendEmailNotificationToManager = function(emailData, cb){									//function to send email notification to the manager in case of cancel or update
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/emailNotificationToManager.ejs', 'utf8');
	var subject = emailData.employeeName + ", Leave Request: " + emailData.leaveFromDate  + " To " + emailData.leaveToDate + ", Days: " + emailData.numberOfDays;
	var html = ejs.render(str, emailData);
	
    app.models.Email.send({																		//query to send the email notification
    	to: emailData.managerEmail,
      	from: 'Employee',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail) {
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
 };


var leaveApplicationNotification = function(emailData, cb){										//function to send an email notification to manager in case of an employee applies for a leave request
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/applyLeave.ejs', 'utf8');
	var subject = emailData.employeeName + ", Leave Request: " + emailData.leaveFromDate  + " To " + emailData.leaveToDate + ", Days: " + emailData.numberOfDays;
	var html = ejs.render(str, emailData);
	
    app.models.Email.send({																		//query to send the email notification
    	to: emailData.managerEmail,
      	from: 'Employee',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail){
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
 };


var sendEmailNotificationToManagerForCancellationRequest = function(emailData, cb){									//function to send email notification to the manager in case of cancel or update
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/emailNotificationToManager.ejs', 'utf8');
	var subject = emailData.employeeName + ", Leave Request: " + emailData.leaveFromDate  + " To " + emailData.leaveToDate + " - Cancellation Request ";
	var html = ejs.render(str, emailData);
	
    app.models.Email.send({																		//query to send the email notification
    	to: emailData.managerEmail,
      	from: 'Employee',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail) {
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
 };

var sendEmailNotificationToManagerForCancelledRequest = function(emailData, cb){									//function to send email notification to the manager in case of cancel or update
	console.log("Email data:- ", emailData);
	var str = fs.readFileSync(__dirname + '/../server/views/emailNotificationToManager.ejs', 'utf8');
	var subject = emailData.employeeName + ", Leave Request: " + emailData.leaveFromDate  + " To " + emailData.leaveToDate + " - Cancelled" ;
	var html = ejs.render(str, emailData);
	
    app.models.Email.send({																		//query to send the email notification
    	to: emailData.managerEmail,
      	from: 'Employee',
      	subject: subject,
     	text: "Hello",
      	html: html
    }
    ,function(err, mail) {
    	if(err){
    		cb(err);
    	}
    	else{
      		console.log('email sent!');
      		cb(null,"email sent");
      	}
    });
 };


var loginStatus = function(id, callback){														//function to check whether employee login for the firest time
	app.models.employee.findOne({where: {id: id},
	fields: {loginStatus: true}
	},function(err, employeeData){	
		if(err){
			return callback(err);
		}
		if(employeeData.loginStatus === '0'){
			return callback(null,0);
		}
		else{
			return callback(null,1);
		}
	});
};


var managerList = function(callback){															//function to find all the managers in an organization
	app.models.Employee.find({where: {reportingManagerId: {neq: null}},
	fields : {reportingManagerId: true, id: true}
	}, function(err, managerList){
		if(err){
			callback(err);
		}
		else{
			callback(null, managerList);
		}
	});
};

var userIdentification = function(cb){															//function to find the current user based on the access token
    var ctx = loopback.getCurrentContext();
    var currentUser = ctx && ctx.get('currentUser');
    var accessToken = ctx && ctx.get('accessToken');
    if(currentUser === undefined){
    	return cb(new Error("NOT A VALID USER"));
    }
  	else{
    	cb(null, accessToken);
  	}
};



var employeeDetail = function(employeeId, callback){
	app.models.Employee.findOne({
	where: {id: employeeId},
	include: {},
	fields: {id: true, reportingManagerId: true ,name: true, firstName: true, lastName:true, empId: true, phoneNo: true, designation: true,levelOfApprovalRequired: true, email: true}}, function(err, employeeData){
		if(err){
			return callback(err);
		}
		else{
			callback(null, employeeData);
		}
	});		
};


var getLastApprover = function(employeeId, level, reportingManagerId, callback){
	var data = {};
	console.log("Input Received: -", employeeId, level, reportingManagerId);
	var managerId = reportingManagerId;
	if(level !== 0){
	async.whilst(
		function(){
			if(level >= 0){
				return true;
			}
			else{
				return false;
			}
		},
		function getLastApproverId(clb){
			app.models.Employee.findOne({where : {id: employeeId}, fields: {empId:true, reportingManagerId: true}}, function(err, employeeData){
				if(err){
					clb(err);
				}
				else{
					if(level === 0){
						data = employeeData;
					}
					else{
						employeeId = employeeData.reportingManagerId;
						console.log('employeeId ', employeeId);
					}
					console.log("Data of last manager", data);
					console.log(" and level is ", level);
					level--;
					clb(null, data);
				}
			});
		},
		function(err, data){
			if(err){
				return callback(err);
			}
			else{
				console.log("Data of last manager", data);
				callback(null, data);
			}
		});
	}
	else if(level === 0){
	async.whilst(
		function(){
			if(managerId){
				return true;
			}
			else{
				return false;
			}
		},
		function getLastApproverId(clb){
			app.models.Employee.findOne({where : {id: employeeId}, fields: {empId:true, reportingManagerId: true}}, function(err, employeeData){
				if(err){
					return clb(err);
				}
				else{
					if(employeeData.reportingManagerId){
						employeeId = employeeData.reportingManagerId;
						managerId = employeeData.reportingManagerId;
					}
					else{
						data = employeeData;
						managerId = employeeData.reportingManagerId;
						console.log('employeeId ', employeeId, managerId);
					}
					console.log("Data of last manager", data);
					console.log(" and level is $$$$$", level);
					clb(null, data);
				}
			});
		},
		function(err, data){
			if(err){
				return callback(err);
			}
			else{
				console.log("Data of last manager", data);
				callback(null, data);
			}
		});	
	}
	else{
		callback(null, data);
	}

};


var typeOfLeaves = function(type, callback){
	var data = {};
	if(type){
		app.models.TypeOfLeaves.findOne({where: {leaveType: type}}, function(err, key){
			if(err){
				return callback(err);
			}
			else if(!key){
				return callback("Not A Valid Leave Type");
			}
			else{
				//console.log("Type of leave ",key);
				callback(null, key.id);
			}
		});
	}
	else{
		app.models.TypeOfLeaves.find(function(err, leaveTypes){
			if(err){
				return callback(err);
			}
			else if(leaveTypes){
				leaveTypes.forEach(function(type){
					if(type.leaveType === "Loss of Pay"){
						data.lossOfPayId = type.id; 
					}	
					else if(type.leaveType === "Privilege"){
						data.privilegeId = type.id;
					}
					else if(type.leaveType === "Compoff"){
						data.compOffId = type.id;
					}
					else if(type.leaveType === "Maternity"){
						data.maternityId = type.id;
					}
					else if(type.leaveType === "Paternity"){
						data.paternityId = type.id;
					}
				});
				callback(null, data);
			}
			else{
				callback(null)
			}
		});						
	}
};


exports.typeOfLeaves = typeOfLeaves ;
exports.getLastApprover = getLastApprover ;
exports.employeeDetail = employeeDetail ;
exports.userIdentification = userIdentification ;
exports.managerList = managerList ;
exports.loginStatus = loginStatus ;
exports.leaveApplicationNotification = leaveApplicationNotification;
exports.sendEmailNotificationToEmployee = sendEmailNotificationToEmployee ;
exports.sendEmailNotificationToManager = sendEmailNotificationToManager ;
exports.sendEmailNotificationToManagerForCancellationRequest = sendEmailNotificationToManagerForCancellationRequest ;
exports.sendEmailNotificationToManagerForCancelledRequest = sendEmailNotificationToManagerForCancelledRequest ;
exports.dateConflicts = dateConflicts;	
exports.latestTimestamp = latestTimestamp;
exports.sortWorkflow =sortWorkflow;
exports.ErpKeysCode = ErpKeysCode;
exports.specialLeaveDateCalculation = specialLeaveDateCalculation;
exports.date = date ;