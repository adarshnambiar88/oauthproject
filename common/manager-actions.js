var async = require('async');
var util = require('./utilities.js');
var moment = require('moment');
require('moment-weekday-calc');
/*
Data to be send:-
[{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Approve",
        "employeeId":”4"
}];
*/



var managerActions = function(actions, callback){                                                          //functions to track the managers action
	var currentDate = new Date().toLocaleString();
	var employeeData = {};
	var managerData = {};
	var actionTakenCode = 0;
	var actionToLeaveRequest;
	var emailData = {};

	async.forEachOf(actions, function(action, index, cb){

		action.actionTimestamp = currentDate;
		actionToLeaveRequest = action.actionByManager;
		employeeData = {};
		managerData = {};
		lastApproverData = {};

		async.waterfall([
		// function actionTakenCode(callback){																   //function to get the action taken code from the key table
		// 	util.ErpKeysCode("action_taken", action.actionByManager, function(err, res){
		// 		if(err){
		// 			return callback(err);
		// 		}
		// 		else{
		// 			var actionTakenCode = res.keyCode;
		// 			console.log("adiFirstPhase",actionTakenCode);
		// 			callback(null, actionTakenCode);
		// 		}
		// 	});
		// },

		function getEmployeeDetails(callback){												  //function to get the employee details (who has apllied this leave)
			util.employeeDetail(action.employeeId, function(err, employeeInformation){
				if(err){
					return callback(err);
				}
				else{
					employeeData = employeeInformation;
					console.log("getEmployeeDetails ", employeeData);
					callback(null);
				}
			});		
		},

		function getEmployeeLastLevelManager(callback){
			util.getLastApprover(action.employeeId, employeeData.levelOfApprovalRequired, employeeData.reportingManagerId, function(err, approverData){
				if(err){
					return callback(err);
				}
				else{
					lastApproverData = approverData;
					console.log("lastApproverData ", lastApproverData);
					callback(null);
				}
			});
		},

		function getManagerDetails(callback){															  //function to get the manager details (who is taking action)
			util.employeeDetail(action.fromEmployee, function(err, managerInformation){
				if(err){
					return callback(err);
				}
				else{
					managerData = managerInformation;
					console.log("managerData ", managerData);
					callback(null);
				}
			});						
		},

		function actionTakenCodeValue(callback){																   //function to get the action taken code from the key table
			util.ErpKeysCode("action_taken", action.actionByManager, function(err, res){
				if(err){
					return callback(err);
				}
				else{
					actionTakenCode = res.keyCode;
					console.log("actionTakenCode ",actionTakenCode);
					callback(null, actionTakenCode);
				}
			});
		},

		function settingToEmployeeField(actionTakenCode, callback){										   //function to set the to Employee field(cases:-when a manager hasn't/has any reporting manager)
			action.actionTaken = actionTakenCode ;
			if(action.actionByManager === "Approve"){
				app.models.Employee.findOne({
				where: {id: action.fromEmployee},
				fields: {empId: true, reportingManagerId: true}}, function(err, employeeData){
					if(err){
						return callback(err);
					}
					managerName = employeeData.name;
					if((employeeData.empId === lastApproverData.empId)||(!employeeData.reportingManagerId)){
					 	action.toEmployee = action.employeeId;
				 		action.lastApprover = 1;
				 		console.log("Last Approver set", action.lastApprover);
					}
					else{
				 		action.toEmployee = employeeData.reportingManagerId;
					}
					if(action.comments === ""){
						action.comments = " ";
					}
					//console.log("2ndphase",action);
					console.log("aciton", action);
					callback(null);
				});
			}
			else if((action.actionByManager === "Deny") || (action.actionByManager === "Sendback")){
				action.toEmployee = action.employeeId ;
				console.log("adiFirstPhase",action.toEmployee);
				callback(null);
			}
		},

		function makeEntryInLeaveWorkflow(callback){													   //function to add an leave workflow entry when a manager takes an action
			console.log("3rdphase:-",action);
			app.models.LeaveWorkflow.create(action , function(err, obj){
				if(err){
					console.log("Hey It's not working");
					console.log(err);
			 		callback(err);
			 	}
				else{
			 		callback(null);
			 	}
			});
		},

		// function getManagerDetails(callback){															  //function to get the manager details (who is taking action)
		// 	app.models.Employee.findOne({
		// 	where: {id: action.fromEmployee},
		// 	fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, managerData){
		// 		if(err){
		// 			return callback(err);
		// 		}
		// 		else{
		// 			callback(null, managerData);
		// 		}
		// 	});					
		// },

		// function getEmployeeDetails(managerData, callback){												  //function to get the employee details (who has apllied this leave)
		// 	app.models.Employee.findOne({
		// 	where: {id: action.employeeId},
		// 	fields: {email: true ,name: true, designation: true}}, function(err, employeeData){
		// 		if(err){
		// 			return callback(err);
		// 		}
		// 		else{
		// 			callback(null, employeeData, managerData);
		// 		}
		// 	});					
		// },

		function getLeaveRequest(callback){									 //function to get the leave request details
			app.models.LeaveRequest.findOne({
			where: {id: action.leaveRequestId}}, function(err, leave){
				if(err){
					return callback(err);
				}
				else{
					console.log("employee and manager data", employeeData, managerData);
					callback(null, leave);
				}
			});
		},
		function sendEmailNotification(leave, callback){						//function to send the notification to an employee about action taken on his/her leave request	
			emailData.actionByManager = actionToLeaveRequest;
			emailData.managerId = action.fromEmployee;
			emailData.managerEmpId = managerData.empId;
			emailData.managerName = managerData.name;
			emailData.managerEmail = managerData.email;
			emailData.employeeEmail = employeeData.email;
			emailData.managerDesignation = managerData.designation;
			emailData.comments = action.comments;
			emailData.numberOfDays = leave.numberOfDays;
			emailData.leaveFromDate = moment(leave.fromDate).format("MMM DD, YYYY");
			emailData.leaveToDate = moment(leave.toDate).format("MMM DD, YYYY");
			util.sendEmailNotificationToEmployee(emailData, function(err, response){
				if(err){
					return callback("Email Notification Failed");
				}
				else{
					console.log("Email Notification Sent");
				}
			});
			callback(null);
		}
		]
		,function(err, response){																		//function returning results from async waterfall
			if(err){
				return cb(err);
			}
			else{
				cb(null);
			}
		});	
	}
	,function(err){																						//function returning results from async forEach to calling funcion
		if(err){
			return callback(err);
		}
		else{
			callback(null);
		}
	});
};

exports.managerActions = managerActions;