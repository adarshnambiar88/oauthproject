var util = require('./utilities.js');
var async = require('async');
var moment = require('moment');
require('moment-weekday-calc');

var cancelPendingLeaveRequest = function(req, callback){
console.log("Request received:-", req);
var typeOfLeaveRequest = req.type;
var data = {};
var currentDate = new Date().toLocaleString();
var cancelLeaveStatus;
var actionTakenByEmployee;
var actionTakenByManager;
var employeeDetail = {};
var emailData = {};
	async.waterfall([
	function cancelLeaveStatusCode(callback){								//function to get the leave status of cancelled leave
		util.ErpKeysCode("leave_status", "Cancelled", function(err, key){
			if(err){
				return callback(err);
			}
			else{
				cancelLeaveStatus = key.keyCode;
				console.log("cancelLeaveStatusCode", cancelLeaveStatus);
				callback(null);
			}
		});
	},

	function updateLeaveStatus(callback){
		app.models.LeaveRequest.updateAll({id: req.id},{status: cancelLeaveStatus}, function(err, response){
			if(err){
				return callback(err);
			}
			else{
				console.log("updateLeaveStatus", response);
				callback(null);
			}
		});
	},

	// function updateSpecialLeaves(callback){											//function to update the special leaves
	// 	if((typeOfLeaveRequest === "Paternity")||(typeOfLeaveRequest === "Maternity")||(typeOfLeaveRequest === "Comp-off")){
	// 		app.models.EmployeeSpecialLeaveEligibility.updateAll(					//update query
	// 		{leaveRequestId:req.id}, {status: 0, leaveRequestId: null}, function(err, response){
	// 			if(err){
	// 				return callback(err);
	// 			}
	// 			else{
	// 				console.log("updateSpecialLeaves");
	// 				callback(null);
	// 			}
	// 		});
	// 	}
	// 	else{
	// 		console.log("updateSpecialLeaves");
	// 		callback(null);
	// 	}
	// },

	function cancelActionTakenCode(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("action_taken", "Cancel", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					actionTakenByEmployee = key.keyCode;
					console.log("cancelActionTakenCode", actionTakenByEmployee);
					callback(null);
				}
			});
	},
	function approveActionTakenCode(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("action_taken", "Approve", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					actionTakenByManager = key.keyCode;
					console.log("approveActionTakenCode", actionTakenByManager);
					callback(null);
				}
			});
	},
	function getEmployeeDetails(callback){
		util.employeeDetail(req.employeeId, function(err, employeeInformation){
			if(err){
				return callback(err);
			}
			else{
				employeeDetail = employeeInformation;
				console.log("getEmployeeDetails", employeeDetail);
				callback(null, employeeDetail);
			}
		});
	},

	function makeLeaveWorkflowEntry(employeeDetail, callback){
		data.actionTaken = actionTakenByEmployee;
		data.actionTimestamp = currentDate;
		data.comments = req.comments;
		data.leaveRequestId = req.id;
		data.toEmployee = employeeDetail.reportingManagerId;
		data.fromEmployee = employeeDetail.id;
		app.models.LeaveWorkflow.create(data, function(err, response){
			if(err){
				return callback(err);
			}
			else{
				console.log("makeLeaveWorkflowEntry", response);
				callback(null, employeeDetail);
			}
		});
	},

	function findLeaveWorkflowAttachedWithLeaveId(employeeDetail, callback){
		app.models.LeaveWorkflow.find({where: {leaveRequestId: req.id}, order: 'actionTimestamp'}, function(err, leaveWorkflows){
			if(err){
				return callback(err);
			}
			else{
				console.log("findLeaveWorkflowAttachedWithLeaveId", leaveWorkflows);
				callback(null, leaveWorkflows);
			}
		});
	},

	function sendEmailNotification(leaveWorkflows, callback){
		console.log("sendEmailNotification", employeeDetail);
		async.each(leaveWorkflows, function(leaveWorkflow, clb){
			if((leaveWorkflow.actionTaken === actionTakenByManager)&&(leaveWorkflow.toEmployee !== employeeDetail.id)){
				async.waterfall([
				// function getEmployeeDetails(callback){
				// 	util.employeeDetail(leaveWorkflow.fromEmployee, function(err, employeeDetail){
				// 		if(err){
				// 			return callback(err);
				// 		}
				// 		else{
				// 			callback(null, employeeDetail);
				// 		}
				// 	});
				// },

				function getManagerDetails(callback){
					util.employeeDetail(leaveWorkflow.fromEmployee, function(err, managerDetail){
						if(err){
							return callback(err);
						}
						else{
							callback(null,managerDetail);
						}
					});
				},

				function sendEmailNotificationToManager(managerDetail, callback){
					emailData.actionByEmployee = "Cancelled A LeaveRequest";
					emailData.employeeEmpId = employeeDetail.empId;
					emailData.employeeName = employeeDetail.name;
					emailData.managerEmail = managerDetail.email;
					emailData.employeeEmail = employeeDetail.email;
					emailData.employeeDesignation = employeeDetail.designation;
					emailData.comments = req.comments;
					emailData.leaveFromDate = moment(req.fromDate).format("MMM DD, YYYY");
					emailData.leaveToDate = moment(req.toDate).format("MMM DD, YYYY");
					emailData.numberOfDays = req.numberOfDays;
					util.sendEmailNotificationToManagerForCancelledRequest(emailData, function(err, response){
						if(err){
							return callback(err);
						}
					});
					callback(null);
				}
				], function(err, response){
					if(err){
						return clb(err);
					}
					else{
						clb(null);
					}
				});
			}
			else{
				clb(null);
			}
		}, function(err){
			if(err){
				return callback(err);
			}
			else{
				callback(null);
			}
		});
		
	}
	], function(err, response){
		if(err){
			callback(err);
		}
		else{
			callback(null)
		}
	});
};

exports.cancelPendingLeaveRequest = cancelPendingLeaveRequest;