var util = require('./utilities.js');
var async = require('async');
var leaveSummary = require('./leave-statistics.js');

var getEmployeeList = function(callback){
	var employeeList = [];
	var data = {};
	async.waterfall([
	function getlist(callback){
		app.models.Employee.find({fields: {id: true, name: true, firstName: true, lastName: true, empId: true, email: true, phoneNo: true, designation: true, active: true}, order: 'firstName'}, function(err, employeeData){
			if(err){
				return callback(err);
			}
			else{
				//console.log("EmployeeData", employeeData);
				callback(null, employeeData);
			}
		});
	},

	// function getDesignation(employeeData, callback){
	// 	//console.log(employeeData);
	// 	async.eachSeries(employeeData, function(employee, clb){
	// 		//console.log("Employee", employee);
	// 		app.models.ErpKeys.findOne({where: {and: [{type: "designation"},{keyCode: employee.designation}]}}, function(err, employeeDesignation){
	// 			if(err){
	// 				return clb(err);
	// 			}
	// 			else{
	// 				data = {};
	// 				data.name = employee.name;
	// 				data.email = employee.email;
	// 				data.phoneNo = employee.phoneNo;
	// 				data.firstName = employee.firstName;
	// 				data.lastName = employee.lastName;
	// 				data.empId = employee.empId;
	// 				data.id = employee.id;
	// 				data.active = employee.active;
	// 				data.designation = employeeDesignation.keyValue;
	// 				//console.log("Data", data);
	// 				employeeList.push(data);
	// 				clb(null);
	// 			}
	// 		});
	// 	}, function(err){
	// 		if(err){
	// 			return callback(err);
	// 		}
	// 		else{
	// 			callback(null, employeeList);
	// 		}
	// 	});
	// },

	function getEmployeeLeaveStatistics(employeeData, callback){
		async.eachSeries(employeeData, function(employee, clb){
			leaveSummary.leaveStatistics(employee.id, function(err, statistics){
				if(err){
					return clb(err);
				}
				else{
					data = {};
					app.models.ErpKeys.findOne({where: {and: [{type: "designation"},{keyCode: employee.designation}]}}, function(err, employeeDesignation){
			 			if(err){
			 				return clb(err);
			 			}
			 			if(employeeDesignation){
			 				data.designation = employeeDesignation.keyValue;
			 			}
			 			data.name = employee.name;
			 			data.email = employee.email;
			 			data.phoneNo = employee.phoneNo;
			 			data.firstName = employee.firstName;
						data.lastName = employee.lastName;
		 				data.empId = employee.empId;
		 				data.id = employee.id;
		 				data.active = employee.active;
			 			data.leaveStatistics = statistics;
			 			//console.log("Data", data);
			 			employeeList.push(data);
			 			clb(null);
	 				});
				}
			});
		}, function(err){
			if(err){
				return callback(err);
			}
			else{
				callback(null, employeeList);
			}
		});
	}
	], function(err, employeeList){
		if(err){
			return callback(err);
		}
		else{
			callback(null, employeeList);
		}
	});

}; 


exports.getEmployeeList = getEmployeeList;