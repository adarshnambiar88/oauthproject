var async = require('async');
var util = require('./utilities.js');
var moment = require('moment');
require('moment-weekday-calc');


var setActiveFieldOfEmployee = function(data, callback){
    console.log("DATA", data.employeeId);
    //var currentDate = moment(new Date()).format("YYYY-MM-DD");
    var currentDate = new Date();
    async.waterfall([
      function getEmployee(callback){
        app.models.Employee.findOne({where: {id: data.employeeId}}, function(err, user){                         //query to find the user details
          if(err){
            return callback(err);
          }
          else{
            callback(null, user);
          }
        });
      },

      function updateActiveStatus(user, callback){
        user.updateAttribute('active', data.status, function(err, res){                        //query to update the user password
          if(err){
              return callback(err);
          }
          else{
            callback(null, user);
          }
        });
      },

      function setActiveInactiveDate(user, callback){
        console.log("ADarsh",user);
        user.updateAttribute('activeInactiveDate', currentDate, function(err, res){                        //query to update the user password
          if(err){
              console.log("Error in setting date of occurrence", currentDate);
              return callback(err);
          }
          else{
            console.log("Successfull in setting date of occurrence");
            callback(null);
          }
        });        
      }

      ], function(err, response){
        if(err){
          return callback(err);
        }
        else{
          callback(null);
        }
    });
};



var updateLeaveEligibility = function(data, callback){
  var employeeId = data.employeeId;
  //console.log("@@@@@@@@@", data);
  var leaveEligibility = data.leaveEligibility;
  var newEligibilityData = {};
  var leaveTypes = {};
  var type;
  async.waterfall([
    function getTypeOfLeaves(cb){
        util.typeOfLeaves(null, function(err, response){
          if(err){
            return cb(err);
          }
          else{
            leaveTypes = response;
            cb(null);
          }
        });
    },

    function getleaveEligibility(clb){
      //console.log("aaa", leaveEligibility);
      async.eachSeries(leaveEligibility, function(eligibleLeaveData, cb){
        //console.log("opo", eligibleLeaveData);
        if(eligibleLeaveData.typeOfLeaves === "Privilege"){
          type = leaveTypes.privilegeId;
        }
        else if(eligibleLeaveData.typeOfLeaves === "Maternity"){
          type = leaveTypes.maternityId; 
        }
        else if(eligibleLeaveData.typeOfLeaves === "Paternity"){
          type = leaveTypes.paternityId; 
        }
        app.models.EmployeeLeaveEligibility.findOne({where: {and: [{employeeId: employeeId}, {eligibilityYear: eligibleLeaveData.eligibilityYear}, {typeOfLeavesId: type}]}}, function(err, currentLeaveEligibilityData){
          if(err){
            return cb(err);
          }
          else if(currentLeaveEligibilityData){
            console.log("leaveEligibilityData", currentLeaveEligibilityData);
            app.models.EmployeeLeaveEligibility.updateAll({id: currentLeaveEligibilityData.id}, {eligibilityCount: eligibleLeaveData.eligibilityCount}, function(err, response){
              if(err){
                return cb(err);
              }
              else{
                cb(null);
              }
            });
          }
          else{
            newEligibilityData.eligibilityYear = eligibleLeaveData.eligibilityYear;
            newEligibilityData.eligibilityCount = eligibleLeaveData.eligibilityCount;
            newEligibilityData.employeeId = employeeId;
            newEligibilityData.typeOfLeavesId = type;

            console.log("NOt have any entry", newEligibilityData);
            app.models.EmployeeLeaveEligibility.create(newEligibilityData, function(err, response){
              if(err){
                return cb(err);
              }
              else{
                console.log("new Data Added Successfully");
                cb(null);
              }   
            });
          }
         });
    },function(err){
      if(err){
        return clb(err);
      }
      else{
        clb(null);
      }
    });
    }
    ], function(err, resposne){
      if(err){
        return callback(err);
      }
      else{
        callback(null);
      }
  });
};

exports.updateLeaveEligibility = updateLeaveEligibility;
exports.setActiveFieldOfEmployee = setActiveFieldOfEmployee;