var util = require('./utilities.js');
var async = require('async');
var _ = require('underscore');
var leaveSummary = require('./leave-statistics.js');

var getTeam = function(id, employeeList, callback){
	var teamList = [];
	console.log("employeeList",employeeList);
	async.eachSeries(employeeList, function(employee, clb){
      	app.models.Employee.find({where: {and: [{reportingManagerId: employee}, {id: {neq: id}}]}, fields: {id: true, empId: true, firstName: true}, order: 'firstName ASC'}, function(err, employees){
			if(err){
				return clb(err);
			}
			else{
				console.log("employees", employees);
				if(employees.length > 0){
					employees.forEach(function(employee){
						if(employee.empId !== 'P10E0001' && employee.empId !== 'P10E0002'){
							teamList.push(employee.id);
						}
					});
					clb(null);
				}
				else{
					clb(null);
				}
			}
		});
    }, function(err){
        if(err){
        	return callback(err);
        }
        else{
        	console.log("teamList", teamList);
            callback(null, teamList);
        }
   	});
};



var getTeamMemberList = function(id, cb){
	var teamList = [];
    var managerList = [];
    console.log("Manager ID",id);
    managerList.push(id);
    console.log("managerList.length", managerList.length,managerList);

    async.whilst(                                                                 //while-loop to find the list of approvers 
      function(){
        if(managerList.length > 0){
          console.log("managerList.length", managerList.length, managerList);
          return true;
        }
        else{
          return false;
        }
      },

      function getTheTeamMembers(callback){
        getTeam(id, managerList, function(err, teamMembers){
          if(err){
            return callback(err);
          }
          else{
            managerList = [];
            console.log("teamMembers", teamMembers,id);
            if(teamMembers.length > 0){
              managerList = teamMembers;
              teamMembers.forEach(function(teamMember){
              	teamList.push(teamMember);	
              });
            }
            console.log("managerList and teamList", managerList, teamList);
            callback(null, teamList);
          }               
        });
      },

      function(err, data){
          if(err){
            return cb(err);
          }
          else{ 
            console.log("list of managers",data);
            cb(null, data);
          }
      }
    ); 
};



var getTeamMemberInformation = function(id, callback){
	console.log("take the Id", id);
	var teamMember = [];
	var teamMemberDetails = {};
	async.waterfall([
	function getListOfMembers(callback){
		getTeamMemberList(id, function(err, teamList){
			if(err){
				return callback(err);
			}
			else{
				console.log("team List", teamList);
				callback(null, teamList);
			}
		});

	},
	function getTeamMember(teamList, callback){
		async.eachSeries(teamList, function(member, clb){
			util.employeeDetail(member, function(err, response){
				if(err){
					return clb(err);
				}
				else{
					app.models.ErpKeys.findOne({where: {and: [{type: "designation"}, {keyCode: response.designation}]}}, function(err, key){
						if(err){
							return clb(err);
						}
						else{
							teamMemberDetails  = {};
							teamMemberDetails.id = response.id;
							teamMemberDetails.name = response.name;
							teamMemberDetails.firstName = response.firstName;
							teamMemberDetails.lastName = response.lastName;
							teamMemberDetails.phoneNo = response.phoneNo;
							teamMemberDetails.email = response.email;
							teamMemberDetails.empId = response.empId;
							if(key){
								teamMemberDetails.designation = key.keyValue;
							}
							teamMember.push(teamMemberDetails);
							//console.log("teamMemberDetails", member, response, teamMemberDetails);
							clb(null);	
						}
					}); 
				}
			});
		}, function(err){
			if(err){
				return callback(err);
			}
			else{
				callback(null, teamMember);
			}
		});
	},
	function getTeamMemberLeaveStatistics(teamMember, callback){
		teamMember = _.sortBy(teamMember, 'firstName');
		console.log(teamMember);
		async.eachSeries(teamMember, function(member, clb){
			leaveSummary.leaveStatistics(member.id, function(err, statistics){
				if(err){
					return clb(err);
				}
				else{
					member.leaveStatistics = statistics;
					clb(null);
				}
			});
		}, function(err){
			if(err){
				return callback(err);
			}
			else{
				callback(null, teamMember);
			}
		});
	}
	], function(err, data){
		if(err){
			return callback(err);
		}
		else{
			console.log("team teamMember");
			callback(null, data);
		}
	});
};



exports.getTeamMemberInformation = getTeamMemberInformation;
exports.getTeamMemberList = getTeamMemberList;
exports.getTeam = getTeam;