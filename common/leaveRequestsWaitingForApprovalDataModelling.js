var util = require('./utilities.js');
var async = require('async');
var leaveRequestsWaitingForApproval = require('./leaveRequestsWaitingForApproval.js');
var moment = require('moment');
require('moment-weekday-calc');


var leaveRequestsWaitingForApprovalDataModelling = function(id, cb){
	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var todayLeaves = [];
	var yesterdayLeaves = [];
	var thisWeekLeaves = [];
	var thisMonthLeaves = [];
	var janLeaves = [];
	var febLeaves = [];
	var marchLeaves = [];
	var aprilLeaves = [];
	var mayLeaves = [];
	var juneLeaves = [];
	var julyLeaves = [];
	var augLeaves = [];
	var septLeaves = [];
	var octLeaves = [];
	var novLeaves = [];
	var decLeaves = [];
	var leaveData = [];
	async.waterfall([
	function leaveRequestsForApproval(callback){
		leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(id, function(err, data){
			if(err){
				return callback(err);
			}
			else{
				callback(null, data);
			}
		});
	},
	function orderingLeavesWithReferenceToAppliedOnField(data, callback){
		var todayDate = new Date();
		var formattedTodayDate = moment(new Date()).format("YYYY-MM-DD");
		var yesterdayDate = new Date(moment(todayDate).subtract(1, 'days'));
		var formattedYesterdayDate = moment(yesterdayDate).format("YYYY-MM-DD");
		//console.log("currentDate", todayDate);
		//console.log("yesterdayDate", yesterdayDate);
		var appliedOn;
		var formattedAppliedOn;
		async.each(data, function(leaveData, clb){
			appliedOn = new Date(leaveData.appliedOn);
			formattedAppliedOn = moment(new Date(leaveData.appliedOn)).format("YYYY-MM-DD");
			//console.log("AppliedOn",appliedOn);
			//console.log("currentDate", todayDate);
			//console.log("yesterdayDate", yesterdayDate);
			if(moment(formattedAppliedOn).isSame(formattedTodayDate)){
				todayLeaves.push(leaveData);
			}
			else if(moment(formattedAppliedOn).isSame(formattedYesterdayDate)){
				yesterdayLeaves.push(leaveData);
			}
			else if(moment(appliedOn).week() === moment(todayDate).week()){
				thisWeekLeaves.push(leaveData);
			}
			else if(appliedOn.getMonth() === todayDate.getMonth()){
				thisMonthLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "January"){
				janLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "February"){
				febLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "March"){
				marchLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "April"){
				aprilLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "May"){
				mayLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "June"){
				juneLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "July"){
				julyLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "August"){
				augLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "September"){
				septLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "October"){
				octLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "November"){
				novLeaves.push(leaveData);
			}
			else if(month[appliedOn.getMonth()] === "December"){
				decLeaves.push(leaveData);
			}
			clb(null);
		}, function(err){
			if(err){
				return callback(err);
			}
			else{
				callback(null);
			}
		});
	},
	function dataModelling(callback){
		if(todayLeaves.length > 0){
			var todayData = {};
			todayData.title = "Today";
			todayData.data = todayLeaves;
			leaveData.push(todayData);
		}
		if(yesterdayLeaves.length > 0){
			var yesterdayData = {};
			yesterdayData.title = "Yesterday";
			yesterdayData.data = yesterdayLeaves;
			leaveData.push(yesterdayData);
		}
		if(thisWeekLeaves.length > 0){
			var thisWeekData = {};
			thisWeekData.title = "This week";
			thisWeekData.data = thisWeekLeaves;
			leaveData.push(thisWeekData);
		}
		if(thisMonthLeaves.length > 0){
			var thisMonthData = {};
			thisMonthData.title = "This month";
			thisMonthData.data = thisMonthLeaves;
			leaveData.push(thisMonthData);
		}
		if(decLeaves.length > 0){
			var decData = {};
			decData.title = "December";
			decData.data = decLeaves;
			leaveData.push(decData);
		}	
		if(novLeaves.length > 0){
			var novData = {};
			novData.title = "November";
			novData.data = novLeaves;
			leaveData.push(novData);
		}
		if(octLeaves.length > 0){
			var octData = {};
			octData.title = "October";
			octData.data = octLeaves;
			leaveData.push(octData);
		}
		if(septLeaves.length > 0){
			var septData = {};
			septData.title = "September";
			septData.data = septLeaves;
			leaveData.push(septData);
		}
		if(augLeaves.length > 0){
			var augData = {};
			augData.title = "August";
			augData.data = augLeaves;
			leaveData.push(augData);
		}	
		if(julyLeaves.length > 0){
			var julyData = {};
			julyData.title = "July";
			julyData.data = julyLeaves;
			leaveData.push(julyData);
		}
		if(juneLeaves.length > 0){
			var juneData = {};
			juneData.title = "June";
			juneData.data = juneLeaves;
			leaveData.push(juneData);
		}	
		if(mayLeaves.length > 0){
			var mayData = {};
			mayData.title = "May";
			mayData.data = mayLeaves;
			leaveData.push(mayData);
		}
		if(aprilLeaves.length > 0){
			var aprilData = {};
			aprilData.title = "April";
			aprilData.data = aprilLeaves;
			leaveData.push(aprilData);
		}
		if(marchLeaves.length > 0){
			var marchData = {};
			marchData.title = "March";
			marchData.data = marchLeaves;
			leaveData.push(marchData);
		}	
		if(febLeaves.length > 0){
			var febData = {};
			febData.title = "February";
			febData.data = febLeaves;
			leaveData.push(febData);
		}	
		if(janLeaves.length > 0){
			var janData = {};
			janData.title = "January";
			janData.data = janLeaves;
			leaveData.push(janData);
		}
		callback(null, leaveData);
	}
	]
	,function(err, response){
		if(err){
			cb(err);
		}
		else{
			//console.log("****************", response);
			cb(null, response);
		}
	});
};

exports.leaveRequestsWaitingForApprovalDataModelling = leaveRequestsWaitingForApprovalDataModelling;
