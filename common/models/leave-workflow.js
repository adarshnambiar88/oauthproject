var async = require('async');
var util = require('../utilities.js');
module.exports = function(LeaveWorkflow){

	LeaveWorkflow.afterRemote('**',function(context, user, next){		
		console.log("-----------------------------",context.methodString);
		next();
	});


	LeaveWorkflow.getLeaveWorkflows = function(leaveId, employeeId, cb){												//API to get the leave workflow attached with all leave request of an employee 
		var data = [];
		var approvedStatus;
		var leaveStatus;
		//var lastApproverId ;
		console.log();
		async.waterfall([
		function getLeaveStatus(callback){
			app.models.LeaveRequest.findOne({where: {id: leaveId}, fields: {status: true}}, function(err, res){
				if(err){
					return callback(err);
				}
				else{
					leaveStatus = res.status;
					callback(null);
				}
			});
		},

		// function getEmployeeDetails(callback){
		// 	util.employeeDetail(employeeId, function(err, employeeData){
		// 		if(err){
		// 			return callback(err);
		// 		}
		// 		else{
		// 			console.log("employeeData", employeeData.id, employeeData.levelOfApprovalRequired);
		// 			callback(null, employeeData);
		// 		}
		// 	});
		// },		

		// function getLastApprover(employeeData, callback){
		// 	util.getLastApprover(employeeData.id, employeeData.levelOfApprovalRequired, employeeData.reportingManagerId, function(err, lastApproverData){
		// 		if(err){
		// 			return callback(err);
		// 		}
		// 		else{
		// 			lastApproverEmpId = lastApproverData.empId; 
		// 			console.log("lastApproverEmpId", lastApproverEmpId);
		// 			callback(null);
		// 		}
		// 	});
		// },

		function approvedStatusValue(callback){														//function to get all the key_codes for action_taken(approve,deny or send back) 
			util.ErpKeysCode("leave_status", "Approved"
			,function(err, result){
				if(err){
					return callback(err);
				}
				else{
					approvedStatus = result.keyCode;
					console.log("approvedStatusValue", approvedStatus);
					callback(null);
				}
			});
		},

		function actionTakenValue(callback){														//function to get all the key_codes for action_taken(approve,deny or send back) 
			util.ErpKeysCode("action_taken", null
			,function(err, actionTakenValues){
				if(err){
					return callback(err);
				}
				else{
					callback(null, actionTakenValues);
				}
			});
		},

		function getWorkflow(actionTakenValues ,callback){											//function to get the leaveworkflow wrt a leave id
			LeaveWorkflow.find({
			where:{leaveRequestId: leaveId},
			include: {
				relation: 'employee',
				scope:{
					fields: ['id', 'name', 'firstName', 'lastName', 'empId', 'reportingManagerId']
				}
			},
			order: 'actionTimestamp DESC'
			}
			,function(err, leaveWorkflows){
				if(err){
					return callback(err);
				}
				else{
					callback(null, actionTakenValues, leaveWorkflows);
				}
			});
		},

		function modellingWorkflow(actionTakenValues, leaveWorkflows, callback){					 //formatting leave workflow to be send 
			var workflow = {};
			async.forEachOfSeries(leaveWorkflows
			,function(leaveWorkflow, index, cb){
				//console.log(leaveWorkflow);
				//console.log("index ", index);
				workflow = {};
				workflow.fromEmployee = leaveWorkflow.employee().name;
				workflow.fromEmployeeFirstName = leaveWorkflow.employee().firstName;
				workflow.fromEmployeeLastName = leaveWorkflow.employee().lastName;
				workflow.toEmployee = leaveWorkflow.toEmployee;
				workflow.actionTaken = leaveWorkflow.actionTaken;
				workflow.actionTimestamp = leaveWorkflow.actionTimestamp;

				if(leaveWorkflow.comments!==" "){
					workflow.comments = leaveWorkflow.comments;
				}
				// if(leaveWorkflow.employee().id === leaveWorkflow.toEmployee){
				// 	workflow.lastApprover = 1;
				// }
				// if(leaveWorkflow.employee().empId === lastApproverEmpId){
				// 	workflow.lastApprover = 1;
				// }
				//console.log("index and status", index, leaveStatus);
				if((index === 0) && (leaveStatus === approvedStatus)){
					workflow.lastApprover = 1;
				}
				actionTakenValues.forEach(function(actionTakenValue){
					if(actionTakenValue.keyCode === workflow.actionTaken){
						workflow.actionTaken = actionTakenValue.keyValue;
					}
				});
				data.push(workflow);
				cb(null);
			}
			,function(err){																				//function returning from async.forEachOf
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		}
		]
		,function(err, response){																		//function returning from getLeaveWorkflows method
			if(err){
				return cb(err);
			}
			else{		
				cb(null, data);
			}
		});

	};


	LeaveWorkflow.remoteMethod('getLeaveWorkflows',									//Registering the getLeaveWorkflows Method in the list of Api's
	 	{
	 		http :{path:'/getLeaveWorkflows', verb: 'get'},
	 		accepts:[{arg:'leaveId', type: 'string', required: true},
	 				 {arg:'employeeId', type: 'string', required: true}],
	 		returns: {type: 'string', root: true }
	 	}
	);

/*
var data = {															
			"fromEmployee": req.employeeId,				
			"toEmployee": req.approverId,
			"actionTaken": key.keyCode,
			"actionTimestamp": currentDate,
			"comments": req.comments,
			"leaveRequestId": req.id
			};

*/


LeaveWorkflow.observe('after save', function(ctx, next){						//function to change the status of leave request after last level manager has taken action on a particular leave request
		var req = ctx.instance; 
		var cancellationInProgressCode;													//or a particular manager has denied the leave request
		console.log("entry after save in leaveWorkflow.observe method",req);
		var leaveStatus;
		var  leaveStatusCode;
		if((req.actionByManager=== "Approve" && req.lastApprover=== 1) || (req.actionByManager=== "Deny")){	
			async.waterfall([
			function cancellationInProgressCodeValue(callback){
				util.ErpKeysCode("leave_status", "Cancellation In Progress", function(err, key){
					if(err){
						return callback(err);
					}
					else{
						cancellationInProgressCode = key.keyCode;
						console.log("cancellationInProgressCode", cancellationInProgressCode);
						callback(null, cancellationInProgressCode);
					}
				});
			},
			function getLeaveRequest(cancellationInProgressCode, callback){
				app.models.LeaveRequest.findOne({where: {id: req.leaveRequestId}}, function(err, leaveRequest){
					if(err){
						return callback(err);
					}
					else{
						console.log("getLeaveRequest", leaveRequest);
						callback(null, leaveRequest);
					}
				});
			},
			function findLeaveStatus(leaveRequest, callback){
				if(req.actionByManager=== "Approve"){
					leaveStatus = "Approved";
				}
				else{
					leaveStatus = "Denied";
				}
				if(leaveRequest.status === cancellationInProgressCode){
					leaveStatus = "Cancelled";
				}
				if((leaveRequest.status === cancellationInProgressCode) && (req.actionByManager=== "Deny")){
					leaveStatus = "Approved";
				}
				util.ErpKeysCode("leave_status", leaveStatus
				,function(err, res){												//function to find the leave status code for approved or denied
					if(err){
						return callback(err);
					}
					else{
						leaveStatusCode = res.keyCode;
						console.log("findLeaveStatus", leaveStatusCode);
						callback(null);
					}
				});	
			},

			function leaveStatusUpdate(callback){									//function to update the leave status in the leave request table
				app.models.LeaveRequest.updateAll({
				id: req.leaveRequestId},{status: leaveStatusCode}
				,function(err, response){
					if(err){
						return callback(err);
					}
					else{
						console.log("leaveStatusUpdate", response);
						callback(null);
					}
				});
			}
			// ,

			// function specialLeaveUpdate(callback){									//function to update the special leave table if action taken is deny
			// 	if(leaveStatus === "Denied" || leaveStatus === "Cancelled"){
			// 		app.models.EmployeeSpecialLeaveEligibility.updateAll(
			// 		{leaveRequestId:req.leaveRequestId}, {status: 0, leaveRequestId: null} ,function(err, response){
			// 			if(err){
			// 				return callback(err);
			// 			}
			// 			else{
			// 				callback(null);
			// 			}
			// 		});
			// 	}
			// 	else{
			// 		callback(null);
			// 	}
			// }
			]
			,function(err, response){												 //function returning from leaveWorkflow after save											
				if(err){
					next(err);
				}
				else{
					next();
				}
			});
		}
		else{
			next();
		}
	});

};
