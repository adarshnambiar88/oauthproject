module.exports = function(UserIdentity) {


  	UserIdentity.observe('before save', function(ctx, next){
  		//console.log(ctx.currentInstance);
  		if(ctx.currentInstance){						
			var reqEmail = ctx.currentInstance.profile.emails[0].value;
			//console.log("reqEmail",reqEmail);
			app.models.Employee.findOne({where: {email: reqEmail}}, function(err, employee){
				if(err){
					console.log("Not found an employee");
					return next(err);
				}
				else if(employee){
					console.log("before save in UserIdentity", reqEmail);
					next();
				}
				else{
					return next("Not An Valid Employee");
				}
			});
		}
		else{
			var reqEmail = ctx.instance.profile.emails[0].value;
			console.log("reqEmail",reqEmail);
			app.models.Employee.findOne({where: {email: reqEmail}},function(err, employee){
				if(err){
					return next(err);
				}
				else if(employee){
					next();
				}
				else{
					return next("Not An Valid Employee");
				}
			});
		}
	});



 	UserIdentity.observe('after save', function(ctx, next){						
	 	var reqEmail = ctx.instance.profile.emails[0].value;
	 	var userId = ctx.instance.userId; 
	 	console.log(ctx.instance.userId);
	 	app.models.Employee.findOne({where: {email: reqEmail}}, function(err, employee){
	 		if(err){
	 			return next(err);
	 		}
	 		else if(employee){
	 			console.log("employee", employee.id);
	 			console.log("userId", userId);
	 			app.models.Employee.updateAll({id: employee.id}, {userId: userId}, function(err, response){
	 				if(err){
	 					return next(err);
	 				}
	 				else{
	 					console.log("response",response);
	 					next();
	 				}
	 			});
	 		}
	 		else{
	 			next();
	 		}
	 	});
	});
};
