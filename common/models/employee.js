var loopback = require('loopback');
var async = require('async'); 
var getTeamMemberInformation = require('../getTeamMemberInformation');
var profileInformation = require('../getProfileInformation');
var getApprovers = require('../getApprovers');
var getEmployeeList = require('../getEmployeeList');
var adminFunctionalities = require('../adminFunctionalities');
var config = require('../../server/config.js');
var util = require('../utilities.js');

module.exports = function(Employee) {
  var count = 0;
  Employee.afterRemote('**',function(context, user, next){   
    console.log("-----------------------------",context.methodString,++count);
    next();
  });

  Employee.on('resetPasswordRequest', function(info) {
    var environment = "QACloudEnvironment";
    var host = config[environment].passwordResetHost;
    var port = config[environment].passwordResetPort;                                //Function to send password reset link to the email id
    var url = 'http://' + host + ':' + port + '/#/password_reset';
    console.log("URL in Employee", url);
    var html = 'Click <a href="' + url + '?id=' +info.accessToken.userId + '&access_token=' + info.accessToken.id + '">here</a> to reset your password';
    //'here' in above html is linked to : 'http://<host:port>/reset-password?access_token=<short-lived/temporary access token>'
    
    app.models.Email.send({                                                  //sending email 
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    },function(err){
      if (err){
        return console.log('Error sending password reset email');
      }
      else{
        console.log('> sending password reset email to:', info.email);
      }
    });
  });



  Employee.getApprovars = function(id, callback){                                           //Function to get the approvers for a particular employee
    getApprovers.getApproverNames(id, function(err, response){
      if(err){
        return callback(err);
      }
      else{
        callback(null, response);
      }
  });

  };


Employee.remoteMethod('getApprovars',                                                 //Registering the getApprovers Method in the list of Api's
    {
      http :{path:'/getApprovars', verb: 'get'},
      accepts:{arg:'id', type: 'string', required: true},
      returns: {type: 'string', root:true}
  
    }
);


Employee.currentUser = function(callback){                                            //Function to identify the current user
util.userIdentification(function(err, response){
  if(err)
    return callback("Unauthorized User");
console.log("user", response);
callback(null, response);
});
}

Employee.remoteMethod('currentUser',         
    {
      http :{path:'/user', verb: 'get'},
      //accepts:{arg:'id', type: 'string'},
      returns: {type: 'string', root:true}
  
    }
);



/*
Data needs to be send:-
  {
    "id":"employeeId",
    "password":"people10"
  }
*/


Employee.beforeRemote('prototype.updateAttributes',function(context, user, next){     //After the first time login we need to change the login status
  var req = context.req.body;
  if(req.action === "resetPasswordRequest"){
    Employee.findById(req.id, function(err, user){                           //to find the employee data so that we can compare b/w new password and default password given by admin 
      if (err){
        return next(err);
      }
      user.hasPassword(req.password, function(err, isMatch){
        if(err){
          return next(err);
        }
        if(isMatch){
          next("New Password cannot be same as Old Password:Please type a different password");
        }
        else{
          user.updateAttribute('loginStatus', 1, function(err, user){
            if(err){ 
              return next(err);
            }
            else{
              next();    
            }
          });        
        }
      });
    });
  }
  else if(req.action === "editRequest"){
    //console.log("Employee Edit functionality input", req);

    if(!req.firstName){
     return next("First Name Is Mandatory");
    }
    if(!req.empId){
      return next("Employee Id Is Mandatory");
    }
    if(!req.email){
      console.log("Email Field Is Mandatory");
      return next("Email Field Is Mandatory");
    }    
    if(!req.managerId){
    return next("Manager Name Is Mandatory");
    }
   if(req.lastName){
    req.name = req.firstName + req.lastName;
    }
    req.permanentAddressLine1 = req.permanentAddressDetails.permanentAddressLine1;
    req.permanentAddressLine2 = req.permanentAddressDetails.permanentAddressLine2;
    req.permanentAddressDistrict = req.permanentAddressDetails.district;
    req.permanentAddressCity = req.permanentAddressDetails.city;
    req.permanentAddressState = req.permanentAddressDetails.state;
    req.permanentAddressCountry = req.permanentAddressDetails.country;
    req.permanentAddressPincode = req.permanentAddressDetails.pincode;
    req.localAddressLine1 = req.localAddressDetails.localAddressLine1;
    req.localAddressLine2 = req.localAddressDetails.localAddressLine2;
    req.localAddressDistrict = req.localAddressDetails.district;
    req.localAddressCity = req.localAddressDetails.city;
    req.localAddressState = req.localAddressDetails.state;
    req.localAddressCountry = req.localAddressDetails.country;
    req.localAddressPincode = req.localAddressDetails.pincode;
    req.designation = req.designationId;
    req.reportingManagerId = req.managerId;
    req.department = req.departmentId;
    req.bloodGroup = req.bloodGroupId;
    //console.log("data modelling for edit functionality", req);
    next();
  }
  else{
    next();
  }
});




Employee.afterRemote('logout', function(context, output, next){                        //Output: Message after the logout
  context.result = "Successfully Logged Out";
  next();
});


Employee.getTeamMemberDetails = function(id, cb){
  getTeamMemberInformation.getTeamMemberInformation(id, function(err, response){
    if(err){
      return cb(err);
    }
    else{
      cb(null, response);
    }
  });
};

Employee.remoteMethod('getTeamMemberDetails',         
    {
      http :{path:'/getTeamMemberDetails', verb: 'get'},
      accepts:{arg:'id', type: 'string'},
      returns: {type: 'string', root:true}
  
    }
);


Employee.profileInformation = function(employeeId, callback){
  profileInformation.getUserDetails(employeeId, function(err, response){
    if(err){
      return callback(err);
    }
    else{
      callback(null, response);
    }
  });

};

Employee.remoteMethod('profileInformation',         
    {
      http :{path:'/profileInformation', verb: 'get'},
      accepts:{arg:'id', type: 'string'},
      returns: {type: 'string', root:true}
  
    }
);


Employee.getEmployeeList = function(callback){
  getEmployeeList.getEmployeeList(function(err, employeeList){
    if(err){
      return callback(err);
    }
    else{
      var activeEmployeeData = {};
      var inActiveEmployeeData = {};
      var activeList = [];
      var inActiveList = [];
      var list = [];
      employeeList.forEach(function(employee){
        if(employee.active === '1'){
          activeList.push(employee);
        }
        else{
          inActiveList.push(employee);
        }
      });
      if(activeList.length > 0){
        activeEmployeeData.title = "Active";
        activeEmployeeData.data = activeList;
      }
      if(inActiveList.length > 0){
        inActiveEmployeeData.title = "Inactive";
        inActiveEmployeeData.data = inActiveList;
      }
      list.push(activeEmployeeData);
      list.push(inActiveEmployeeData);
      //console.log("List", list);
      callback(null, list);
    }
  });

};

Employee.remoteMethod('getEmployeeList',         
    {
      http :{path:'/getEmployeeList', verb: 'get'},
      returns: {type: 'string', root:true}
  
    }
);



Employee.setActiveFieldOfEmployee = function(data, callback){
  //console.log("DATA", data);
  adminFunctionalities.setActiveFieldOfEmployee(data, function(err, response){
    if(err){
      return callback(err);
    }
    else{
      callback(null, response);
    }
  });

};

Employee.remoteMethod('setActiveFieldOfEmployee',         
    {
      http :{path:'/setActiveFieldOfEmployee', verb: 'post'},
      accepts:{arg: 'data', type: 'object', http: { source: 'body' }},
      returns: {type: 'string', root:true}
    }
);



Employee.getManagerList = function(callback){
  var approverList = [];
  var data = {};
  async.waterfall([
    function getApprovalRoleId(callback){
      app.models.roles.findOne({where: {name: "Approver"}}, function(err, approverRoleId){
        if(err){
          return callback(err);
        }
        else{
          //console.log()
          callback(null, approverRoleId);
        }    
      });
    },

    function getApproverList(approverRoleId, callback){
      app.models.EmployeeRoleMapping.find({where: {rolesId: approverRoleId.id},
        include: {
          relation: 'employee',
          scope: {
            fields: ['id', 'name']
          }
        },
        fields: {employeeId: true}
      }, function(err, managerList){
        if(err){
          return callback(err);
        }
        else{
          return callback(null, managerList);
        }
      });
    },
    function getModelledData(managerList, callback){
      managerList.forEach(function(manager){
        data = {};
        data.employeeId = manager.employeeId;
        data.name = manager.employee().name;
        approverList.push(data);
      });
      callback(null, approverList);
    }
    ], function(err, approverList){
    if(err){
      return callback(err);
    }
    else{
      return callback(null, approverList);
    }
  });
 };

Employee.remoteMethod('getManagerList',         
    {
      http :{path:'/getManagerList', verb: 'get'},
      returns: {type: 'string', root:true}
    }
);


// Employee.beforeRemote('prototype.updateAttributes',function(context, user, next){
//   var req = context.req.body;
//   console.log("Employee Edit functionality input", req);
//   var data = {};
//   req.permanentAddressLine1 = req.permanentAddressDetails.permanentAddressLine1;
//   req.permanentAddressLine2 = req.permanentAddressDetails.permanentAddressLine2;
//   req.permanentAddressDistrict = req.permanentAddressDetails.district;
//   req.permanentAddressCity = req.permanentAddressDetails.city;
//   req.permanentAddressState = req.permanentAddressDetails.state;
//   req.permanentAddressCountry = req.permanentAddressDetails.country;
//   req.permanentAddressPincode = req.permanentAddressDetails.pincode;
//   req.localAddressLine1 = req.localAddressDetails.localAddressLine1;
//   req.localAddressLine2 = req.localAddressDetails.localAddressLine2;
//   req.localAddressDistrict = req.localAddressDetails.district;
//   req.localAddressCity = req.localAddressDetails.city;
//   req.localAddressState = req.localAddressDetails.state;
//   req.localAddressCountry = req.localAddressDetails.country;
//   req.localAddressPincode = req.localAddressDetails.pincode;
//   req.designation = req.designationId;
//   req.reportingManagerId = req.managerId;
//   req.department = req.departmentId;
//   req.bloodGroup = req.bloodGroupId;
//   console.log("data modelling for edit functionality", req);
//   next();
// });

Employee.afterRemote('prototype.updateAttributes',function(context, output, next){
//console.log("Result ", context.result,output);
next();
});




Employee.beforeRemote('create',function(context, user, next){
  var req = context.req.body;
  //console.log('Data recieved', req);
  if(!req.firstName){
    return next("First Name Is Mandatory");
  }
  if(!req.empId){
    return next("Employee Id Is Mandatory");
  }
  if(!req.email){
    console.log("Email Field Is Mandatory");
    return next("Email Field Is Mandatory");
  }  
  if(!req.managerId){
    return next("Manager Name Is Mandatory");
  }
  if(req.lastName){
    req.name = req.firstName + req.lastName;
  }
  else{
    req.name = req.name = req.firstName;
  }
  if(req.permanentAddressDetails){
    req.permanentAddressLine1 = req.permanentAddressDetails.permanentAddressLine1;
  }
  if(req.permanentAddressDetails){
    req.permanentAddressLine2 = req.permanentAddressDetails.permanentAddressLine2;
  }
  if(req.permanentAddressDetails){
    req.permanentAddressDistrict = req.permanentAddressDetails.district;
  }
  if(req.permanentAddressDetails){
    req.permanentAddressCity = req.permanentAddressDetails.city;
  }
  if(req.permanentAddressDetails){
    req.permanentAddressState = req.permanentAddressDetails.state;
  }
  if(req.permanentAddressDetails){
    req.permanentAddressCountry = req.permanentAddressDetails.country;
  }
  if(req.permanentAddressDetails){
    req.permanentAddressPincode = req.permanentAddressDetails.pincode;
  }
  if(req.localAddressDetails){
    req.localAddressLine1 = req.localAddressDetails.localAddressLine1;
  }
  if(req.localAddressDetails){
    req.localAddressLine2 = req.localAddressDetails.localAddressLine2;
  }
  if(req.localAddressDetails){
    req.localAddressDistrict = req.localAddressDetails.district;
  }
  if(req.localAddressDetails){
    req.localAddressCity = req.localAddressDetails.city;
  }
  if(req.localAddressDetails){
    req.localAddressState = req.localAddressDetails.state;
  }
  if(req.localAddressDetails){
    req.localAddressCountry = req.localAddressDetails.country;
  }
  if(req.localAddressDetails){
    req.localAddressPincode = req.localAddressDetails.pincode;  
  }
  if(req.designationId){
    req.designation = req.designationId;  
  }
  if(req.managerId){
    req.reportingManagerId = req.managerId;
  }
  if(req.bloodGroupId){
    req.bloodGroup = req.bloodGroupId;
  }
  if(req.joiningDate){
    req.activeInactiveDate = req.joiningDate;
  }
  // req.password = '$2a$10$udE0ZMJqUbq1t6IM/tE/K.I8tZWx2FaCHF1Ql8pohcHBBUXqREz3m';
  // req.login_status = '0';
  // req.active = '1';
  console.log("INput Recieved", req);
  next();
});

Employee.afterRemote('create',function(context, user, next){
  var res = context.result;
  var data = {};
  data.employeeId = res.id;
  data.leaveEligibility = res.leaveEligibility;
  console.log("data *******************", data);

  if(res.leaveEligibility){
    adminFunctionalities.updateLeaveEligibility(data, function(err, response){
      if(err){
        return next(err);
      }
      else{
        console.log("leaveEligibility updated successfully");
        next();
      }
    });
  }
  else{
    next();
  }
});


Employee.activeInactiveDateBulkUpdate = function(callback){
  async.waterfall([
    function getEmployees(callback){
      Employee.find(function(err, employeeList){
        if(err){
          return callback(err);
        }
        else{
          callback(null, employeeList);
        }
      });
    },

    function updateDateOfOccurrence(employeeList, callback){
      async.eachSeries(employeeList, function(employee, clb){
        Employee.updateAll({id: employee.id}, {activeInactiveDate: employee.joiningDate}, function(err, res){
          if(err){
            return clb(err);
          }
          else{
            clb(null);
          }
        });
      }, function(err){
        if(err){
          return callback(err);
        }
        else{
          callback(null);
        }
      });
    }
    ], function(err, response){
        if(err){
          return callback(err);
        }
        else{
          callback(null);
        }
  });

};

Employee.remoteMethod('activeInactiveDateBulkUpdate',         
    {
      http :{path:'/activeInactiveDateBulkUpdate', verb: 'get'},
      returns: {type: 'string', root:true}
    }
);


Employee.updateLeaveEligibility = function(data, callback){
  console.log("DATA", data);
  adminFunctionalities.updateLeaveEligibility(data, function(err, response){
    if(err){
      return callback(err);
    }
    else{
      callback(null, response);
    }
  });

};

Employee.remoteMethod('updateLeaveEligibility',         
    {
      http :{path:'/updateLeaveEligibility', verb: 'post'},
      accepts:{arg: 'data', type: 'object', http: { source: 'body' }},
      returns: {type: 'string', root:true}
    }
);




};
