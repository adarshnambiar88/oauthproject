module.exports = function(ErpKeys){

	var count = 0;
  	ErpKeys.afterRemote('**',function(context, user, next){   
    	console.log("-----------------------------",context.methodString,++count);
    	next();
  	});

	ErpKeys.designationList = function(callback){
		ErpKeys.find({where: {type: "designation"}}, function(err, designationList){
			if(err){
				return callback(err);
			}
			else{
				callback(null, designationList);
			}
		});
	};

	ErpKeys.remoteMethod('designationList',													//Registering the designationList Method in the list of Api's
	 	{
	 		http :{path:'/designationList', verb: 'get'},
	 		returns: {type: 'string', root: true}
	 	}
	);



	ErpKeys.departmentList = function(callback){
		ErpKeys.find({where: {type: "department"}}, function(err, departmentList){
			if(err){
				return callback(err);
			}
			else{
			callback(null, departmentList);
			}
		});
	};
	
	ErpKeys.remoteMethod('departmentList',													//Registering the departmentList Method in the list of Api's
	 	{
	 		http :{path:'/departmentList', verb: 'get'},
	 		returns: {type: 'string', root: true}
	 	}
	);



	ErpKeys.bloodGroupList = function(callback){
		ErpKeys.find({where: {type: "blood_group"}}, function(err, bloodGroupList){
			if(err){
				return callback(err);
			}
			else{
				callback(null, bloodGroupList);
			}
		});
	};

	ErpKeys.remoteMethod('bloodGroupList',													//Registering the bloodGroupList Method in the list of Api's
	 	{
	 		http :{path:'/bloodGroupList', verb: 'get'},
	 		returns: {type: 'string', root:true}
	
	 	}
	);



	ErpKeys.employeeTypeList = function(callback){
		ErpKeys.find({where: {type: "employee_type"}}, function(err, employeeTypeList){
			if(err){
				return callback(err);
			}
			else{
				callback(null, employeeTypeList);
			}
		});	
	};

	ErpKeys.remoteMethod('employeeTypeList',													//Registering the employeeTypeList Method in the list of Api's
	 	{
	 		http :{path:'/employeeTypeList', verb: 'get'},
	 		returns: {type: 'string', root: true}
	 	}
	);



	ErpKeys.employeeStatusList = function(callback){
		ErpKeys.find({where: {type: "employee_status"}}, function(err, employeeStatusList){
			if(err){
				return callback(err);
			}
			else{
				callback(null, employeeStatusList);
			}
		});	
	};

	ErpKeys.remoteMethod('employeeStatusList',													//Registering the employeeStatusList Method in the list of Api's
	 	{
	 		http :{path:'/employeeStatusList', verb: 'get'},
	 		returns: {type: 'string', root: true}
	 	}
	);


};
