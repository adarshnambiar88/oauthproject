module.exports = function(EmployeeLeaveEligibility) {

	var util = require('../utilities.js');
	var async = require('async');
	var moment = require('moment');
	require('moment-weekday-calc');


	EmployeeLeaveEligibility.uploadPrivilegeLeaveEligibility = function(year, clb){											//Function to get all the leave request of a particular employee for the current year
		
		var privilegeKeyId = 0;

		async.waterfall([

		function getPrivilegeTypeOfLeaveId(callback){
			util.typeOfLeaves("Privilege", function(err, privilegeKey){
				if(err){
					return callback(err);
				}
				else{
					privilegeKeyId = privilegeKey;
					callback(null);
				}
			});
		},

		function findListOfEmployees(callback){
			app.models.Employee.find(function(err, employeeList){
				if(err){
					callback(err);
				}
				else{
					callback(null, employeeList);
				}
			});
		},

		function insertLeaveEligibility(employeeList,callback){
			var confirmationDateYear;
			var privilege_count = 21;
			var employee_privilege_count;
			var totalDaysInYear;
			var days;
			var toDate = ""+year+"-12-31";
			var employee_privilege_count_int_part ;
			var employee_privilege_count_fraction_part;
			var currentDate = new Date();
			var privilege_count_data = {};
			async.eachSeries(employeeList, function(employee, clb){
				confirmationDateYear = new Date(employee.confirmationDate).getFullYear();
				//console.log(confirmationDateYear, year);
				if(confirmationDateYear === year){
					if(moment(currentDate).isBefore(employee.confirmationDate)){
						employee_privilege_count = 0;
						//console.log("###########",employee.confirmationDate,employee.name,employee_privilege_count);
					}
					else{
						//console.log(confirmationDateYear, year);
						var isLeapYear = moment([year]).isLeapYear();
						//console.log(isLeapYear);
						if(isLeapYear){
							totalDaysInYear = 366; 
						}
						else{
							totalDaysInYear = 365;
						}
						days = moment().weekdayCalc(employee.confirmationDate, toDate, [0,1,2,3,4,5,6]);
						employee_privilege_count = (privilege_count/totalDaysInYear)*days;
						employee_privilege_count_int_part = Math.floor(employee_privilege_count);
						employee_privilege_count_fraction_part = employee_privilege_count - employee_privilege_count_int_part;
						//console.log("###########",employee.confirmationDate,employee.name,days,employee_privilege_count,employee_privilege_count_int_part,employee_privilege_count_fraction_part);
						if((employee_privilege_count_fraction_part >= 0) && (employee_privilege_count_fraction_part <= 0.5)){
							employee_privilege_count = employee_privilege_count_int_part + 0.5;
						}
						else if((employee_privilege_count_fraction_part > 0.5) && (employee_privilege_count_fraction_part < 1)){
							employee_privilege_count = employee_privilege_count_int_part + 1;
						}
						//console.log("###########",employee.confirmationDate,employee.name,days,employee_privilege_count,employee_privilege_count_int_part,employee_privilege_count_fraction_part);
						}
				}
				else{
					employee_privilege_count = 21;
				}
				privilege_count_data.eligibilityYear = year;
				privilege_count_data.eligibilityCount = employee_privilege_count;
				privilege_count_data.employeeId = employee.id;
				privilege_count_data.typeOfLeavesId = privilegeKeyId;
				console.log("final data",employee.id, year);
				EmployeeLeaveEligibility.findOne({where: {and: [{employeeId: employee.id}, {eligibilityYear: year}, {typeOfLeavesId: privilegeKeyId}]}}, function(err, leaveEligibilityData){
					if(err){
						return clb(err);
					}
					else if(leaveEligibilityData){
						console.log("leaveEligibilityData", leaveEligibilityData);
						EmployeeLeaveEligibility.updateAll({id: leaveEligibilityData.id}, {eligibilityCount: employee_privilege_count}, function(err, response){
							if(err){
								return clb(err);
							}
							else{
								clb(null);
							}
						});
					}
					else{
						console.log("NOt have any entry");
						EmployeeLeaveEligibility.create(privilege_count_data, function(err, response){
							if(err){
								return clb(err);
							}
							else{
								clb(null);
							}		
						});
					}
				});
			},function(err){
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		}
		]
		,function(err, response){
			if(err){
				clb(err);
			}
			else{
				clb(null, "Uploaded Successfully");
			}
		});
		
	};


	EmployeeLeaveEligibility.remoteMethod('uploadPrivilegeLeaveEligibility',													//Registering the getList Method in the list of Api's
	 	{
	 		http :{path:'/uploadPrivilegeLeaveEligibility', verb: 'post'},
	 		accepts:{arg:'year', type: 'number', required: true},
	 		returns: {type: 'string', root:true}
	 	}
	);

};
