module.exports = function(LeaveRequest) {
	var specialLeaves = require('../leave-options.js');
	var leaveSummary = require('../leave-statistics.js');
	var leaveRequestList = require('../getLeaveRequestList.js');
	var leaveRequestsWaitingForApprovalDataModelling = require('../leaveRequestsWaitingForApprovalDataModelling.js');
	var cancelPendingLeaveRequest = require('../cancelPendingLeaveRequest.js');
	var cancelApprovedLeaveRequest = require('../cancelApprovedLeaveRequest.js');
	var actionOnCancellationInProgressLeaveRequest = require('../actionOnCancellationInProgressLeaveRequest.js');
	var util = require('../utilities.js');
	var async = require('async');
	var ejs = require('ejs');
	var fs = require('fs');
	var moment = require('moment');
	require('moment-weekday-calc');


	LeaveRequest.afterRemote('**',function(context, user, next){		
	console.log("-----------------------------",context.methodString);
	next();
	});


	LeaveRequest.getList =function(employeeId, clb){											//Function to get all the leave request of a particular employee for the current year
		leaveRequestList.getLeaveRequestList(employeeId, function(err, leaveData){
			if(err){
				return clb(err);
			}
			else{
				clb(null, leaveData);
			}
		});
	};


	LeaveRequest.remoteMethod('getList',													//Registering the getList Method in the list of Api's
	 	{
	 		http :{path:'/getList', verb: 'get'},
	 		accepts:{arg:'id', type: 'string', required: true},
	 		returns: {type: 'string', root:true}
	
	 	}
	);
	 


	LeaveRequest.leaveStatistics = function(id, cb){     			           				//API to get the leave statistics
      	leaveSummary.leaveStatistics(id, function(err, response){
			if(err){
				return cb(err);
			}
			else{
				cb(null, response);
			}
		});
	};


	LeaveRequest.remoteMethod('leaveStatistics',											//Registering the leave statistics Method in the list of Api's
	 	{
	 		http :{path:'/leaveStatistics', verb: 'get'},
	 		accepts:{arg:'id', type: 'string', required: true},
	 		returns: {type: 'string', root: true }
	 	}
	 	);


	/*
		Data to be send to cancel a leave request
		{
         	"action":"cancel",
         	"type":type,
         	"id":leaveID,
         	"fromDate": fDate,
         	"toDate":tDate
       };
	*/

	/*
		Data to be send to update a leave request
		{
  			"fromDate": "2016-07-20",
  			"toDate": "2016-07-20",
  			"numberOfDays": 1,
  			"employeeId": "P1012349",
  			"halfDaySlot":0,
  			"approverId": "P1012348",
  			"comments": "after sendback",
  			"action":"update",
  			"id":358,
  			"type":"Privilege"
		}

	*/


	LeaveRequest.beforeRemote('prototype.updateAttributes',function(context, user, next){   //to update the status of cancelled request
		
		var req = context.req.body;
		var typeOfLeaveRequest = req.type;

		if(req.action === "update")													//if the request action is update
		{
			var specialLeaveId = null;
			if(req.numberOfDays === 0){
				return next("IT'S ALREADY A HOLIDAY");
			}
			var fromDate = new Date(req.fromDate);
			var day = fromDate.getDay();
			if(day === 0 || day === 6){
				return next("IT'S ALREADY A HOLIDAY:Please Pay Attention");
			}
			async.waterfall([
			// function updateSpecialLeaveStatusForSpecialLeaves(callback){					//function to update special type of leaves of the corresponding leave to its initial state
			// 	var typeOfLeaveRequest = req.type;
			// 	if((typeOfLeaveRequest === "Paternity")||(typeOfLeaveRequest === "Maternity")||(typeOfLeaveRequest === "Comp-off")){
			// 		app.models.EmployeeSpecialLeaveEligibility.updateAll(			//query to update the employeeSpecialLeaveTable to its initial state
			// 		{leaveRequestId:req.id}, {status: 0, leaveRequestId: null}, function(err, response){
			// 			if(err){		
			// 				return callback(err);
			// 			}
			// 			else{
			// 			specialLeaveId = response.id;
			// 			callback(null);
			// 			}
			// 		});
			// 	}
			// 	else{
			// 		callback(null);
			// 	}
			// },

			function dateConflicts(callback){												//function to check for the date conflicts										
				util.dateConflicts(req,function(err, response){								//calling the dateConflicts function from utility.js file
					if(err){
						return callback(err);
					}
					else{
						callback(null);
					}
				});
			},

			function settingDataToBeUpdated(callback){										//function for data formatting to update the leave request
				var currentDate = new Date().toLocaleString();
				util.ErpKeysCode("action_taken", "Update",function(err, key){				//calling ErpKeysCode function to get the action taken code for apply 
					if(err){
						return callback(err);
					}
					var data = {
						"fromEmployee": req.employeeId,
						"toEmployee": req.approverId,
						"actionTaken": key.keyCode,
						"actionTimestamp": currentDate,
						"comments": req.comments,
						"leaveRequestId": req.id
					};
				
					callback(null, data);
				});
			},

			function leaveWorkflowEntry(data, callback){									//update the leave workflow table according to the update
				app.models.LeaveWorkflow.create(data ,function(err, obj){
					if(err){
						return callback(err);
					}
					else{
						callback(null);
					}
				});
			},

			// function updateSpecialLeaves(callback){											//update special leave table for the corresponding employee
			// 	util.updateSpecialLeaves(req, function(err,response){						//calling the updateSpecialLeave function from the utilities.js
			// 		if(err){
			// 			return callback(err);
			// 		}
			// 		else{
			// 			callback(null);
			// 		}
			// 	});
			// }
			]
			,function(err, response){														//function returning from beforeRemote hook
				if(err){
					return next(err);
				}
				else{
					next();
				}
			});
		}
		else{
			next();
		}

	});	


	LeaveRequest.afterRemote('prototype.updateAttributes',function(context, output, next){ 
		var res = context.result;
		
		async.waterfall([
		function getEmployeeDetails(callback){												//function to get the all employees details
			app.models.Employee.findOne({
			where: {id: res.employeeId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, employeeData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData);
				}
			});					
		},

		function getManagerDetails(employeeData, callback){									//function to get the manager details
			app.models.Employee.findOne({
			where: {id: employeeData.reportingManagerId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, managerData){
				if(err){
					return callback(err);
				}
				else{
					callback(null, employeeData, managerData);
				}
			});					
		},

		function sendEmailNotification(employeeData, managerData, callback){				//function to send the email notification to the manager
			var emailData = {
					actionByEmployee: res.action,
					employeeEmpId: employeeData.empId,
					employeeName: employeeData.name,
					managerEmail: managerData.email,
					employeeEmail: employeeData.email,
					employeeDesignation: employeeData.designation,
					comments: res.comments,
					leaveFromDate: moment(res.fromDate).format("MMM DD, YYYY"),
					leaveToDate: moment(res.toDate).format("MMM DD, YYYY"),
					numberOfDays: res.numberOfDays
			};
			util.sendEmailNotificationToManager(emailData, function(err, response){ 		//calling the sendEmailNotificationToManager from utilities.js
				if(err){
					return callback("Email Notification Failed");
				}
			});
			callback(null);
		}
		]
		,function(err){																		//function returning from afterRemote hook
			if(err){
				return next(err);
			}
			else{
				next();
			}
		});
	});


	/*
		Data needs to be send(from UI)
		{
  			"fromDate": "2016-03-23",                                
  			"toDate": "2016-03-23",
  			"numberOfDays": 1,
 			"description": "sick leave",
  			"type": "Loss of Pay",
 			"employeeId": "3",
  			"halfDaySlot":0,
  			"approverId": "2"
		}
		Other options for type field://Privilege,COMP OFF,Paternity,Maternity
		Other option for halfDaySlot field:// or 0
	*/



	LeaveRequest.beforeRemote('create',function(context, user, next){						//to apply a leave request
		var req = context.req;
		console.log("request ",req.body);
		if(req.body.approverId === undefined || req.body.approverId === null){
			console.log("NO APPROVER");
			return next("There is no approver for this employee");
		}
		var pendingStatusCode ;
		var cancelStatus;
		var count_date_match = 0;
		var count_privilege = 0;
		var typeOfLeaveRequest = req.body.type;
		var fromDate = new Date(req.body.fromDate);
		var day = fromDate.getDay();
		if(day === 0 || day === 6){
			return next("IT'S ALREADY A HOLIDAY:Please Pay Attention");
		}
		req.body.leaveRequestYear = fromDate.getFullYear();
		if(req.body.numberOfDays === 0){
			return next("IT'S ALREADY A HOLIDAY");
		}
		async.waterfall([
		function pendingStatusCode(callback){												//function to get the pendingStatus code 
			util.ErpKeysCode("leave_status", "Pending", function(err, key){
				if(err){
					return callback(err);
				}
				else{
					req.body.status = key.keyCode;
					console.log("1st phase",req.body);
					callback(null);
				}
			});
		},

		function dateConflicts(callback){													//function to check for date conflicts
			util.dateConflicts(req.body, function(err, response){							//function to call the dateConflicts method from tthe utilities.js
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		},

		function typeOfLeaveCode(callback){																  //function to get the leave type key value
			app.models.TypeOfLeaves.findOne({where: {leaveType: typeOfLeaveRequest}
			},function(err, leave){
				if(err){
					callback(err);
				}
				else{
					req.body.typeOfLeavesId = leave.id;
					//console.log("inside util 3rd phase", req.body);
					callback(null);
				}
			});
		},

		function checkProbationPeriod(callback){
			app.models.Employee.findOne({where: {id: req.body.employeeId}, fields: {name: true, confirmationDate: true}}, function(err, employeeData){
				if(err){
					return callback(err);
				}
				else{
					console.log("EmployeeData", employeeData, typeOfLeaveRequest);
					if((typeOfLeaveRequest === "Privilege") && (fromDate < employeeData.confirmationDate)){
						console.log("Your Probation Period is not over");
						return callback("Your Probation Period is not over");
					}
					else{
						console.log("Your Probation Period is over");
						callback(null);
					}
				}
			});
		}
		]
		,function(err, response){															//function returning from the beforeRemote hook
			if(err){
			  return next(err);
			}
			else{
				next();
			}
		});
	});		



	LeaveRequest.afterRemote('create',function(context, output, next){						//after creating the leave request
		var res = context.result;
		console.log("response in afterRemote",res);
		var currentDate = new Date().toLocaleString();
		var typeOfLeaveRequest = res.type;

		async.waterfall([
		// function updateSpecialLeaves(callback){												//update the special leaves table
		// 	util.updateSpecialLeaves(res, function(err,response){
		// 		if(err){
		// 			return callback(err);
		// 		}
		// 		else{
		// 			console.log("updateSpecialLeaves", response);
		// 			callback(null);
		// 		}
		// 	});
		// },

		function dataFormattingForLeaveWorkflowEntry(callback){								//function for data formatting for the leave workflow entry
			util.ErpKeysCode("action_taken", "Apply", function(err, key){					//calling function of ErpKeysCOde of utilities.js to find the actionTaken code for leave apply 
				if(err){
					return callback(err);
				}
				var data = {
					"fromEmployee": res.employeeId,
					"toEmployee": res.approverId,
					"actionTaken": key.keyCode,
					"actionTimestamp": currentDate,
					"comments": res.description,
					"leaveRequestId": res.id
				};
				console.log("dataFormattingForLeaveWorkflowEntry", data);
				callback(null, data);
			});
		},

		function updateLeaveWorkflowTable(data, callback){									//function to update the leaveWorkflow table
			app.models.LeaveWorkflow.create(data , function(err, obj){
				if(err){
					return callback(err);
				}
				else{
					console.log("updateLeaveWorkflowTable", obj);
					callback(null);
				}
			});
		},

		function getEmployeeDetails(callback){												//function to get the employee details
			app.models.Employee.findOne({
			where: {id: res.employeeId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, employeeData){
				if(err){
					return callback(err);
				}
				else{
					console.log("getEmployeeDetails", employeeData);
					callback(null, employeeData);
				}
			});					
		},

		function getManagerDetails(employeeData, callback){									//function to get the manager details
			app.models.Employee.findOne({													//query to find the employee
			where: {id: res.approverId},
			fields: {reportingManagerId: true ,name: true, empId: true, designation: true, email: true}}, function(err, managerData){
				if(err){
					return callback(err);
				}
				else{
					console.log("getManagerDetails", managerData);
					callback(null, employeeData, managerData);
				}
			});					
		},

		function sendEmailNotification(employeeData, managerData, callback){				//function to send email notification to the manager regarding leave application
			var emailData = {
					actionByEmployee : "Applied",
					managerId : managerData.empId,
					employeeEmpId : employeeData.empId,
					employeeName : employeeData.name,
					managerEmail : managerData.email,
					employeeEmail : employeeData.email,
					employeeDesignation : employeeData.designation,
					description : res.description ,
					leaveFromDate : moment(res.fromDate).format("MMM DD, YYYY"),
					leaveToDate : moment(res.toDate).format("MMM DD, YYYY"),
					numberOfDays : res.numberOfDays
			};
			util.leaveApplicationNotification(emailData, function(err, response){
				if(err){
					return callback("Email Notification Failed");
				}
				else{
					console.log("Email Notification Sent");
				}
			});
			callback(null);
		}
		]
		,function(err, response){															//function returning from afterRemote hook
			if(err){
				return next(err);
			}
			else{
				console.log("LeaveRequest.afterRemote Ended");
				next();
			}
		});
	});



	

	LeaveRequest.leaveRequestsWaitingForApproval = function(id, cb){						//function to get the leave requests waiting for approval from a manager
		leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(id, function(err, response){
			if(err){
				return cb(err);
			}
			else{
				cb(null, response);
			}
		});
	};


	LeaveRequest.remoteMethod('leaveRequestsWaitingForApproval',							//Registering the getList Method in the list of Api's
	 	{
	 		http :{path:'/leaveRequestsWaitingForApproval', verb: 'get'},
	 		accepts:{arg:'id', type: 'string', required: true},
	 		returns: {type: 'string', root:true}
	 	}
	);




	LeaveRequest.cancelLeaveRequest = function(data, callback){
		//var req = req.body;
		var req = data;
		console.log("req", data);
		if(req.currentStatus === "Pending"){
			console.log("In Pending");
			cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, response){
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		}
		else if(req.currentStatus === "Approved"){
			cancelApprovedLeaveRequest.cancelApprovedLeaveRequest(req, function(err, response){
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		}
		else if(req.currentStatus === "Cancellation In Progress"){
			actionOnCancellationInProgressLeaveRequest.actionOnCancellationInProgressLeaveRequest(req, function(err, response){
				if(err){
					return callback(err);
				}
				else{
					callback(null);
				}
			});
		}
	};

	LeaveRequest.remoteMethod('cancelLeaveRequest',											//Registering the leave statistics Met//hod in the list of Api's
	 	{
	 		http :{path:'/cancelLeaveRequest', verb: 'post'},
	 		accepts:{arg: 'data', type: 'object', http: { source: 'body' }},
	 		returns: {type: 'string', root: true }
	 	}
	);


};
