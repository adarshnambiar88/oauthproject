var async = require('async');
var util = require('./utilities.js');
var moment = require('moment');
require('moment-weekday-calc');

var leaveStatistics = function(id, cb){												//function to get the leave statistics of an employee
	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();

	var cancelStatus;
	var deniedStatus;
	var pendingStatus;
	var approvedStatus;
	var cancellationInProgressStatus;

	var privilege_leave_count = 0;
	var maternity_leave_count = 0;
	var paternity_leave_count = 0;
	var leaveTypes;

	async.waterfall([
	function getLeaveStatusValue(callback){											//function to get the cancel leave status
		util.ErpKeysCode("leave_status", null, function(err, response){
			if(err){
				callback(err);
			}
			else{
				response.forEach(function(res){
					if(res.keyValue === "Approved"){
						approvedStatus = res.keyCode;
					}
					else if(res.keyValue === "Pending"){
						pendingStatus = res.keyCode;
					}
					else if(res.keyValue === "Denied"){
						deniedStatus = res.keyCode;
					}
					else if(res.keyValue === "Cancelled"){
						cancelStatus = res. keyCode;
					}
					else if(res.keyValue === "Cancellation In Progress"){
						cancellationInProgressStatus = res.keyCode;
					}
				});
				callback(null);
			}
		});
	},

	function getLeaveTypeCodes(callback){
		util.typeOfLeaves(null , function(err, response){
			if(err){
				return callback(err);
			}
			else{
				leaveTypes = response;
				//console.log(leaveTypes);
				callback(null);
			}
		});
	},

	function getEligibleLeaveCount(callback){
		app.models.EmployeeLeaveEligibility.find({where: {and: [{employeeId: id}, {eligibilityYear: currentYear}]}},function(err, eligibleLeaveTypes){
			if(err){
				return callback(err);
			}
			else if(eligibleLeaveTypes){
				eligibleLeaveTypes.forEach(function(eligibleLeaveType){
					if(eligibleLeaveType.typeOfLeavesId === leaveTypes.privilegeId){
						privilege_leave_count = eligibleLeaveType.eligibilityCount;
					}
					else if(eligibleLeaveType.typeOfLeavesId === leaveTypes.maternityId){
						maternity_leave_count = eligibleLeaveType.eligibilityCount;
					}
					else if(eligibleLeaveType.typeOfLeavesId === leaveTypes.paternityId){
						paternity_leave_count = eligibleLeaveType.eligibilityCount;
					}
				});
				callback(null);
			}
			else{
				return callback("No Leaves Has Been Assigned To You");
			}
		});
	},

	function findLeaveRequests(callback){											//function to find all leave request along with type of leaves by making a join between leave_request and type_of_leave table
		app.models.LeaveRequest.find({
		where: {and: [{employeeId:id}
		,{leaveRequestYear:currentYear}
		,{status: {nin: [cancelStatus, deniedStatus]}}]}
		},function(err, leaves){
			if(err){
				callback(err);
			}
			else{
				callback(null, leaves);		
			}
		});
	},

	function leaveStatisticsData(leaves, callback){									//function to fetch out the leave statistics of a particular employee
		var count_pending = 0;
		var count_approved = 0;
		var count_remaining = privilege_leave_count;
		var count_privilege = 0;
		var count_lop = 0;
		var count_total = privilege_leave_count;
		var count_toBeTaken = 0;
		var count_compoff = 0;
		var count_paternity = 0;
		var count_maternity = 0;
		var data = {
	 		pending : count_pending ,
	 		LOP: count_lop ,
	 		privilege: count_privilege ,
	 		remaining: count_remaining ,
	 		availed: (count_approved - count_toBeTaken) ,
	 		totalLeaves: count_total ,
	 		toBeTaken: count_toBeTaken,
	 		paternity: count_paternity,
	 		maternity: count_maternity,
	 		compOff: count_compoff
	 		};
		if(leaves.length < 1){
			return callback(null,data);
		}
		leaves.forEach(function(leave){												//to loop through all the leaves to find the different parameters of leave statistics
			if(leave.status === pendingStatus){
				count_pending+=leave.numberOfDays;
			}
			else if(leave.status === approvedStatus){
				count_approved+=leave.numberOfDays;
				if(moment(currentDate).isBefore(leave.fromDate)){
					count_toBeTaken+=leave.numberOfDays ;
				}
			}
			if(leave.typeOfLeavesId === leaveTypes.privilegeId){
				count_privilege+=leave.numberOfDays;
				//console.log("privilege:-",count_privilege);
			}
			else if(leave.typeOfLeavesId === leaveTypes.lossOfPayId){
				count_lop+=leave.numberOfDays;
			}
			else if(leave.typeOfLeavesId === leaveTypes.compOffId){
				count_compoff+=leave.numberOfDays; 
			}
			else if(leave.typeOfLeavesId === leaveTypes.paternityId){
				count_paternity+=leave.numberOfDays;
			}
			else if(leave.typeOfLeavesId === leaveTypes.maternityId){
				count_maternity+=leave.numberOfDays;
			}			
		});
			//console.log("privilege:-",count_privilege);	
		if(count_remaining !== 0){							//creaing JSON (Data to be passed)
			count_remaining =  count_remaining - (count_privilege);
		}
		data = {};
	 	data.pending = count_pending;
	 	data.LOP = count_lop;
	 	data.privilege = count_privilege;
	 	data.remaining = count_remaining;
	 	data.availed = (count_approved - count_toBeTaken);
	 	data.totalLeaves = count_total;
	 	data.toBeTaken = count_toBeTaken;
	 	if(count_compoff > 0){
	 		data.compOff = count_compoff;
	 	}
	 	if(count_paternity > 0){
	 		data.paternity = count_paternity;
	 	}
	 	if(count_maternity > 0){
	 		data.maternity = count_maternity;
	 	}
	 	callback(null,data);
	}
	],

	function(err, data){																//returning final result to the calling function
		if(err){
			return cb(err);
		}
		else{
			//console.log("data", data);
			cb(null,data);
		}
	});
};

	exports.leaveStatistics = leaveStatistics;