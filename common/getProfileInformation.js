var util = require('./utilities.js');
var async = require('async');
var _ = require('underscore');

var getUserDetails = function(employeeId, callback){
	var profileDetails = {};
	var permanentAddress = {};
	var localAddress = {};
  var data =  {};
  var privilegeCount = 0;
  var paternityCount = 0;
  var maternityCount = 0;
  var compOffCount = 0;
  var leaveEligibilityData = [];
  var eligibilityWithFirstYear = {};
  var eligibilityWithSecondYear = {};
  	async.waterfall([
    function userRawDetail(callback){
      app.models.Employee.findOne({where: {id: employeeId}}, function(err, userDetail){
        if(err){
          return callback(err);
        }
        else{
          profileDetails.id = userDetail.id;
        	profileDetails.name = userDetail.name;
          profileDetails.firstName = userDetail.firstName;
          profileDetails.lastName = userDetail.lastName;
        	profileDetails.empId = userDetail.empId;
      		profileDetails.email = userDetail.email;
      		profileDetails.phoneNo = userDetail.phoneNo;
      		profileDetails.dob = userDetail.dob;
      		profileDetails.joiningDate = userDetail.joiningDate;
          profileDetails.confirmationDate = userDetail.confirmationDate; 
          profileDetails.active = userDetail.active; 
          profileDetails.activeInactiveDate = userDetail.activeInactiveDate;         
      		profileDetails.levelOfApprovalRequired = userDetail.levelOfApprovalRequired;
          profileDetails.emergencyContactName = userDetail.emergencyContactName;
      		profileDetails.emergencyContactPhoneNo = userDetail.emergencyContactPhoneNo;
          profileDetails.nationality = userDetail.nationality;
      		profileDetails.gender = userDetail.gender;
      		permanentAddress.permanentAddressLine1 = userDetail.permanentAddressLine1;
      		permanentAddress.permanentAddressLine2 = userDetail.permanentAddressLine2;
          permanentAddress.district = userDetail.permanentAddressDistrict;
          permanentAddress.city = userDetail.permanentAddressCity;
      		permanentAddress.state = userDetail.permanentAddressState;
      		permanentAddress.country = userDetail.permanentAddressCountry;
      		permanentAddress.pincode = userDetail.permanentAddressPincode;
      		localAddress.localAddressLine1 = userDetail.localAddressLine1;
      		localAddress.localAddressLine2 = userDetail.localAddressLine2;
          localAddress.district = userDetail.localAddressDistrict;
      		localAddress.city = userDetail.localAddressCity;
      		localAddress.state = userDetail.localAddressState;
      		localAddress.country = userDetail.localAddressCountry;
      		localAddress.pincode = userDetail.localAddressPincode;
      		profileDetails.permanentAddressDetails = permanentAddress;
      		profileDetails.localAddressDetails = localAddress;
      		//console.log("userRawDetail", profileDetails);
          //console.log("userDetail", userDetail);
          callback(null, userDetail);
        }
      });
    },

    function getLeaveEligibility(userDetail, callback){

      app.models.EmployeeLeaveEligibility.find({where: {employeeId: employeeId}, include: {relation: 'typeOfLeaves'}, order: 'eligibilityYear ASC'},function(err, eligibilityData){
        if(err){
          return callback(err);
        }
        else if(eligibilityData){
          console.log(eligibilityData);
        
          eligibilityData.forEach(function(eligibleLeaveData){
            //console.log("****", eligibleLeaveData.typeOfLeaves().leaveType);
            if(eligibleLeaveData.eligibilityYear === "2016"){
              eligibilityWithFirstYear.year = eligibleLeaveData.eligibilityYear;
              if(eligibleLeaveData.typeOfLeaves().leaveType === "Privilege"){
                //console.log("OOOOOOOO",eligibleLeaveData);
                privilegeCount = eligibleLeaveData.eligibilityCount;
                eligibilityWithFirstYear.privilege = privilegeCount;
              }
              else{
                eligibilityWithFirstYear.privilege = privilegeCount; 
              }

              if(eligibleLeaveData.typeOfLeaves().leaveType === "Paternity"){
                paternityCount = eligibleLeaveData.eligibilityCount;
                eligibilityWithFirstYear.paternity = paternityCount; 
              }
              else{
                if(userDetail.gender === 'M'){
                  eligibilityWithFirstYear.paternity = paternityCount;   
                }
              }

              if(eligibleLeaveData.typeOfLeaves().leaveType === "Maternity"){
                maternityCount = eligibleLeaveData.eligibilityCount;
                eligibilityWithFirstYear.maternity = maternityCount; 
              }
              else{
                if(userDetail.gender === 'F'){
                  eligibilityWithFirstYear.maternity = maternityCount;   
                }
              }
            }
            else if(eligibleLeaveData.eligibilityYear === "2017"){
              eligibilityWithSecondYear.year = eligibleLeaveData.eligibilityYear;
              if(eligibleLeaveData.typeOfLeaves().leaveType === "Privilege"){
                privilegeCount = eligibleLeaveData.eligibilityCount;
                eligibilityWithSecondYear.privilege = privilegeCount;
              }
              else{
                eligibilityWithSecondYear.privilege = privilegeCount; 
              }

              if(eligibleLeaveData.typeOfLeaves().leaveType === "Paternity"){
                paternityCount = eligibleLeaveData.eligibilityCount;
                eligibilityWithSecondYear.paternity = paternityCount; 
              }
              else{
                if(userDetail.gender === 'M'){
                  eligibilityWithSecondYear.paternity = paternityCount;   
                }
              }

              if(eligibleLeaveData.typeOfLeaves().leaveType === "Maternity"){
                maternityCount = eligibleLeaveData.eligibilityCount;
                eligibilityWithSecondYear.maternity = maternityCount; 
              }
              else{
                if(userDetail.gender === 'F'){
                  eligibilityWithSecondYear.maternity = maternityCount;   
                }
              }
            }            
          });
          if(!(_.isEmpty(eligibilityWithFirstYear))){
            leaveEligibilityData.push(eligibilityWithFirstYear);
          }
          if(!(_.isEmpty(eligibilityWithSecondYear))){
            leaveEligibilityData.push(eligibilityWithSecondYear);
          }
          profileDetails.eligibilityCount = leaveEligibilityData;
          console.log(profileDetails.eligibilityCount);
          callback(null, userDetail);
        }
        else{
          return callback("No Eligibility Count");
        }
      });
    },

    function getDesignation(userDetail, callback){
      app.models.ErpKeys.findOne({where: {and: [{type: "designation"}, {keyCode: userDetail.designation}]}}, function(err, key){
            if(err){
              return callback(err);
            }
            else if(key){
              profileDetails.designation = key.keyValue; 
              profileDetails.designationId = userDetail.designation;
              callback(null, userDetail);
            }
            else{
              //console.log("getDesignation", profileDetails);
              callback(null, userDetail); 
            }
          });
    },

    function getDepartment(userDetail, callback){
      app.models.ErpKeys.findOne({where: {and: [{type: "department"}, {keyCode: userDetail.departmentId}]}}, function(err, key){
            if(err){
              return callback(err);
            }
            else if(key){
              profileDetails.department = key.keyValue;  
              profileDetails.departmentId = userDetail.departmentId;
              callback(null, userDetail);
            }
            else{
              //console.log("getDesignation", profileDetails);
              callback(null, userDetail); 
            }
          });
    },

    function getBloodGroup(userDetail, callback){
      app.models.ErpKeys.findOne({where: {and: [{type: "blood_group"}, {keyCode: userDetail.bloodGroup}]}}, function(err, key){
            if(err){
              return callback(err);
            }
            else if(key){
              profileDetails.bloodGroup = key.keyValue;  
              profileDetails.bloodGroupId = userDetail.bloodGroup;
              callback(null, userDetail);
            }
            else{
              //console.log("getDesignation", profileDetails);
              callback(null, userDetail); 
            }
          });
    },


    // function getEmployeeStatus(userDetail, callback){
    //   app.models.ErpKeys.findOne({where: {and: [{type: "employee_status"}, {keyCode: userDetail.employeeStatus}]}}, function(err, key){
    //         if(err){
    //           return callback(err);
    //         }
    //         else{
    //           profileDetails.employeeStatus = key.keyValue;  
    //           console.log("getDesignation", profileDetails);
    //           callback(null, userDetail); 
    //         }
    //       });
    // },

    function getReportingManager(userDetail, callback){
    	app.models.Employee.findOne({where: {id: userDetail.reportingManagerId}}, function(err, managerDetails){
    		if(err){
    			callback(err);
    		}
        else if(managerDetails){
          profileDetails.managerName = managerDetails.name;
          profileDetails.managerId = userDetail.reportingManagerId;
          callback(null);
        }
    		else{
    			//console.log("getReportingManager", profileDetails);
    			callback(null);
    		}
    	});
    }
  ], function(err, response){
    if(err){
      return callback(err);
    }
    else{
      //console.log("profileDetails", profileDetails);
      callback(null, profileDetails);
    }
  });

};

exports.getUserDetails = getUserDetails;		