var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  });

  afterEach(function(){
    	mockery.disable();
  });


  it(' first call to ErpKeysCode should fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
      error: 'ErpKeysCode fails the leaveStatistics'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'ErpKeysCode fails the leaveStatistics'});
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      done();
    });
  });



  it(' second call to ErpKeysCode should fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, {
      error: 'ErpKeysCode fails the leaveStatistics'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'ErpKeysCode fails the leaveStatistics'});
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      done();
    });
  });


it(' first call to EmployeePrivilegeLeaveEligibility.findOne should fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, {
      error: 'EmployeePrivilegeLeaveEligibility.findOne failed'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'EmployeePrivilegeLeaveEligibility.findOne failed'});
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      done();
    });
  });

it(' first call to EmployeePrivilegeLeaveEligibility.findOne should fail the leaveStatistics by No Privilege Count', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, null);
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(err).to.be.a('string');
      expect(err).to.eql('No Privilege Count');
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      done();
    });
  });


it(' first call to LeaveRequest.find should fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
     app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
    var leaveCount = {
      eligibility: 21
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveCount);
    app.models.LeaveRequest.find.onCall(0).callsArgWith(1, {
      error: 'LeaveRequest.find failed'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'LeaveRequest.find failed'});
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
      done();
    });
  });



it(' third call to ErpKeysCode should fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
    var leaveCount = {
      eligibility: 21
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveCount);
    app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
    utilMock.ErpKeysCode.onCall(2).callsArgWith(2, {
    error: 'ErpKeysCode fails the leaveStatistics'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'ErpKeysCode fails the leaveStatistics'});
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      expect(utilMock.ErpKeysCode.getCall(2).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(2).args[1]).to.eql(null);
      expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
      done();
    });
  });



it(' first call to EmployeeSpecialLeaveEligibility should fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        },
        EmployeeSpecialLeaveEligibility: {
          find: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
     var leaveCount = {
      eligibility: 21
    };
    var leave = [{
      id: 1, leaveRequestYear: 2016, fromDate: '2016-09-12', toDate: '2016-09-12',
      numberOfDays: 1, status: 1, description: 'adasd', halfDaySlot: 0,
      typeOfLeavesId: 1, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 1, leaveType: 'Privilege', eligibility: 21};
      }
      },
      {
      id: 1, leaveRequestYear: 2016, fromDate: '2016-05-13', toDate: '2016-05-13',
      numberOfDays: 1, status: 2, description: 'adasd', halfDaySlot: 0,
      typeOfLeavesId: 2, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 2, leaveType: 'Loss of Pay', eligibility: 0};
      }
      }  
      ];
    var response = [
    {id: 5, type: 'leaveStatus', keyCode: 1, keyValue: 'Approved'},
    {id: 6, type: 'leaveStatus', keyCode: 2, keyValue: 'Pending'}
    ];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveCount);
    app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, leave);
    utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, response);
    app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, {
      error: 'EmployeeSpecialLeaveEligibility.find fails'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(app.models.LeaveRequest.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'EmployeeSpecialLeaveEligibility.find fails'});
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      expect(utilMock.ErpKeysCode.getCall(2).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(2).args[1]).to.eql(null);
      expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
      done();
    });
  });



it(' first call to EmployeeSpecialLeaveEligibility should not fail the leaveStatistics(return empty array)', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        },
        EmployeeSpecialLeaveEligibility: {
          find: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
     var leaveCount = {
      eligibility: 21
    };
    var leave = [{
      id: 1, leaveRequestYear: 2016, fromDate: '2016-09-12', toDate: '2016-09-12',
      numberOfDays: 1, status: 1, description: 'adasd', halfDaySlot: 0,
      typeOfLeavesId: 1, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 1, leaveType: 'Privilege', eligibility: 21};
      }
      },
      {
      id: 1, leaveRequestYear: 2016, fromDate: '2016-05-13', toDate: '2016-05-13',
      numberOfDays: 1, status: 2, description: 'adasd', halfDaySlot: 0,
      typeOfLeavesId: 2, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 2, leaveType: 'Loss of Pay', eligibility: 0};
      }
      }  
      ];
    var response = [
    {id: 5, type: 'leaveStatus', keyCode: 1, keyValue: 'Approved'},
    {id: 6, type: 'leaveStatus', keyCode: 2, keyValue: 'Pending'}
    ];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveCount);
    app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, leave);
    utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, response);
    app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, []);
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(app.models.LeaveRequest.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      expect(utilMock.ErpKeysCode.getCall(2).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(2).args[1]).to.eql(null);
      expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
      done();
    });
  });


it(' first call to EmployeeSpecialLeaveEligibility(Paternity) should not fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        },
        EmployeeSpecialLeaveEligibility: {
          find: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
    var leaveCount = {
      eligibility: 21
    };
    var leave = [{
      id: 1, leaveRequestYear: 2016, fromDate: '2016-09-12', toDate: '2016-09-12',
      numberOfDays: 1, status: 1, description: 'adasd', halfDaySlot: 0,
      typeOfLeavesId: 5, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 5, leaveType: 'Paternity', eligibility: 21};
      }
      } 
      ];
    var obj = {id: 1, leaveRequestYear: 2016, fromDate: '2016-09-12', toDate: '2016-09-12',
        numberOfDays: 1, status: 1, description: 'adasd', halfDaySlot: 0,
        typeOfLeavesId: 5, employeeId: 1
      };
    var specialLeaves = [{
      id:1, workedOn: null, status: 1, typeOfLeavesId:5,
      leaveRequestId: 1, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 5, leaveType: 'Paternity', eligibility: 5};
      },
      leaveRequest: function(){
        return obj;
      }
      }
      ];
    var response = [
    {id: 5, type: 'leaveStatus', keyCode: 1, keyValue: 'Approved'},
    {id: 6, type: 'leaveStatus', keyCode: 2, keyValue: 'Pending'}
    ];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveCount);    
    app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, leave);
    utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, response);
    app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, specialLeaves);
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(app.models.LeaveRequest.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      expect(utilMock.ErpKeysCode.getCall(2).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(2).args[1]).to.eql(null);
      expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
      done();
    });
  });



it(' first call to EmployeeSpecialLeaveEligibility(Maternity) should not fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        },
        EmployeeSpecialLeaveEligibility: {
          find: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
    var leaveCount = {
      eligibility: 21
    };
    var obj = {
            id: 2, leaveRequestYear: 2016, fromDate: '2016-09-13', toDate: '2016-09-13',
            numberOfDays: 1, status: 1, description: 'adasd', halfDaySlot: 0,
            typeOfLeavesId: 4, employeeId: 1
          };
    var specialLeaves = [
      {id:1, workedOn: null, status: 1, typeOfLeavesId:4,
      leaveRequestId: 2, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 4, leaveType: 'Maternity', eligibility: 90};
      },
      leaveRequest: function(){
        return obj;
      }
      }
      ];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveCount);
    app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
    utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, []);
    app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, specialLeaves);
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(app.models.LeaveRequest.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      expect(utilMock.ErpKeysCode.getCall(2).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(2).args[1]).to.eql(null);
      expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
      done();
    });
  });


it(' first call to EmployeeSpecialLeaveEligibility should not fail the leaveStatistics', function(done){
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app = {
      models: {
        LeaveRequest: {
          find: sinon.stub()
        },
        EmployeePrivilegeLeaveEligibility: {
          findOne: sinon.stub()
        },
        EmployeeSpecialLeaveEligibility: {
          find: sinon.stub()
        }
      }
    };
    var res1 = {
      keyCode: 4
    };
    var res2 = {
      keyCode: 3
    };
    var leaveCount = {
      eligibility: 21
    };
    var obj = {
            id: 3, leaveRequestYear: 2016, fromDate: '2016-09-14', toDate: '2016-09-14',
            numberOfDays: 1, status: 1, description: 'adasd', halfDaySlot: 0,
            typeOfLeavesId: 3, employeeId: 1
        };
    var specialLeaves = [
      {
      id:2, workedOn: null, status: 1, typeOfLeavesId:3,
      leaveRequestId: 3, employeeId: 1,
      typeOfLeaves: function(){
        return {id: 3, leaveType: 'Comp-off', eligibility: 0};
      }
      }
      ];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
    utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveCount);
    app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
    utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, []);
    app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, specialLeaves);
    mockery.registerMock('./utilities.js', utilMock);
    var leaveStatistics = require('../common/leave-statistics.js');
    leaveStatistics.leaveStatistics(1, function(err, statistics){
      expect(app.models.LeaveRequest.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.called).to.be.true;
      expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
      expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
      expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
      expect(utilMock.ErpKeysCode.getCall(2).args[0]).to.eql("leave_status");
      expect(utilMock.ErpKeysCode.getCall(2).args[1]).to.eql(null);
      expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
      done();
    });
  });


});