var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;


describe('cancelPendingLeaveRequest Test', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  	});

  	afterEach(function(){
    	mockery.disable();
  	});


	it('first call to ERPKeys should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub()
		};
		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {error: 'ErpKeysCode fails the leave options'}, {
        keyCode : 4
	    });

  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest({}, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(err).to.eql({error: 'ErpKeysCode fails the leave options'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});


	it('LeaveRequest should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, {
			error: "ErpKeysCode undefined"
		});
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest({}, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'ErpKeysCode undefined'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});


	it('EmployeeSpecialLeaveEligibility should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, {
			error: "EmployeeSpecialLeaveEligibility.updateAll fails"});
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'EmployeeSpecialLeaveEligibility.updateAll fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});


	it('Second call to ERPKeys should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Privilege',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, {
			error: "utilMock.ErpKeysCode fails"
		});
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'utilMock.ErpKeysCode fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});


	it('Second call to ERPKeys should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, {
			error: "utilMock.ErpKeysCode fails"
		});
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'utilMock.ErpKeysCode fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});


	it('Third call to ERPKeys should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, {
			error: "utilMock.ErpKeysCode fails"
		});		
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'utilMock.ErpKeysCode fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});


	it('utilMock.employeeDetail should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, {});
		utilMock.employeeDetail.onCall(0).callsArgWith(1, {
			error: "utilMock.employeeDetail fails"
		});		
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'utilMock.employeeDetail fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});

	it('LeaveWorkflow.create should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, {});
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, {
			error: "LeaveWorkflow.find fails"
		});		
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'LeaveWorkflow.find fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});



	it('LeaveWorkflow.find should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, {});
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});		
		app.models.LeaveWorkflow.find.onCall(0).callsArgWith(1, {
			error: "LeaveWorkflow.find fails"
		});		
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'LeaveWorkflow.find fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});


	it('LeaveWorkflow.find should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, {});
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});		
		app.models.LeaveWorkflow.find.onCall(0).callsArgWith(1, {
			error: "LeaveWorkflow.find fails"
		});		
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(err).to.eql({error: 'LeaveWorkflow.find fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});





	it('utilMock.employeeDetail fails should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, {});
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});		
		app.models.LeaveWorkflow.find.onCall(0).callsArgWith(1, null, {});		
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			done();	
  		});
		
	});



	it('LeaveWorkflow.find should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub()
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};
 		var leaveWorkflows = [{
 			actionTaken : 4,
 			toEmployee: 4
 		}];

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, key);
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, key);
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});		
		app.models.LeaveWorkflow.find.onCall(0).callsArgWith(1, null, leaveWorkflows);	
		utilMock.employeeDetail.onCall(1).callsArgWith(1, {
			error: "utilMock.employeeDetail fails"
		});	
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(utilMock.employeeDetail.called).to.be.true;
  			expect(err).to.eql({error: 'utilMock.employeeDetail fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});



	it('sendEmailNotificationToManager should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub(),
			sendEmailNotificationToManager : sinon.stub() 
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};
 		var leaveWorkflows = [{
 			actionTaken : 4,
 			toEmployee: 4
 		}];

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, key);
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, key);
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});		
		app.models.LeaveWorkflow.find.onCall(0).callsArgWith(1, null, leaveWorkflows);	
		utilMock.employeeDetail.onCall(1).callsArgWith(1, null, {});	
		utilMock.sendEmailNotificationToManager.onCall(0).callsArgWith(1, {
			error: "sendEmailNotificationToManager fails"
		});
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(utilMock.employeeDetail.called).to.be.true;
  			expect(err).to.eql({error: 'sendEmailNotificationToManager fails'});
  			expect(err).to.be.a('object');
  			done();	
  		});
		
	});

	it('sendEmailNotificationToManager should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub(),
			sendEmailNotificationToManager : sinon.stub() 
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};
 		var leaveWorkflows = [{
 			actionTaken : 4,
 			toEmployee: 4
 		}];

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, key);
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, key);
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});		
		app.models.LeaveWorkflow.find.onCall(0).callsArgWith(1, null, leaveWorkflows);	
		utilMock.employeeDetail.onCall(1).callsArgWith(1, null, {});	
		utilMock.sendEmailNotificationToManager.onCall(0).callsArgWith(1, null, {});
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(utilMock.employeeDetail.called).to.be.true;
  			done();	
  		});
		
	});



	it('sendEmailNotificationToManager should fail the cancelPendingLeaveRequest', function(done){
		var utilMock = {
			ErpKeysCode : sinon.stub(),
			employeeDetail : sinon.stub(),
			sendEmailNotificationToManager : sinon.stub() 
		};
		app = {
			models : {
				LeaveRequest: {
					updateAll: sinon.stub()
				},
				EmployeeSpecialLeaveEligibility: {
					updateAll: sinon.stub()	
				},
				LeaveWorkflow: {
					find: sinon.stub(),
					create: sinon.stub()
				}
			}
		};
		var key = {
			keyCode: 4
		};
		var req = {
        	action: 'cancel',
        	type: 'Paternity',
        	id: 3,
        	fromDate: '2016-09-09',
        	toDate: '2016-09-09',
        	numberOfDays: 1,
        	employeeId: 2,
         	comments: 'comments',
        	currentStatus : 'Pending'
 		};
 		var leaveWorkflows = [{
 			actionTaken : 3,
 			toEmployee: 4
 		}];

		utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, key);
		app.models.LeaveRequest.updateAll.onCall(0).callsArgWith(2, null, {});
		app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
		utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, key);
		utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null, key);
		utilMock.employeeDetail.onCall(0).callsArgWith(1, null, {});
		app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});		
		app.models.LeaveWorkflow.find.onCall(0).callsArgWith(1, null, leaveWorkflows);	
		utilMock.employeeDetail.onCall(1).callsArgWith(1, null, {});	
		utilMock.sendEmailNotificationToManager.onCall(0).callsArgWith(1, null, {});
  		mockery.registerMock('./utilities.js', utilMock);
  		var cancelPendingLeaveRequest = require('../common/cancelPendingLeaveRequest.js');
  		cancelPendingLeaveRequest.cancelPendingLeaveRequest(req, function(err, key){
  			expect(utilMock.ErpKeysCode.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(app.models.LeaveRequest.updateAll.called).to.be.true;
  			expect(utilMock.employeeDetail.called).to.be.true;
  			done();	
  		});
		
	});





 });

