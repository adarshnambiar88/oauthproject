var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;


describe('leaveRequestsWaitingForApprovalDataModelling Test', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  	});

  	afterEach(function(){
    	mockery.disable();
  	});


  it('should give an error at the first call of leaveRequestsWaitingForApproval ', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
      	leaveRequestsWaitingForApproval : sinon.stub()
      };
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, {
      	error: "leaveRequestsWaitingForApproval failed"
      });
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        expect(err).to.be.a('object');
        expect(err.error).to.equal('leaveRequestsWaitingForApproval failed');
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for this month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-06-06"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for today', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-06-21"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for this yesterday', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-06-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });

  it('leaveRequestsWaitingForApproval for this week', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-06-23"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for JAN month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-01-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for FEB month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-02-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });

  it('leaveRequestsWaitingForApproval for MARCH month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-03-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });

  it('leaveRequestsWaitingForApproval for APRIL month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-04-04"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for MAY month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-05-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for JULY month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-07-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for AUGUST month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-08-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for SEPETEMBER month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-09-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });

  it('leaveRequestsWaitingForApproval for OCTOBER month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-10-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for NOVEMBER month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-11-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });


  it('leaveRequestsWaitingForApproval for DECEMBER month', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };

      var leaveRequestsWaitingForApproval = {
        leaveRequestsWaitingForApproval : sinon.stub()
      };
      var data = [{
        appliedOn : "2016-12-20"
      }];
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.onCall(0).callsArgWith(1, null, data);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leaveRequestsWaitingForApproval.js', leaveRequestsWaitingForApproval);
      var leaveRequestsWaitingForApprovalDataModelling = require('../common/leaveRequestsWaitingForApprovalDataModelling');

      leaveRequestsWaitingForApprovalDataModelling.leaveRequestsWaitingForApprovalDataModelling(2, function(err, list){
        expect(leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval.called).to.be.true;
        done();
      });
      
  });

});