var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  	});

  afterEach(function(){
    	mockery.disable();
  	});

  it('should call 1st afterRemote method', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    var nextSpy = sinon.stub();

    Employee.afterRemote = sinon.stub();
    Employee.afterRemote.onCall(0).callsArgWith(1, {methodString : 'getLeave'}, {}, nextSpy);

    Employee.on = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();


    employee(Employee);
    expect(nextSpy.called).to.be.true;

    done();
  });
  

  it('should call 2nd afterRemote method', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    var nextSpy = sinon.stub();

    Employee.afterRemote = sinon.stub();
    Employee.afterRemote.onCall(1).callsArgWith(1, {methodString : 'getLeave'}, {}, nextSpy);

    Employee.on = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();


    employee(Employee);
    expect(nextSpy.called).to.be.true;
    done();
  });


  it('should fail the findById in beforeRemote method', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    var nextSpy = sinon.stub();
    Employee.afterRemote = sinon.stub();
    Employee.on = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findById = sinon.stub();
    var context = {
      req: {
        body: {
          id: 1,
          password: 'people10'
        }
      }
    };
    Employee.findById.onCall(0).callsArgWith(1, {
      error: 'before method failed'
    });
    Employee.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
    employee(Employee);
    expect(nextSpy.called).to.be.true;
    done();
  });

  it('should fail the user hasPassword in beforeRemote method', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    var nextSpy = sinon.stub();
    Employee.afterRemote = sinon.stub();
    Employee.on = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findById = sinon.stub();
   // user.updateAttribute = sinon.stub();
   //user.hasPassword = sinon.stub();
    var context = {
      req: {
        body: {
          id: 1,
          password: 'people10'
        }
      }
    };
    var user = {
      hasPassword : sinon.stub(),
      loginStatus: 0 
    };
    Employee.findById.onCall(0).callsArgWith(1, null, user);
    user.hasPassword.onCall(0).callsArgWith(1,{
      error: 'before method failed'
    });
    Employee.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
    employee(Employee);
    expect(nextSpy.called).to.be.true;
    done();
  });

  it('should fail the user has Matching Password in beforeRemote method', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    var nextSpy = sinon.stub();
    Employee.afterRemote = sinon.stub();
    Employee.on = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findById = sinon.stub();
    var context = {
      req: {
        body: {
          id: 1,
          password: 'people10'
        }
      }
    };
    var user = {
      loginStatus: 0 
    };
    var user = {
      hasPassword : sinon.stub()
    };
    Employee.findById.onCall(0).callsArgWith(1, null, user);
    user.hasPassword.onCall(0).callsArgWith(1, null, 1);
    Employee.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
    employee(Employee);
    expect(nextSpy.called).to.be.true;
    done();
  });

  it('should fail the update method in beforeRemote method', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    var nextSpy = sinon.stub();
    Employee.afterRemote = sinon.stub();
    Employee.on = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findById = sinon.stub();
    var context = {
      req: {
        body: {
          id: 1,
          password: 'people10'
        }
      }
    };
    var user = {
      loginStatus: 0 
    };
    var user = {};
    user.hasPassword = sinon.stub();
    user.updateAttribute = sinon.stub();
    Employee.findById.onCall(0).callsArgWith(1, null, user);
    user.hasPassword.onCall(0).callsArgWith(1, null, 0);
    user.updateAttribute.onCall(0).callsArgWith(2, {
      error: 'update error'
    });
    Employee.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
    employee(Employee);
    expect(nextSpy.called).to.be.true;
    done();
  });

  it('should not fail the beforeRemote method', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    var nextSpy = sinon.stub();
    Employee.afterRemote = sinon.stub();
    Employee.on = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findById = sinon.stub();
    var context = {
      req: {
        body: {
          id: 1,
          password: 'people10'
        }
      }
    };
    var user = {
      loginStatus: 0 
    };
    var user = {};
    user.hasPassword = sinon.stub();
    user.updateAttribute = sinon.stub();
    Employee.findById.onCall(0).callsArgWith(1, null, user);
    user.hasPassword.onCall(0).callsArgWith(1, null, 0);
    user.updateAttribute.onCall(0).callsArgWith(2, null, user);
    Employee.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
    employee(Employee);
    expect(nextSpy.called).to.be.true;
    done();
  });



  it('should fail the reset password method',function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    app = {
      models: {
        Email: {
          send: sinon.stub()
        }
      }
    };
    var fun = {
      accessToken: {userId: 1, id: 'moFoRZhewWCXvorVnybGTorg8yH6LZChbidwULRdPO3fuJM1fWCXa4S0w3sN5RGd'},
      email: 'erp4people10@gmail.com'
    };
    Employee.afterRemote = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.on = sinon.stub();
     app.models.Email.send.onCall(0).callsArgWith(1, {
      error: 'Email sending failed'
    });
    Employee.on.onCall(0).callsArgWith(1, fun);
    employee(Employee);
    expect(app.models.Email.send.called).to.be.true;
    done();
  });
  


  it('should send the reset password link',function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    app = {
      models: {
        Email: {
          send: sinon.stub()
        }
      }
    };
    app.models.Email.send.onCall(0).callsArgWith(1, null);
    var fun = {
      accessToken: {userId: 1, id: 'moFoRZhewWCXvorVnybGTorg8yH6LZChbidwULRdPO3fuJM1fWCXa4S0w3sN5RGd'},
      email: 'erp4people10@gmail.com'
    };
    Employee.afterRemote = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.on = sinon.stub();
    Employee.on.onCall(0).callsArgWith(1, fun);
    employee(Employee);
    expect(app.models.Email.send.called).to.be.true;
    done();
  });

  it('should fail the getApprovars', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    Employee.afterRemote = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findOne = sinon.stub();
    Employee.on = sinon.stub();
    Employee.findOne.onCall(0).callsArgWith(1, {
      error: 'Error in approver list'
    });
    employee(Employee);
    Employee.getApprovars(1, function(err, approverList){
      expect(Employee.findOne.called).to.be.true;
      expect(Employee.findOne.getCall(0).args[0].where.id).to.equal(1);
      expect(err).to.eql({error: 'Error in approver list'});
      done();
    });
  });


  it('should give the empty array in case of highest management user', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    Employee.afterRemote = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findOne = sinon.stub();
    Employee.on = sinon.stub();
    var emp = {
      levelOfApprovalRequired: 0,
      reportingManagerId:2
    };
    Employee.findOne.onCall(0).callsArgWith(1, null, emp);
    employee(Employee);
    Employee.getApprovars(1, function(err, approverList){
      expect(Employee.findOne.called).to.be.true;
      expect(Employee.findOne.getCall(0).args[0].where.id).to.equal(1);
      done();
    });
  });


  it('should fail in finding the reporting manager details', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    Employee.afterRemote = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findOne = sinon.stub();
    Employee.on = sinon.stub();
    var emp = {
      levelOfApprovalRequired: 1,
      reportingManagerId:2
    };
    Employee.findOne.onCall(0).callsArgWith(1, null, emp);
    Employee.findOne.onCall(1).callsArgWith(1, {
      error: 'Error in finding the manager'
    });
    employee(Employee);
    Employee.getApprovars(1, function(err, approverList){
      expect(Employee.findOne.called).to.be.true;
      expect(Employee.findOne.getCall(0).args[0].where.id).to.equal(1);
      expect(Employee.findOne.getCall(1).args[0].where.id).to.equal(2);
      expect(err).to.eql({error: 'Error in finding the manager'});
      done();
    });
  });



  it('should give the approver list', function(done){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    Employee.afterRemote = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    Employee.beforeRemote = sinon.stub();
    Employee.findOne = sinon.stub();
    Employee.on = sinon.stub();
    var emp = {
      levelOfApprovalRequired: 1,
      reportingManagerId:2
    };
    var res = {
      name: 'adarsh',
      id: 2,
      reportingManagerId: null
    };
    Employee.findOne.onCall(0).callsArgWith(1, null, emp);
    Employee.findOne.onCall(1).callsArgWith(1, null, res);
    employee(Employee);
    Employee.getApprovars(1, function(err, approverList){
      expect(Employee.findOne.called).to.be.true;
      expect(Employee.findOne.getCall(0).args[0].where.id).to.equal(1);
      expect(Employee.findOne.getCall(1).args[0].where.id).to.equal(2);
      expect(approverList).to.be.a('array');
      expect(approverList[0].name).to.equal('adarsh');
      expect(approverList[0].id).to.equal(2);
      done();
    });
  });
    

  it('should get the team members', function(){
    var employee = require('../common/models/employee.js');
    var Employee = {};
    Employee.afterRemote = sinon.stub();
    Employee.remoteMethod = sinon.stub();
    
  });



});