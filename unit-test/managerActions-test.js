
var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  });

  afterEach(function(){
    	mockery.disable();
  });


  it('ErpKeysCode should fail the managerActions.js', function(done){
  	var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
      error: 'ErpKeysCode fails the manager-actions'
    });
    var actions = [{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Approve",
        "employeeId": "4"
    }];
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
    	expect(err).to.be.a('object');
      expect(err).to.eql({error: 'ErpKeysCode fails the manager-actions'});
    	done();
    });
  });


  it('app.models.Employee should fail the managerActions.js', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actions = [{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Approve",
        "employeeId": "4"
    }];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    app.models.Employee.findOne.onCall(0).callsArgWith(1, {
      error: 'Employee not found'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'Employee not found'});
      done();
    });
  });

  it('LeaveWorkflow.create should fail the managerActions.js(employee reporting manager as null)', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        },
        LeaveWorkflow: {
            create: sinon.stub()
          }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var employeeData = {
      empId : "P10E0001",
      reportingManagerId : null
    };
    var actions = [{
        "fromEmployee": "3",
        "comments": "",
        "leaveRequestId": "161",
        "actionByManager": "Approve",
        "employeeId": "4"
    }];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    app.models.Employee.findOne.onCall(0).callsArgWith(1, null, employeeData);
    app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, {
      error: 'LeaveWorkflow fails the managerActions.js'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'LeaveWorkflow fails the managerActions.js'});
      done();
    });
  });


it('LeaveWorkflow.create should fail the managerActions.js(employee reporting manager as not null)', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        },
        LeaveWorkflow: {
            create: sinon.stub()
          }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var employeeData = {
      reportingManagerId : 5
    };
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    var actions = [{
        "fromEmployee": "3",
        "comments": "",
        "leaveRequestId": "161",
        "actionByManager": "Approve",
        "employeeId": "4"
    }];
    app.models.Employee.findOne.onCall(0).callsArgWith(1, null, employeeData);
    mockery.registerMock('./utilities.js', utilMock);
     app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, {
      error: 'LeaveWorkflow fails the managerActions.js'
    });
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'LeaveWorkflow fails the managerActions.js'});
      done();
    });
  });


it('LeaveWorkflow.create should fail the managerActions.js action taken = deny', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        },
        LeaveWorkflow: {
            create: sinon.stub()
          }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actions = [{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Deny",
        "employeeId": "4"
    }];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(0).callsArgWith(1, {
      error: 'Employee not found'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'Employee not found'});
      done();
    });
  });


it('LeaveWorkflow.create should fail the managerActions.js action taken = deny', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        },
        LeaveWorkflow: {
            create: sinon.stub()
          }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actions = [{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Sendback",
        "employeeId": "4"
    }];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(0).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(1).callsArgWith(1, {
      error: 'Employee not found'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'Employee not found'});
      done();
    });
  });


it('LeaveRequest.findOne should fail the managerActions.js action taken = sendback', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        },
        LeaveWorkflow: {
            create: sinon.stub()
          },
        LeaveRequest: {
            findOne: sinon.stub()
          }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
     var actions = [{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Sendback",
        "employeeId": "4"
    }];
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(0).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(1).callsArgWith(1, null, {});
    app.models.LeaveRequest.findOne.onCall(0).callsArgWith(1, {
      error: 'LeaveRequest fails'
    });
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
      expect(err).to.be.a('object');
      expect(err).to.eql({error: 'LeaveRequest fails'});
      done();
    });
  });

it('util.sendEmailNotificationToEmployee should fail the managerActions.js action taken = sendback', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        },
        LeaveWorkflow: {
            create: sinon.stub()
          },
        LeaveRequest: {
            findOne: sinon.stub()
          }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actions = [{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Sendback",
        "employeeId": "4"
    }];

    utilMock.sendEmailNotificationToEmployee = sinon.stub();
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    app.models.Employee.findOne.onCall(0).callsArgWith(1, null, {});
    app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(1).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(2).callsArgWith(1, null, {});
    app.models.LeaveRequest.findOne.onCall(0).callsArgWith(1, null, {});
    utilMock.sendEmailNotificationToEmployee.onCall(0).callsArgWith(1, 'Email Notification Failed');
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
      expect(err).to.be.a('string');
      expect(err).to.eql('Email Notification Failed');
      done();
    });
  });


it('actions managerActions should not fail', function(done){
    app = {
      models: {
        Employee: {
          findOne: sinon.stub()
        },
        LeaveWorkflow: {
            create: sinon.stub()
          },
        LeaveRequest: {
            findOne: sinon.stub()
          }
      }
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    var actions = [{
        "fromEmployee": "3",
        "comments": "sadsadfdf",
        "leaveRequestId": "161",
        "actionByManager": "Sendback",
        "employeeId": "4"
    }];

    utilMock.sendEmailNotificationToEmployee = sinon.stub();
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, 2);
    app.models.Employee.findOne.onCall(0).callsArgWith(1, null, {});
    app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(1).callsArgWith(1, null, {});
    app.models.Employee.findOne.onCall(2).callsArgWith(1, null, {});
    app.models.LeaveRequest.findOne.onCall(0).callsArgWith(1, null, {});
    utilMock.sendEmailNotificationToEmployee.onCall(0).callsArgWith(1, null, "");
    mockery.registerMock('./utilities.js', utilMock);
    var manager = require('../common/manager-actions.js');
    manager.managerActions(actions, function(err, actionTaken){
    
      done();
    });
  });




});