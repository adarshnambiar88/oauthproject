var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  });

  afterEach(function(){
    	mockery.disable();
  });


//   it('should call the first after remoteMethod', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveRequest(LeaveRequest);
//     expect(nextSpy.called).to.be.true;
//     done();
//   });



//   it('leaveRequestList.getList should fails the leaveRequest', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {};
//     var leaveRequestListMock = {
//      getList: sinon.stub()
//     };
//     var utilMock = {};
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../getLeaveRequestList.js', leaveRequestListMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveRequestListMock.getList.onCall(0).callsArgWith(1, {
//      error: 'leaveRequestList.getList fails'
//     });
//     leaveRequest(LeaveRequest);
//     LeaveRequest.getList(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(err).to.be.a('object');
//       expect(err).to.eql({error: 'leaveRequestList.getList fails'});
//       done();
//     });
//   });



//  it('should give an empty list of leaves', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {};
//     var leaveRequestListMock = {
//      getList: sinon.stub()
//     };
//     var utilMock = {};
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../getLeaveRequestList.js', leaveRequestListMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveRequestListMock.getList.onCall(0).callsArgWith(1, null, []);
//     leaveRequest(LeaveRequest);
//     LeaveRequest.getList(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(leaveRequestListMock.getList.called).to.be.true;
//       expect(list).to.be.eql([]);
//       done();
//     });
//   });



// it('should give an denied list of leaves', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {};
//     var leaveRequestListMock = {
//      getList: sinon.stub()
//     };
//     var leaves = [{
//       status: "Denied",
//       fromDate: "2016-06-09"
//     }];
//     var utilMock = {};
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../getLeaveRequestList.js', leaveRequestListMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveRequestListMock.getList.onCall(0).callsArgWith(1, null, leaves);
//     leaveRequest(LeaveRequest);
//     LeaveRequest.getList(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(leaveRequestListMock.getList.called).to.be.true;
//       expect(list).to.be.eql([]);
//       done();
//     });
//   });



// it('should pass when leave is yet to be taken', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {};
//     var leaveRequestListMock = {
//      getList: sinon.stub()
//     };
//     var utilMock = {};
//     var leaves = [{
//       status: "Approved",
//       fromDate: "2016-06-09"
//     }];
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../getLeaveRequestList.js', leaveRequestListMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveRequestListMock.getList.onCall(0).callsArgWith(1, null, leaves);
//     leaveRequest(LeaveRequest);
//     LeaveRequest.getList(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(leaveRequestListMock.getList.called).to.be.true;
//       done();
//     });
//   });


// it('should pass when leave is availed', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {};
//     var leaveRequestListMock = {
//      getList: sinon.stub()
//     };
//     var utilMock = {};
//     var leaves = [{
//       status: "Approved",
//       fromDate: "2016-03-09"
//     }];
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../getLeaveRequestList.js', leaveRequestListMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveRequestListMock.getList.onCall(0).callsArgWith(1, null, leaves);
//     leaveRequest(LeaveRequest);
//     LeaveRequest.getList(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(leaveRequestListMock.getList.called).to.be.true;
//       done();
//     });
//   });


// it('should pass when leave is Pending', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {};
//     var leaveRequestListMock = {
//      getList: sinon.stub()
//     };
//     var utilMock = {};
//     var leaves = [{
//       status: "Pending",
//       fromDate: "2016-06-09"
//     }];
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../getLeaveRequestList.js', leaveRequestListMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveRequestListMock.getList.onCall(0).callsArgWith(1, null, leaves);
//     leaveRequest(LeaveRequest);
//     LeaveRequest.getList(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(leaveRequestListMock.getList.called).to.be.true;
//       done();
//     });
//   });



// it('leaveSummaryMock.leaveStatistics should fail the leaveRequest.leaveStatistics', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {
//       leaveStatistics: sinon.stub()
//     };
//     var utilMock = {};
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveSummaryMock.leaveStatistics.onCall(0).callsArgWith(1, {
//       error: "leaveSummaryMock.leaveStatistics fails"
//     });
//     leaveRequest(LeaveRequest);
//     LeaveRequest.leaveStatistics(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(leaveSummaryMock.leaveStatistics.called).to.be.true;
//       expect(err).to.be.a('object');
//       expect(err).to.eql({error: "leaveSummaryMock.leaveStatistics fails"});;
//       done();
//     });
//   });


//   it('leaveSummaryMock.leaveStatistics should not fail the leaveRequest.leaveStatistics', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var leaveSummaryMock = {
//       leaveStatistics: sinon.stub()
//     };
//     var utilMock = {};
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.afterRemote.onCall(0).callsArgWith(1, {}, {}, nextSpy);
//     leaveSummaryMock.leaveStatistics.onCall(0).callsArgWith(1, null, {});
//     leaveRequest(LeaveRequest);
//     LeaveRequest.leaveStatistics(1, function(err, list){
//       expect(nextSpy.called).to.be.true;
//       expect(leaveSummaryMock.leaveStatistics.called).to.be.true;
//       done();
//     });
//   });

//   it('utilMock.ErpKeysCode should fail the leaveRequest.leaveStatistics', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "cancel"
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
//        error: 'utilMock.ErpKeysCode fails'
//      });
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       expect(utilMock.ErpKeysCode.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });


//   it('utilMock.ErpKeysCode should not fail the leaveRequest.leaveStatistics', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Paternity",
//           action: "cancel"
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, {
//       error: 'EmployeeSpecialLeaveEligibility.updateAll fails'
//     });
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       expect(utilMock.ErpKeysCode.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });

//    it('utilMock.ErpKeysCode should not fail the leaveRequest.leaveStatistics(Maternity)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Maternity",
//           action: "cancel"
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, {
//       error: 'EmployeeSpecialLeaveEligibility.updateAll fails'
//     });
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       expect(utilMock.ErpKeysCode.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });


//   it('utilMock.ErpKeysCode should not fail the leaveRequest.leaveStatistics(Comp-off)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Comp-off",
//           action: "cancel"
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       expect(utilMock.ErpKeysCode.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });


//   it('utilMock.ErpKeysCode should not fail the leaveRequest.leaveStatistics(Comp-off)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "cancel"
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, {
//       error: 'EmployeeSpecialLeaveEligibility.updateAll fails'
//     });
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       expect(utilMock.ErpKeysCode.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });


//    it('should not fail the beforeRemote(action = update)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "update",
//           numberOfDays: 0
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//     expect(LeaveRequest.beforeRemote.called).to.be.true;
//     done();
//   });



//   it('should not fail the beforeRemote(action = update)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//     expect(LeaveRequest.beforeRemote.called).to.be.true;
//     done();
//   });

//    it('should fail the beforeRemote(action = update)(Paternity)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Paternity",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, {
//       error: 'EmployeeSpecialLeaveEligibility.updateAll fails'
//     });
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });



//   it('should fail the beforeRemote(action = update)(Maternity)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Maternity",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });


//   it('should fail the beforeRemote(action = update)(Comp-off)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Comp-off",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode: sinon.stub()
//     };
//     app = {
//       models: {
//        EmployeeSpecialLeaveEligibility: {
//         updateAll: sinon.stub()
//        } 
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     app.models.EmployeeSpecialLeaveEligibility.updateAll.onCall(0).callsArgWith(2, null, {});
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy)
//     leaveRequest(LeaveRequest);
//       //expect(nextSpy.called).to.be.true;
//       expect(LeaveRequest.beforeRemote.called).to.be.true;
//       //expect(err.called).to.be.true;
//       done();
//   });




// it('should fail the beforeRemote(action = update) date conflicts', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//      dateConflicts: sinon.stub()
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
    
//     utilMock.dateConflicts.onCall(0).callsArgWith(1, {
//       error: 'utilMock.dateConflicts fails'
//     });
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
//     leaveRequest(LeaveRequest);
//     //expect(utilMock.dateConflicts.called).to.be.true;
//     done();
//   });


// it('should fail the beforeRemote(action = update) ErpKeysCode', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode:sinon.stub(),
//       dateConflicts: sinon.stub()
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     utilMock.dateConflicts.onCall(0).callsArgWith(1, null, "");
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
//       error: 'utilMock.ErpKeysCode fails'
//     });
//     leaveRequest(LeaveRequest);
//     // expect(utilMock.dateConflicts.called).to.be.true;
//     // expect(utilMock.ErpKeysCode.called).to.be.true;
//     done();
//   });


// it('LeaveWorkflow should fail the beforeRemote(action = update)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode:sinon.stub(),
//       dateConflicts: sinon.stub(),

//     };
//     app = {
//       models: {
//         LeaveWorkflow: {
//           create: sinon.stub()
//         }
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     utilMock.dateConflicts.onCall(0).callsArgWith(1, null, "");
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, {
//       error: 'LeaveWorkflow fails'
//     });
//     leaveRequest(LeaveRequest);
//     // expect(utilMock.dateConflicts.called).to.be.true;
//     // expect(utilMock.ErpKeysCode.called).to.be.true;
//     done();
//   });


// it('updateSpecialLeaves should fail the beforeRemote(action = update)', function(done){
//     var leaveRequest = require('../common/models/leave-request.js');
//     var LeaveRequest = {};
//     var nextSpy = sinon.stub();
//     LeaveRequest.afterRemote = sinon.stub();
//     LeaveRequest.remoteMethod = sinon.stub();
//     LeaveRequest.beforeRemote = sinon.stub();
//     var specialLeavesMock = {};
//     var context = {
//       req: {
//         body: {
//           type: "Privilege",
//           action: "update",
//           numberOfDays: 1
//         }
//       }
//     };
//     var utilMock = {
//       ErpKeysCode:sinon.stub(),
//       dateConflicts: sinon.stub(),
//       updateSpecialLeaves: sinon.stub()
//     };
//     app = {
//       models: {
//         LeaveWorkflow: {
//           create: sinon.stub()
//         }
//       }
//     };
//     mockery.registerMock('../leave-options.js', specialLeavesMock);
//    // mockery.registerMock('../leave-statistics.js', leaveSummaryMock);
//     mockery.registerMock('../utilities.js', utilMock);
//     utilMock.dateConflicts.onCall(0).callsArgWith(1, null, "");
//     LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
//     utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
//     app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, "");
//      utilMock.updateSpecialLeaves.onCall(0).callsArgWith(1, {
//       error: 'updateSpecialLeaves fails'
//      });
//     leaveRequest(LeaveRequest);
//     // expect(utilMock.dateConflicts.called).to.be.true;
//     // expect(utilMock.ErpKeysCode.called).to.be.true;
//     done();
//   });




it('should run the beforeRemote(action = update)', function(done){
    var leaveRequest = require('../common/models/leave-request.js');
    var LeaveRequest = {};
    var nextSpy = sinon.stub();
    LeaveRequest.afterRemote = sinon.stub();
    LeaveRequest.remoteMethod = sinon.stub();
    LeaveRequest.beforeRemote = sinon.stub();
    var specialLeavesMock = {};
    var context = {
      req: {
        body: {
          type: "Privilege",
          action: "update",
          numberOfDays: 1
        }
      }
    };
    var utilMock = {
      updateSpecialLeaves: sinon.stub()
    };
    utilMock.dateConflicts = sinon.stub();
    utilMock.ErpKeysCode = sinon.stub();
    app = {
      models: {
        LeaveWorkflow: {
          create: sinon.stub()
        }
      }
    };
    utilMock.dateConflicts.onCall(0).callsArgWith(1, null, "");
    LeaveRequest.beforeRemote.onCall(0).callsArgWith(1, context, {}, nextSpy);
    utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
    app.models.LeaveWorkflow.create.onCall(0).callsArgWith(1, null, "");
    utilMock.updateSpecialLeaves.onCall(0).callsArgWith(1, null, "");
    mockery.registerMock('../leave-options.js', specialLeavesMock);
    mockery.registerMock('../utilities.js', utilMock);

    leaveRequest(LeaveRequest);
    console.log(utilMock.updateSpecialLeaves);
    expect(utilMock.updateSpecialLeaves.called).to.be.true;
    expect(utilMock.ErpKeysCode.called).to.be.true;
    expect(LeaveRequest.beforeRemote.called).to.be.true;
    expect(app.models.LeaveWorkflow.called).to.be.true;
    expect(utilMock.dateConflicts.called).to.be.true;
    done();
  });


});