var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  });

  afterEach(function(){
    	mockery.disable();
  });


  it('EmployeePrivilegeLeaveEligibility should fail leave-options', function(done){
  	var leaveOption = require('../common/leave-options.js');
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
  				findOne : sinon.stub()
  			}
  		}
  	};
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, {
  		error: 'EmployeePrivilegeLeaveEligibility gives an error'
  	});
  	leaveOption.leaveOptions(12,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(err).to.eql({error: 'EmployeePrivilegeLeaveEligibility gives an error'});
  		expect(err).to.be.a('object');
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.getCall(0).args[0].where).to.eql({employeeId: 12});
  		done();
  	});
  });



 it('first call of ErpKeysCode should fail leave-options', function(done){
    app = {
      models: {
        EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        }
      }
    };
    var leaveType = {
      eligibility: 21
    };
    var utilMock = {
      ErpKeysCode: sinon.stub()
    };
    app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, null);
    mockery.registerMock('./utilities.js', utilMock);
    var leaveOption = require('../common/leave-options.js');
    leaveOption.leaveOptions(12,function(err, leaves){
      expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
      expect(err).to.eql('No Privilege Count');
      expect(err).to.be.a('string');
      done();
    });
  });


  it('first call of ErpKeysCode should fail leave-options', function(done){
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        }
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
  		error: 'ErpKeysCode fails the leave options'
  	});
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(12,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(err).to.eql({error: 'ErpKeysCode fails the leave options'});
  		expect(err).to.be.a('object');
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		done();
  	});
  });


  it('second call of ErpKeysCode should fail leave-options', function(done){
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        }
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, {});
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, {
  		error: 'Denied status is not identified'
  	});
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(12,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(err).to.eql({error: 'Denied status is not identified'});
  		expect(err).to.be.a('object');
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		done();
  	});
  });


  it('LeaveRequest.find should fail leave-options', function(done){
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        },
  			LeaveRequest: {
  				find: sinon.stub()
  			}
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	var res1 = {
  		keyCode: 4
  	};
  	var res2 = {
  		keyCode: 3
  	};
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
  	app.models.LeaveRequest.find.onCall(0).callsArgWith(1, {
  		error: 'LeaveRequest.find failed'
  	});
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(12,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(app.models.LeaveRequest.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(err).to.eql({error: 'LeaveRequest.find failed'});
  		expect(err).to.be.a('object');
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
  		done();
  	});
  });



it('LeaveRequest.find shouldn"t fail leave-options', function(done){
  	app = {
  		models: {
        EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        },
  			LeaveRequest: {
  				find: sinon.stub()
  			},
  			EmployeeSpecialLeaveEligibility: {
  				find: sinon.stub()
  			}
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var leave = [{
  		id: 1, leaveRequestYear: 2016, fromDate: '2016-05-12', toDate: '2016-05-12',
  		numberOfDays: 1, status: 1, description: 'adasd', halfDaySlot: 0,
  		typeOfLeavesId: 1, employeeId: 1,
  		typeOfLeaves: function(){
  			return {id: 1, leaveType: 'Privilege', eligibility: 21};
  		} 
  	}];
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	var res1 = {
  		keyCode: 4
  	};
  	var res2 = {
  		keyCode: 3
  	};
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
  	app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, leave);
  	app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, {
  		error: 'EmployeeSpecialLeaveEligibility.find'
  	});
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(1 ,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(app.models.LeaveRequest.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
  		expect(err).to.eql({error: 'EmployeeSpecialLeaveEligibility.find'});
  		expect(err).to.be.a('object');
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
  		done();
  	});
  });


it('EmployeeSpecialLeaveEligibility should fail leave-options', function(done){
  	app = {
  		models: {
        EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        },
  			LeaveRequest: {
  				find: sinon.stub()
  			},
  			EmployeeSpecialLeaveEligibility: {
  				find: sinon.stub()
  			}
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	var res1 = {
  		keyCode: 4
  	};
  	var res2 = {
  		keyCode: 3
  	};
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
  	app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
  	app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, {
  		error: 'EmployeeSpecialLeaveEligibility.find'
  	});
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(1 ,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(app.models.LeaveRequest.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
  		expect(err).to.eql({error: 'EmployeeSpecialLeaveEligibility.find'});
  		expect(err).to.be.a('object');
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
  		done();
  	});
  });



it('EmployeeSpecialLeaveEligibility should not fail leave-options', function(done){
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        },
  			LeaveRequest: {
  				find: sinon.stub()
  			},
  			EmployeeSpecialLeaveEligibility: {
  				find: sinon.stub()
  			}
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	var res1 = {
  		keyCode: 4
  	};
  	var res2 = {
  		keyCode: 3
  	};
  
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
  	app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
  	app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, []);
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(1 ,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(app.models.LeaveRequest.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
  		done();
  	});
  });


it('EmployeeSpecialLeaveEligibility should not fail leave-options', function(done){
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        },
  			LeaveRequest: {
  				find: sinon.stub()
  			},
  			EmployeeSpecialLeaveEligibility: {
  				find: sinon.stub()
  			}
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	var res1 = {
  		keyCode: 4
  	};
  	var res2 = {
  		keyCode: 3
  	};
  	var specialLeaves = [{
  		id:1, workedOn: null, status: 0, typeOfLeavesId:5,
  		leaveRequestId: null, employeeId: 1,
  		typeOfLeaves: function(){
  			return {id: 5, leaveType: 'Paternity', eligibility: 5}
  		} 
  	}];
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
  	app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
  	app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, specialLeaves);
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(1 ,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(app.models.LeaveRequest.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
  		done();
  	});
  });


it('EmployeeSpecialLeaveEligibility should not fail leave-options', function(done){
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        },
  			LeaveRequest: {
  				find: sinon.stub()
  			},
  			EmployeeSpecialLeaveEligibility: {
  				find: sinon.stub()
  			}
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	var res1 = {
  		keyCode: 4
  	};
  	var res2 = {
  		keyCode: 3
  	};
  	var specialLeaves = [{
  		id:1, workedOn: null, status: 0, typeOfLeavesId:5,
  		leaveRequestId: null, employeeId: 1,
  		typeOfLeaves: function(){
  			return {id: 4, leaveType: 'Maternity', eligibility: 90}
  		} 
  	}];
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
  	app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
  	app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, specialLeaves);
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(1 ,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(app.models.LeaveRequest.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
  		done();
  	});
  });



it('EmployeeSpecialLeaveEligibility should not fail leave-options', function(done){
  	app = {
  		models: {
  			EmployeePrivilegeLeaveEligibility: {
          findOne : sinon.stub()
        },
  			LeaveRequest: {
  				find: sinon.stub()
  			},
  			EmployeeSpecialLeaveEligibility: {
  				find: sinon.stub()
  			}
  		}
  	};
  	var leaveType = {
  		eligibility: 21
  	};
  	var utilMock = {
  		ErpKeysCode: sinon.stub()
  	};
  	var res1 = {
  		keyCode: 4
  	};
  	var res2 = {
  		keyCode: 3
  	};
  	var specialLeaves = [{
  		id:1, workedOn: null, status: 0, typeOfLeavesId:5,
  		leaveRequestId: null, employeeId: 1,
  		typeOfLeaves: function(){
  			return {id: 3, leaveType: 'Comp-off', eligibility: 1}
  		} 
  	}];
  	app.models.EmployeePrivilegeLeaveEligibility.findOne.onCall(0).callsArgWith(1, null, leaveType);
  	utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null, res1);
  	utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null, res2);
  	app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
  	app.models.EmployeeSpecialLeaveEligibility.find.onCall(0).callsArgWith(1, null, specialLeaves);
  	mockery.registerMock('./utilities.js', utilMock);
  	var leaveOption = require('../common/leave-options.js');
  	leaveOption.leaveOptions(1 ,function(err, leaves){
  		expect(app.models.EmployeePrivilegeLeaveEligibility.findOne.called).to.be.true;
  		expect(app.models.LeaveRequest.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.called).to.be.true;
  		expect(app.models.EmployeeSpecialLeaveEligibility.find.called).to.be.true;
  		expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(0).args[1]).to.eql("Cancelled");
  		expect(utilMock.ErpKeysCode.getCall(1).args[0]).to.eql("leave_status");
  		expect(utilMock.ErpKeysCode.getCall(1).args[1]).to.eql("Denied");
  		expect(app.models.LeaveRequest.find.getCall(0).args[0].where.and[2].status.nin).to.eql([4,3]);
  		done();
  	});
  });

});
