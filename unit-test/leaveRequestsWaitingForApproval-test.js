var mockery = require('mockery');
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
                                            //istanbul cover node_modules/mocha/bin/_mocha unit-test/  --recursive
describe('My Test1', function(){

	beforeEach(function(){
    	mockery.enable({
    		useCleanCache: true,
    		warnOnReplace: false,
    		warnOnUnregistered: false
    	});
  	});

  	afterEach(function(){
    	mockery.disable();
  	});


  it('should give an error at the first call of ErpKeysCode ', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, {
        error: 'Cancel status is undefined'
      },
      {
        keyCode : 3
      });

      mockery.registerMock('./utilities.js', utilMock);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');

      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(err.error).to.equal('Cancel status is undefined');
        done();
      });
      
  });


   it('should give an error at the second call of ErpKeysCode ', function(done){

      var utilMock = {
        ErpKeysCode : sinon.stub()
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, {
        error: 'Cancellation in progress status is undefined'
      },
      {
        keyCode : 3
      });

      mockery.registerMock('./utilities.js', utilMock);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(err.error).to.equal('Cancellation in progress status is undefined');
        done();
      });
      
  });


    it('should give an error at the third call of ErpKeysCode ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub()
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, {
        error: 'Approved status is undefined'
      },
      {
        keyCode : 3
      });

      mockery.registerMock('./utilities.js', utilMock);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        expect(err.error).to.equal('Approved status is undefined');
        done();
      });
      
  });



    it('should give an error at the third call of ErpKeysCode ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, {
        error : "latestTimestamp gives error on call 0"
      });

      mockery.registerMock('./utilities.js', utilMock);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        expect(err.error).to.equal('latestTimestamp gives error on call 0');
        done();
      });
      
  });

  it('should give an error at the first call of LeaveWorkflow ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          }  
        }
      };
      app.models.LeaveWorkflow.findOne
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, {
        error : "LeaveWorkflow is undefined"
      });

      mockery.registerMock('./utilities.js', utilMock);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        expect(err.error).to.equal('LeaveWorkflow is undefined');
        done();
      });
      
  });


  it('should give an error at the first call of ErpKeys ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 5
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, {
        error: "ErpKeys is undefined"
      });

      mockery.registerMock('./utilities.js', utilMock);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
         expect(err.error).to.equal('ErpKeys is undefined');
        done();
      });
      
  });



  it('should give an error at the first call of TypeOfLeaves ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDay : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 4
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });
      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, {
        error: "TypeOfLeaves is undefined"
      });      
      mockery.registerMock('./utilities.js', utilMock);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
         expect(err.error).to.equal('TypeOfLeaves is undefined');
        done();
      });
      
  });



  it('should give an error at the first call of leaveSummary.leaveStatistics ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      var leaveSummary = {
        leaveStatistics: sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDaySlot : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });

      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, null, {}); 
      leaveSummary.leaveStatistics.onCall(0).callsArgWith(1 , {
        error: "leaveSummary.leaveStatistics is undefined"
      })
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leave-statistics.js', leaveSummary);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        expect(err.error).to.equal('leaveSummary.leaveStatistics is undefined');
        done();
      });
      
  });



  it('should give an error at the second call of LeaveWorkflow ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      var leaveSummary = {
        leaveStatistics: sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDaySlot : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });

      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, null, {}); 
      leaveSummary.leaveStatistics.onCall(0).callsArgWith(1 , null, {});
      app.models.LeaveWorkflow.findOne.onCall(1).callsArgWith(1, {
        error : "LeaveWorkflow.findOne is undefined"
      });
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leave-statistics.js', leaveSummary);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        expect(err.error).to.equal('LeaveWorkflow.findOne is undefined');
        done();
      });

  });


  it('should give an error at the fourth call of ErpKeys ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      var leaveSummary = {
        leaveStatistics: sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDaySlot : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(3).callsArgWith(2, {
        error: "ErpKeys undefined"
      },
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });

      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, null, {}); 
      leaveSummary.leaveStatistics.onCall(0).callsArgWith(1 , null, {});
      app.models.LeaveWorkflow.findOne.onCall(1).callsArgWith(1, null, {});
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leave-statistics.js', leaveSummary);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        expect(err.error).to.equal('ErpKeys undefined');
        done();
      });

  });


  it('should give an error at the first call of LeaveRequest ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      var leaveSummary = {
        leaveStatistics: sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          },
          LeaveRequest: {
            find: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDaySlot : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(3).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });

      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, null, {}); 
      leaveSummary.leaveStatistics.onCall(0).callsArgWith(1 , null, {});
      app.models.LeaveWorkflow.findOne.onCall(1).callsArgWith(1, null, {});
      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, {
        error: "LeaveRequest is undefined"});
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leave-statistics.js', leaveSummary);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        expect(err.error).to.equal('LeaveRequest is undefined');
        done();
      });

  });


  it('should give an empty array at the first call of LeaveRequest ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      var leaveSummary = {
        leaveStatistics: sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          },
          LeaveRequest: {
            find: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDaySlot : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(3).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });

      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, null, {}); 
      leaveSummary.leaveStatistics.onCall(0).callsArgWith(1 , null, {});
      app.models.LeaveWorkflow.findOne.onCall(1).callsArgWith(1, null, {});
      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, []);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leave-statistics.js', leaveSummary);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        //expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        //expect(err.error).to.equal('LeaveRequest is undefined');
        done();
      });

  });



 it('should give an array at the first call of LeaveRequest ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      var leaveSummary = {
        leaveStatistics: sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          },
          LeaveRequest: {
            find: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDaySlot : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };

      var leaves = [{
        fromDate : "2016-09-09",
        toDate : "2016-09-09"
      }];
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(3).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, obj);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });

      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, null, {}); 
      leaveSummary.leaveStatistics.onCall(0).callsArgWith(1 , null, {});
      app.models.LeaveWorkflow.findOne.onCall(1).callsArgWith(1, null, {});
      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, leaves);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leave-statistics.js', leaveSummary);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        //expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        //expect(err.error).to.equal('LeaveRequest is undefined');
        done();
      });

  });

 it('should give an array at the first call of LeaveRequest ', function(done){
      var utilMock = {
        ErpKeysCode : sinon.stub(),
        latestTimestamp : sinon.stub()
      };
      var leaveSummary = {
        leaveStatistics: sinon.stub()
      };
      app ={
        models: {
          LeaveWorkflow: {
            findOne: sinon.stub()    
          },
          ErpKeys: {
            findOne: sinon.stub()
          },
          TypeOfLeaves:{
            findOne: sinon.stub()
          },
          LeaveRequest: {
            find: sinon.stub()
          }  
        }
      };

      var obj = {
        leaveRequest: function(){
          return { leaveRequestYear: '2016',
                   fromDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              toDate: 'Tue Jul 19 2016 05:30:00 GMT+0530 (IST)',
                              numberOfDays: 1,
                              status: 4,
                              description: 'tesg',
                              halfDaySlot: 0,
                              typeOfLeavesId: 4,
                              employeeId: '1055',
                              halfDaySlot : 1,
                              id: 3428,
                              employee: function(){
                                                   return { 
                                                         empId: 'P10E0154',
                                                         name: 'sangeetha',
                                                          designation: 54,
                                                          id: 1055 }
                              }
                  }
        }
      };

      var leaves = [{
        fromDate : "2016-09-09",
        toDate : "2016-09-09"
      }];
      utilMock.ErpKeysCode.onCall(0).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(1).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(2).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.ErpKeysCode.onCall(3).callsArgWith(2, null,
      {
        keyCode : 3
      });
      utilMock.latestTimestamp.onCall(0).callsArgWith(1, null, [{
        leave_request_id: 1,
        action_timestamp: "2016-06-06 15:51:00"}]);
      app.models.LeaveWorkflow.findOne.onCall(0).callsArgWith(1, null, null);
      app.models.ErpKeys.findOne.onCall(0).callsArgWith(1, null, {
         keyCode : 3
      });

      app.models.TypeOfLeaves.findOne.onCall(0).callsArgWith(1, null, {}); 
      leaveSummary.leaveStatistics.onCall(0).callsArgWith(1 , null, {});
      app.models.LeaveWorkflow.findOne.onCall(1).callsArgWith(1, null, {});
      app.models.LeaveRequest.find.onCall(0).callsArgWith(1, null, leaves);
      mockery.registerMock('./utilities.js', utilMock);
      mockery.registerMock('./leave-statistics.js', leaveSummary);
      var leaveRequestsWaitingForApproval = require('../common/leaveRequestsWaitingForApproval');
      leaveRequestsWaitingForApproval.leaveRequestsWaitingForApproval(2, function(err, list){
        expect(utilMock.ErpKeysCode.called).to.be.true;
        //expect(err).to.be.a('object');
        expect(utilMock.ErpKeysCode.getCall(0).args[0]).to.equal("leave_status");
        //expect(err.error).to.equal('LeaveRequest is undefined');
        done();
      });

  });


  });