var path = require('path');
var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.erp_lms;
ds.isActual('employee', function(err, actual) {
    if (!actual) {
        ds.autoupdate('employee', function(err, result) {
 			 if (err) throw err;
 			 console.log("result of autoupdate", result);  
 			 ds.disconnect();         
        });
    }
}); 
