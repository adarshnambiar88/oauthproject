var request = require('supertest');
var async = require('async');
var chai = require('chai');
var expect = chai.expect;

describe('GET /Calculate toDate fom fromDate and numberOfDays',function(){
	
	it('should get the toDate from fromDate and numberOfDays', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-28&&numberOfDays=2')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.toDate).to.equal('2016-03-29T00:00:00.000Z');
					done();
				});
	});


	it('should get the toDate from fromDate and numberOfDays', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-28&&numberOfDays=1')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.toDate).to.equal('2016-03-28T00:00:00.000Z');
					done();
				});
	});


	it('should get the toDate from fromDate and numberOfDays', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-24&&numberOfDays=2')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.toDate).to.equal('2016-03-28T00:00:00.000Z');
					done();
				});
	});


	it('should get the toDate from fromDate and numberOfDays', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-25&&numberOfDays=2')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.toDate).to.equal('2016-03-29T00:00:00.000Z');
					done();
				});
	});


	it('should get the toDate from fromDate and numberOfDays', function(done){
				request('http://localhost:5000/')
				.get('date?fromDate=2016-03-26&&numberOfDays=1')
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.toDate).to.equal('2016-03-28T00:00:00.000Z');
					done();
				});
	});



});
