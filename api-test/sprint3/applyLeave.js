var request = require('supertest');
var async = require('async');
var chai = require('chai');
var expect = chai.expect;

describe('POST /Apply leave',function(){
	beforeEach(function(){
		request = request('http://localhost:5000/api')
	});
	afterEach(function(){

	});

	it('Can apply a leave-request', function(done){
		async.waterfall([
			function applyNewLeaveRequest(callback){
				request
				.post('/leave_requests')
				.send({fromDate: '2016-05-28T00:00:00.000Z',
						toDate: '2016-05-29T00:00:00.000Z',
						numberOfDays: '2',
  						description: 'sick leave',
  						type: 'Privilege',
  						employeeId: 'P1012349',
  						halfDaySlot: '1',
  						approverId: 'P1012348'})
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					callback(null, leave.employeeId, leave.id);
				});	
			},
			function getNewlyAppliedLeaveRequest(employeeId, id, callback){
				request
				.get('/leave_requests/'+ id)
				.expect(200, function(err, res){
					var leave = JSON.parse(res.text);
					expect(leave.id+"").to.equal(id);
					expect(leave.employeeId).to.equal(employeeId);
					expect(leave.fromDate).to.equal('2016-05-28T00:00:00.000Z');
					expect(leave.toDate).to.equal('2016-05-29T00:00:00.000Z');
					expect(leave.numberOfDays).to.equal(2);
					expect(leave.description).to.equal('sick leave');
					callback(null, id);
				})
			},
			function deleteLeaveRequest(id, callback){
				request
				.delete('/leave_requests/' + id)
				.expect(200, function(err, res){
					console.log(res.status);
					expect(res.status).to.equal(200);
					callback();
				});
			}
			],function(err){
				done();
			});
	})
});
