var server = require('./server');
var ds = server.dataSources.erp_lms;
var lbTables = ['userIdentity', 'userCredential', 'user', 'accessToken'];								//array of table that needs to be created
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
  ds.disconnect();
});