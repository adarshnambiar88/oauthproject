module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  var async = require('async');
  var ejs = require('ejs');
  var fs = require('fs');
  var util = require('../../common/utilities.js');
  var specialLeaves = require('../../common/leave-options.js');
  var summary = require('../../common/leave-statistics.js');
  var actionTaken = require('../../common/manager-actions.js');
  var loginData = require('../../common/login');



  router.get('/date', function(request, response){  	                                           //get request to get to-date field from from-date and to-date
  	var fromDate = new Date(request.query.fromDate);                                             //or to get number of days field from from-date and to-date
  	var toDate = new Date(request.query.toDate);
  	var numberOfDays = request.query.numberOfDays ;
  	util.date(fromDate,toDate,numberOfDays, function(err, res){
  			response.send(res);
  	});
  });

  router.get('/specialLeaveDateCalculation', function(request, response){                         //date calculation same as above but here date calculation will be done 
    var fromDate = new Date(request.query.fromDate);                                              //including holidays as these are special leaves
    var toDate = new Date(request.query.toDate);
    var numberOfDays = request.query.numberOfDays ;
    util.specialLeaveDateCalculation(fromDate,toDate,numberOfDays, function(err, res){
        response.send(res);
    });
  });

  router.get('/specialLeaves', function(request, response){                                       //get request to get the special leaves wrt to an employee
    var id = request.query.id ;
    specialLeaves.leaveOptions(id, function(err,res){
      response.send(res);
    });
  });

  router.get('/leaveStatistics', function(request, response){                                      //get request to get the leave statistics of an employee
    var employeeId = request.query.id ;
    summary.leaveStatistics(employeeId, function(err,res){
    response.send(res);
    });
  });


  router.post('/managerActions', function(request, response){                                      //post request for manager actions like approve,deny or send-back
    var actions = request.body ;
    actionTaken.managerActions(actions, function(err,res){
    response.send(res);
    });
  });


  var data = {};



  router.get('/login', function(req, res, next) {
  return res.redirect('http://apps.people10.com/auth/google');  



//   console.log("Inside the login function");
//   // console.log("METHOD", req);
//   res.header('Access-Control-Allow-Origin','*');
//   console.log(req.method);
//    if ('OPTIONS' == req.method) {
//       res.send(200);
//     }
//     else {
//       res.redirect('http://localhost:5000/auth/google');
//     }
  
// //res.send(data);
});

  router.get('/success', function(req, res, next) {
  console.log('COOKIES');
  console.log(req.cookies);
  console.log(req.cookies['connect.sid']);
  console.log('COOKIES END');
  loginData.getUserDetails(function(err, d){
    if(err){
      return res.status(400).send(err);
    }
    else{
      //console.log("*********** data", data);
      data = d;
      res.redirect('http://139.59.27.22:9000/#leave_history?token='+d.id);
      //res.send(data);
    }
  });
});

  router.get('/getUserDetails', function(req, res, next) {
  loginData.getUserDetails(function(err, d){
    if(err){
      return res.status(400).send(err);
    }
    var data = {
      token: {
        id : d.employeeData.id,
        accessToken: d.id
      },
      empId:d.employeeData.empId,
      freshLogin:d.freshLogin,
      name:d.name,
      admin:d.admin,
      manager:d.manager,
      fName:d.firstName,
      lName:d.lastName
    }
    res.send(data);
  });
});

  server.use(router);
};
