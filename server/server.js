var loopback = require('loopback');
var boot = require('loopback-boot');
var config = require('./config.js');
var LoopBackContext = require('loopback-context');
var util = require('../common/utilities.js');
//var loginData = require('../common/login');

var environment = "QACloudEnvironment";

// var cors = require('cors');
// var corsOptions = {
//   origin: '*',
//   optionsSucccessCode: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
// };


host = config[environment].host;
port = config[environment].port;
database = config[environment].database;
username = config[environment].username;
password = config[environment].password;
console.log("Param", host, port, database, username, password);


app = module.exports = loopback();

//app.use(cors());

// app.use(function(req, res, next) {
//         res.header("Access-Control-Allow-Origin", "*");
//         res.header("Access-Control-Allow-Headers", "X-Requested-With");
//         res.header("Access-Control-Allow-Headers", "Content-Type");
//         res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
//         next();
//     });

app.use(loopback.context());                                              //middleware to find the current user using access token
app.use(loopback.token());
app.use(function setCurrentUser(req, res, next) {
  console.log("lets count");
  if (!req.accessToken) {
    //console.log("error in accessToken");
    return next();
  }
  app.models.User.findById(req.accessToken.userId, function(err, user) {
    if (err) {
      return next(err);
    }
    //console.log("user", user);
    if (!user) {
      return next(new Error('No user with this access token was found.'));
    }
    var loopbackContext = loopback.getCurrentContext();
    if (loopbackContext) {
      //console.log("inside server.js",user);
      loopbackContext.set('currentUser', user);
      var c = loopback.getCurrentContext();
      var accessToken = c.get('accessToken');
      //console.log("accessToken", accessToken);
    }
    next();
  });
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};




// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});





var PassportConfigurator = require('loopback-component-passport').PassportConfigurator;
var passportConfigurator = new PassportConfigurator(app);

 
// Load the provider configurations
var config = {};
try {
  config = require('../providers.json');
} catch(err) {
  console.error('Please configure your passport strategy in `providers.json`.');
  console.error('Copy `providers.json.template` to `providers.json` and replace the clientID/clientSecret values with your own.');
  process.exit(1);
}


// Initialize passport
passportConfigurator.init();
 
// Set up related models
passportConfigurator.setupModels({
  userModel: app.models.user,
  userIdentityModel: app.models.userIdentity,
  userCredentialModel: app.models.userCredential
});

// Configure passport strategies for third party auth providers
for(var s in config) {
  var c = config[s];
  c.session = c.session !== false;
  passportConfigurator.configureProvider(s, c);
}


// app.options('*', cors());

// var data = {};



// app.get('/loginD', function(req, res, next) {
//   return res.redirect('http://localhost:5000/auth/google');  



//   console.log("Inside the login function");
//   // console.log("METHOD", req);
//   res.header('Access-Control-Allow-Origin','*');
//   console.log(req.method);
//    if ('OPTIONS' == req.method) {
//       res.send(200);
//     }
//     else {
//       res.redirect('http://localhost:5000/auth/google');
//     }
  
// //res.send(data);
// });

// app.get('/success', function(req, res, next) {
//   console.log('COOKIES');
//   console.log(req.cookies);
//   console.log(req.cookies['connect.sid']);
//   console.log('COOKIES END');
//   loginData.getUserDetails(function(err, d){
//     if(err){
//       return res.status(400).send(err);
//     }
//     else{
//       //console.log("*********** data", data);
//       data = d;
//       res.redirect('http://localhost:9000');
//       //res.send(data);
//     }
//   });
// });

// app.get('/getUserDetails', function(req, res, next) {
//   loginData.getUserDetails(function(err, d){
//     if(err){
//       return res.status(400).send(err);
//     }
//     var data = {
//       token: {
//         id : d.employeeData.id,
//       },
//       empId:d.employeeData.empId,
//       freshLogin:d.freshLogin,
//       name:d.name,
//       fName:d.firstName,
//       lName:d.lastName
//     }

//     return res.send(data);
//   });
// });









