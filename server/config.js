module.exports = {
	localEnvironment:{
		  host: "10.0.1.112",
   		port: 5432,
    	database: "erp",
    	username: "postgres",
    	password: "postgres",
      passwordResetPort: 9000,
      passwordResetHost: "10.0.1.112"

	},
	QAEnvironment: {
		  host: "10.0.1.226",
   		port: 5432,
    	database: "qaerp",
    	username: "postgres",
    	password: "postgres",
      passwordResetPort: 9000,
      passwordResetHost: "10.0.1.226"
	},
	UATEnvironment:{
		  host: "10.0.1.226",
   		port: 5432,
    	database: "uaterp",
    	username: "postgres",
    	password: "postgres",
      passwordResetPort: 9040,
      passwordResetHost: "10.0.1.226"
	},
	DevEnvironment:{
		  host: "10.0.1.221",
   		port: 5432,
    	database: "erp_lms",
    	username: "postgres",
    	password: "postgres",
      passwordResetPort: 9000,
      passwordResetHost: "10.0.1.221"
	},
	productionEnvironment:{
		  host: "10.0.1.84",
   		port: 5433,
    	database: "erp_lms",
    	username: "dbOwnerAdmin",
    	password: "mayank",
      passwordResetPort: 9000,
      passwordResetHost: "10.0.1.84"      
	},
  QACloudEnvironment: {
    host: "139.59.27.22",
    port: 5432,
    database: "erp-lms",
    username: "postgres",
    password: "postgres",
    passwordResetPort: 9000,
    passwordResetHost: "139.59.27.22"
  }

};



 